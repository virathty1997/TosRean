window.onload = function (e) {
        console.log(e);
        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            columnFill:false,
            columnSpan:false,
            title:{
              title:"Rate Number"
            },
            toolTip: {
                content: toolTipFormatter
            },

            data: [{
                type: "bar",
                name:"Rate Report",
                dataPoints: [
                    { y: 100, label: "1" ,color:"#af710f"},
                    { y: 120, label: "2", color:"#AF7F3A"},
                    { y: 147, label: "3" ,color:"#c1b710"},
                    { y: 217, label: "4" ,color:"#87aa0d"},
                    { y: 369, label: "5" ,color:"#4da477"},
                ]
            }]
        });
        chart.render();

        function toolTipFormatter(e) {
            var str = "";
            var total = 0 ;
            var str3;
            var str2 ;
            for (var i = 0; i < e.entries.length; i++){
                str = "<strong>"+  e.entries[i].dataPoint.y + "</strong>";
            }
            return str;
        }
    }