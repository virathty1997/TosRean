
var gDeleteId = '';
/*Added by Thoura on 10-July-2018*/
$(document).ready(function () {

    $(".favorite.heart").click(function (e) {
        e.preventDefault();
        if (gUserLoginId == undefined || gUserLoginId == null || gUserLoginId == '' || gUserLoginId == 0) {
            $('#loginModal').modal('show');
            return;
        }

        var value = this.hash.split('=');
        var data = new FormData();
        data.append("uploadId",value[1]);
        data.append("userLoginId",gUserLoginId);
        $.ajax({
            url: '../model/mark.php',
            method: 'post',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data > 0 || Number(data) > 0){

                    var heartId = '#heart'+value[1];

                    if ($(heartId).hasClass('active')){
                        $(heartId).removeClass('active');
                        //alert("You've unmarked this book");
                    }else{
                        $(heartId).addClass('active');
                        //alert("You've marked this book");
                    }
                }
            }
        });
    });
});


$(document).ready(function () {
    $(".last-favorite.delete").click(function (e) {
        e.preventDefault();
        if (gUserLoginId == undefined || gUserLoginId == null || gUserLoginId == '' || gUserLoginId == 0) {
            alert('Please login your account!');
            return;
        }
        var value = this.hash.split('=');
        gDeleteId = value[1];

        $('#button-popup-delete').trigger('click'); /*remote to pop up dialog*/

        $("#button-delete").click(function () {
            var data = new FormData();
            data.append("uploadId",gDeleteId);
            $.ajax({
                url: '../action/delete_book.php',
                method: 'post',
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data > 0 || Number(data) > 0){
                        var id = '#book-container'+gDeleteId;
                        $(id).remove();
                    }
                }
            });
        })
    });
});

/*Top button event by Thoura on 17-07-2018*/
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("button-top").style.display = "block";
    } else {
        document.getElementById("button-top").style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}


$("#upload-book-id").click(function () {

    if (gUserLoginId == undefined || gUserLoginId == null || gUserLoginId == '' || gUserLoginId == 0) {
        event.preventDefault();
        $('#loginModal').modal('show');
    }
})


/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }