
if (this.gUserLoginId == undefined || this.gUserLoginId == 0 || this.gUserLoginId == null){
    this.gUserLoginId = 0;
}else{
    var myNotification = setInterval(countNewNotifications, 2000);
}

function countNewNotifications() {
    //console.log('UserLogin Id : '+this.gUserLoginId);
    if (this.gUserLoginId == 0) return;
    var data = new FormData();
    data.append("userLoginId",gUserLoginId);
    $.ajax({
        url: "../action/count_new_notification.php",
        method: "POST",
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (result) {
            //console.log('Result : '+result);
            if (result != null && result != undefined){
                $("#notification-number").attr('data-count',result);
                if (result > 0 || Number(result) > 0){
                    $("#notification-number").css('visibility','visible');
                }else{
                    $("#notification-number").css('visibility','hidden');
                }
            }
        },
        error:function (e) {
            console.log(e);
        }
    });
}

$("#notification-icon").click(function () {

    $("#noti-container").load("../action/load_notification.php",{
        userLoginId:gUserLoginId,
        limitRows: 10
    });
});

/*On Header.php File*/
$("#noti-container").on("click", "a", function() {
    var index = $(this).index();
    var id = "#notification"+index;
    var info = $(id).text().split('|');
    if (info.length != 2 || info[0] != 'comment' && info[0] != 'reaction' && info[0] != 'upload' || Number(info[1]) <= 0) return;
    updateSeenNotification(info[0],Number(info[1]));
});

/*On View Notification Page*/
$("#container-notifications").on("click", "a", function() {
    var index = $(this).index();
    var id = "#notification"+index;
    var info = $(id).text().split('|');
    if (info.length != 2 || info[0] != 'comment' && info[0] != 'reaction' && info[0] != 'upload' || Number(info[1]) <= 0) return;
    updateSeenNotification(info[0],Number(info[1]));
});


function updateSeenNotification(type,id) {

    if (type == '' || id <= 0) return;
    var data = new FormData();
    data.append("type",type);
    data.append("id",id);
    $.ajax({
        url: "../action/update_seen_notification.php",
        method: "POST",
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (result) {
            console.log('Result : '+result);
        },
        error:function (e) {
            console.log('Error update seen: '+e);
        }
    });
}

$("#clear-notification").on("click", function() {
    clearNewNotifications();
});

function clearNewNotifications() {
    if (this.gUserLoginId == 0) return;
    var data = new FormData();
    data.append("userLoginId",this.gUserLoginId);

    $.ajax({
        url: "../action/clear_notification.php",
        method: "POST",
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (result) {
            console.log(result);
        },
        error:function (e) {
            console.log(e);
        }
    });
}

