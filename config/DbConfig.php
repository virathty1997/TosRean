<?php

class DbConfig
{
    static  function query($sql, &$last_id = 0, &$effected_rows = 0)
    {
        $dbServerName = "localhost";
        $dbUsername = "root";
        $dbPassword = "";
        $dbName = "rean_post";

        $con = mysqli_connect($dbServerName,$dbUsername,$dbPassword,$dbName);
        $result = mysqli_query($con,$sql);
        $effected_rows = mysqli_affected_rows($con);
        $last_id = mysqli_insert_id($con);
        mysqli_close($con);
        return $result;
    }
}

?>