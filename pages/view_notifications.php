<?php
    include 'header.php';
    if (isset($_GET['date'])){
        $filterDate = $_GET['date'];
    }
?>

<link rel="stylesheet" href="../assets/css/view_notifications.css">
    <div id="wrapper-notifications" class="wrapper-notifications">

        <div id="all-container-notifications" class="all-container-notifications">
            <div class="notif-header">
                <div class="notif-title">
                    <div style="width: 50%; margin: 0;">
                        <p title="View All" id="id-all-notification" style="font-weight: bold; margin: 0; padding: 0;" class="mark-label">All Notifications</p>
                    </div>
                    <div style="width: 50%; margin: 0;">
                        <p style="font-weight: bold; margin: 0; padding: 0;" id="id-clear-notification" class="mark-label clear-noti">Clear Notification</p>
                    </div>
                </div>
                <div class="bar"></div>
            </div>
            <div style="margin: 0; padding: 0;" id="container-notifications" class="container-notifications">

            </div>
        </div>
    </div>

<script>
    var gFilterDate = '<?php echo $filterDate;?>';
    console.log(gFilterDate)
    window.onload = function () {
        if (gFilterDate == undefined || gFilterDate == null || gFilterDate == ''){
            loadAllNotifications();
        }else{
            loadFilterNotifications();
        }
    }

    function loadAllNotifications() {
        $("#container-notifications").load("../action/load_notification.php",{
            userLoginId:gUserLoginId
        });
    }

    function loadFilterNotifications() {
        if (gFilterDate == undefined || gFilterDate == null || gFilterDate == '') return;

        $("#container-notifications").load("../action/load_notification.php",{
            userLoginId:gUserLoginId,
            filterDate:gFilterDate
        });
    }

    $("#id-clear-notification").on("click", function() {
        clearNewNotifications();
        if (gFilterDate == undefined || gFilterDate == null || gFilterDate == ''){
            loadAllNotifications();
        }else{
            loadFilterNotifications();
        }
    });

    function clearNewNotifications() {
        if (this.gUserLoginId == 0) return;
        var data = new FormData();
        data.append("userLoginId",this.gUserLoginId);
        $.ajax({
            url: "../action/clear_notification.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                console.log(result);
            },
            error:function (e) {
                console.log(e);
            }
        });
    }

    $("#id-all-notification").click(function () {
        //loadAllNotifications();
        window.location.replace("view_notifications.php");
    })

</script>

<?php
    include 'footer.php';
?>
