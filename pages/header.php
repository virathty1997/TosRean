<?php
require_once '../node_modules/vendor/autoload.php';
include_once "../config/DbConfig.php";
include_once "../model/User.class.php";
include_once('../action/loginWithGoogle.php');


if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$userLoginId = isset($_SESSION['userLoginId']) ? $_SESSION['userLoginId'] : 0;

$gUserType = '';

if (isset($_COOKIE['email']) && isset($_COOKIE['last_login'])) {
    $cookie_email = $_COOKIE['email'];
    $cookie_last_login = $_COOKIE['last_login'];

    $user = User::getUserByCookie($cookie_email, $cookie_last_login);
    if ($user) {
        $_SESSION['profile'] = $user->profile;
        $_SESSION['fullname'] = $user->fullName;
        $_SESSION['userLoginId'] = $user->id;
        $_SESSION['email'] = $user->email;

        $gUserType = $user->userType;
    }
}

if ($client->getAccessToken()) {

    $gUserType = 'gmail';

    $plus = new Google_Service_Plus($client);
    $profile = $plus->people->get('me');

    $loginEmail = ($profile['emails'][0]['value']);
    $gUser = User::getUserByLoginAsEmail($loginEmail);

    $dt = new DateTime();
    $lastLoginDate = $dt->format('Y-m-d H:i:s');

    if ($gUser) {
        $_SESSION['profile'] = $gUser->profile;
        $_SESSION['fullname'] = $gUser->fullName;
        $_SESSION['userLoginId'] = $gUser->id;

        //SAVE to COOKIES for RE-SIGN IN
        $cookieName = "email";
        $cookieValue = $loginEmail;
        setcookie($cookieName, $cookieValue, time() + (86400 * 30), "/");
        setcookie("last_login", $lastLoginDate, time() + (86400 * 30), "/");

        /*Update Last Login in Users table*/
        User::updateLastLogin($gUser->id, $lastLoginDate);

    } else {

        $user = new User();
        $user->userType = 'gmail';
        $user->fullName = $profile['displayName'];
        $user->gender = "";
        $user->dob = date('y-m-d', strtotime($profile['birthday']));
        $user->email = $loginEmail;
        $user->password = "";
        $user->profile = $profile['image']['url'];
        $user->google_code = $_GET['code'];
        $user->last_login = $lastLoginDate;
        $insertedId = User::add($user);

        $ch = curl_init($profile['image']['url']);
        $fp = fopen('../images/go.png', 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        $profileURL = $profile['image']['url'];

        $_SESSION['profile'] = $profileURL;
        $_SESSION['fullname'] = $profile['displayName'];
        $_SESSION['userLoginId'] = $insertedId;

        //save to cookie for NEW sign in
        $cookieName = "email";
        $cookieValue = $loginEmail;
        setcookie($cookieName, $cookieValue, time() + (86400 * 30), "/");
        setcookie("last_login", $lastLoginDate, time() + (86400 * 30), "/");
    }

} else {
    $authUrl = $client->createAuthUrl();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rean_Post</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="../assets/css/styles.css">
    <link rel="stylesheet" href="../assets/css/index.css">
    <link rel="stylesheet" href="../assets/css/styles.min.css">
    <link rel="stylesheet" href="../assets/css/dropdown.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
    integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script>
        var gUserLoginId = <?php echo $userLoginId;?>;
    </script>
</head>
<body>

    <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
        <div class="container">

            <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <div class="logo">
                    <a href="index.php">
                        <img src="../assets/images/logo.png" alt="">
                    </a>
                </div>
                <ul class="nav navbar-nav mr-auto">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                        href="#">Category </a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Business">Business</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Cartoon">Cartoon</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Political">Political</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Novel">Novel</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Moeys">Moeys</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Science">Science</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Law">Law</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Economic">Economic</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Legend">Legend</a>
                            <a class="dropdown-item" role="presentation" href="category.php?genres=Health">Health</a>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a id="upload-book-id" class="nav-link" href="upload.php">Upload</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" href="about.php">About</a>
                    </li>

                    <li>
                        <form action="search_result.php" method="POST">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" id="txtSearch"
                                name="search_value" style="border: none;"/>
                                <div class="input-group-btn">
                                    <button class="btn-search" type="submit" name="search">
                                        <i class='fa fa-search'></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </li>
                </ul>

                <!--Notification--By Thoura Lai: 03-July-2018 -->
                <div <?php echo (isset($_SESSION['userLoginId'])) ? '' : 'hidden' ?> class="notifications">
                   <ul style="left: 0;" class="nav navbar-nav mr-auto">
                    <li class="dropdown">
                        <div style="display: flex; position: relative;" class="contain">
                            <div class="icon" style="position: absolute;">
                                <div id="notification">
                                   <div class="ring-icon">
                                       <i class="p3 fa fa-bell fa-stack-1x fa-inverse"></i>
                                   </div>
                                   <div class="ring-number">
                                        <span id="notification-number" class="p1 fa-stack fa-5x has-badge" data-count="0">
                                   </div>

                                </span>
                            </div>
                        </div>
                        <div style="z-index: 10;" class="content">
                            <a id="notification-icon" style="margin-left: 25px;"
                            class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown"
                            aria-expanded="false" href="#"></a>
                            <div class="notification-content dropdown-menu dropdown-menu-right" role="menu">
                                <div class="notif-header">
                                    <div class="notif-title">
                                        <div style="width: 50%;">
                                            <p class="mark-label top-text">Notifications</p>
                                        </div>
                                        <div style="width: 50%;">
                                            <p id="clear-notification" class="mark-label clear-noti">Clear
                                            Notification</p>
                                        </div>
                                    </div>
                                    <div class="bar"></div>
                                </div>
                            <div style="height: 450px; width: 400px; overflow-y:scroll;" id="noti-container" class="noti-container">
                            </div>
                            <div class="notif-footer">
                                <div class="bar"></div>
                                <div class="notifi-see-all">
                                    <a href="view_notifications.php" class="mark-label">See All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <!--Profile-->
    <div <?php echo (isset($_SESSION['userLoginId'])) || !empty($profile) ? '' : 'hidden' ?> class="profile-dropdown"
       style="float:right;">

       <img src="<?php echo (isset($_SESSION['profile'])) ? $_SESSION['profile'] : '../images/no_profile.png' ?>"
       width="40px" height="40px" style="border-radius: 50px;" alt="" onError="this.onerror=null;this.src='../images/no_profile.png';">
       <div class="profile-dropdown-content">
        <div class="name">
            <label style="margin-left: 10px"
            class="mark-label"><?php echo (isset($_SESSION['fullname'])) ? $_SESSION['fullname'] : '' ?></label>
        </div>
        <div class="bar"></div>
        <div <?php echo ($gUserType != 'owner' && $gUserType != "" ) ? ' hidden ' : ' '; ?> class="profile">
            <a class="mark-label"
            href="profile.php?id=<?php echo (isset($_SESSION['userLoginId'])) ? $_SESSION['userLoginId'] : 0 ?>">Profile</a>
        </div>
        <div class="help">
            <a class="mark-label" href="help.php">Help</a>
        </div>
        <div class="bar"></div>
        <div class="sign-out">
            <a class="mark-label" href="../action/sign_out.php">Sign out</a>
        </div>
    </div>

</div>

<span class="navbar-text actions">
    <a <?php echo (isset($_SESSION['userLoginId'])) ? 'hidden' : '' ?>
    class="btn btn-light action-button" role="button" href="sign_in.php">Sign In</a>
    <a <?php echo (isset($_SESSION['userLoginId'])) ? 'hidden' : '' ?>
    class="btn btn-light action-button" role="button" href="sign_up.php">Sign Up</a>
</span>

</div>
</div>
</nav>

<div id="loginModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please login</h4>
<!--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <a href="index.php" class="close" data-dismiss="modal" aria-hidden="true">x</a>

            </div>
            <div class="modal-body">
              <div class="sign-form">
                <form action="../action/compare_sign_in.php" method="post">
                    <div class="form-group">

                        <input class="form-control" type="email" name="email" placeholder="Username or email"
                        value="<?php echo isset($email) ? $email : '' ?>">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" id="password" name="password" placeholder="Password"
                        value="<?php echo isset($password) ? $password : '' ?>">
                    </div>
                    <div class="form-group submit">
                        <input class="btn btn-primary form-control" type="submit" id="signin" name="signin" value="Sign In"
                        style="border-radius: 4px;background: transparent;color: #1ea291;border: 1px solid #1da391;">
                    </div>
                </form>
                  <?php
                      if(isset($authUrl)){
                          print "<a href='$authUrl' class='btn btn-fb' style='width: 100%;background: #1ea291;''><i class='fa fa-google'></i> Google</a>";
                          print "<a href='$authUrl' class='btn btn-tw' style='width: 100%;background: #1ea291;'><i class='fa fa-facebook'></i> Facebook</a>";
                      }else{
                          print "<a href='#' class='btn btn-fb' style='width: 100%;background: #1ea291;''><i class='fa fa-google'></i> Google</a>";
                          print "<a href='#' class='btn btn-tw' style='width: 100%;background: #1ea291;'><i class='fa fa-facebook'></i> Facebook</a>";
                      }
                  ?>
              </div>

        </div>
    </div>
</div>
</div>

