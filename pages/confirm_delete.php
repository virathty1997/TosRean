
<button hidden id="button-popup-delete" class="btn btn-default" data-href="/delete.php?id=54" data-toggle="modal" data-target="#confirm-delete">
    pop up..
</button>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div style="display: block; padding: 10px 15px;" class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title" id="myModalLabel">Confirm Delete</h5>
            </div>

            <div style="display: flex; align-items: center; padding: 10px;" class="modal-body">
                <i style="color: green; font-size: 30px; margin-right: 10px;" class="fas fa-question-circle"></i>
                <p style="margin: 0;"> Are you sure to delete this book?</p>
            </div>

            <div style="padding: 10px 10px; margin: 0;" class="modal-footer">
                <button id="button-cancel" style="background: #0dadb1;" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="button-delete" style="background: rgba(232,0,0,0.78);" type="button" class="btn btn-default" data-dismiss="modal">Delete</button>
            </div>
        </div>
    </div>
</div>