<?php
    include 'header.php';
?>

    <div style="display: flex; justify-content: center; align-items: center;">
        <img style="width: 50%; height: 20%; border-radius: 50px;" src="../images/construction.jpg" alt="">
    </div>

    <!-- Button trigger modal -->
    <button hidden id="popup-success" type="button" class="btn btn-primary" data-toggle="modal" data-target="#dialog-success">Pop up</button>

    <!-- Modal -->
    <div class="modal fade" id="dialog-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Help Page</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    This page is in Under Construction. Please come back later.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<script>

    window.onload = function () {
        $('#popup-success').trigger('click');
    }

</script>

<?php
    include 'footer.php';
?>
