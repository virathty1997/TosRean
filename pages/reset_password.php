<?php
    require_once '../model/Email.php';
    require_once '../model/User.class.php';
    require_once '../config/DbConfig.php';

    $isSuccess = false;
    $isFail = false;

    $newPasswordEmpty = false;
    $newPasswordInvalid = false;

    $confirmPasswordInvalid = false;

    if (isset($_POST['cancel'])){
        header('location:../pages/sign_in.php');
    }else if(isset($_POST['change'])){

        $newPassword = $_POST['new-password'];
        $confirmPassword = $_POST['confirm-password'];
        $generatedCode = isset($_GET['code'])?$_GET['code']:'';

        if (!empty($generatedCode)){

            if (empty($newPassword)){
                $newPasswordEmpty = true;
            }else{
                if(strlen($newPassword) < 8 || strlen($newPassword) > 50){
                    $newPasswordInvalid = true;
                }else if ($newPassword != $confirmPassword){
                    $confirmPasswordInvalid = true;
                }else{
                    $hashedPassword = password_hash($newPassword,PASSWORD_DEFAULT);
                    if (User::resetPasswordByCode($hashedPassword,$generatedCode) > 0){
                        $isSuccess = true;
                        header('location:../pages/sign_in.php');
                        exit();
                    }else $isFail = true;
                }
            }
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset Password</title>
    <link rel="stylesheet" href="../assets/css/reset_password.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="reset-wrapper">
    <div class="reset-container">
        <form action="" method="post">
            <div class="reset-title form-group">
                <h4 class="reset-email">Reset Password</h4>
            </div>
            <div class="reset-body form-group">
                <input class="form-control" type="password" name="new-password" id="new-password" placeholder="New password" value="<?php echo isset($newPassword)?$newPassword:'' ?>">
                <label <?php echo ($newPasswordEmpty)?'':'hidden' ?> class="error reset-email" >Please enter new password.</label>
                <label <?php echo ($newPasswordInvalid)?'':'hidden' ?> class="error reset-email" >Password is at least 8 and less than 50 characters.</label>
                <br><?php echo ($newPasswordEmpty || $newPasswordInvalid)?'<br>':'';?>
                <input class="form-control" type="password" name="confirm-password" id="confirm-password" placeholder="Confirm password" value="<?php echo isset($confirmPassword)?$confirmPassword:'' ?>">
                <label <?php echo ($confirmPasswordInvalid)?'':'hidden' ?> class="error reset-email" >Confirm password does not match.</label>
            </div>
            <div class="reset-footer form-group">
                <input class="cancel btn btn-default" type="submit" name="cancel" id="cancel" value="Cancel">
                <input class="send btn btn-primary" type="submit" name="change" id="change" value="Change">
            </div>
        </form>
    </div>
</div>
</body>
</html>


