<?php
    require_once '../model/Email.php';
    require_once '../model/User.class.php';
    require_once '../config/DbConfig.php';

    $isSuccess = false;
    $isFail = false;
    $emailInvalid = false;
    $emailEmpty = false;
    $emailNotFound = false;

    if (isset($_POST['back'])){
        header('location:../pages/sign_in.php');
    }else if(isset($_POST['send'])){
        $email = $_POST['email'];
        if (empty($email)){
            $emailEmpty = true;
        }else{
            if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $emailInvalid = true;
            }else{
                if (User::isExistEmail($email)){
                    $user = User::updateVerificationCode($email);
                    if (!$user){
                        $isFail = true;
                    }else{
                        if (sendResetPasswordLink($user)){
                            $isSuccess = true;
                        }else $isFail = true;
                    }
                }else{
                    $emailNotFound = true;
                }
            }
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset Password</title>
    <link rel="stylesheet" href="../assets/css/reset_password.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>
    <div class="reset-wrapper">
        <div class="reset-container">
            <form action="" method="post">
                <div class="reset-title form-group">
                    <h4 class="reset-email">Reset Password</h4>
                </div>
                <div class="reset-body form-group">
                    <label class="reset-email" for="email">Enter email</label>
                    <br>
                    <label <?php echo ($emailEmpty)?'':'hidden' ?> class="error reset-email" >Please enter your email to reset password.</label>
                    <label <?php echo ($emailInvalid)?'':'hidden' ?> class="error reset-email" >Email address is invalid.</label>
                    <label <?php echo ($emailNotFound)?'':'hidden' ?> class="error reset-email" >Email address is not exist.</label>
                    <label <?php echo ($isSuccess)?'':'hidden' ?> class="success reset-email" >Reset password link has been sent to your email address. Please check your mail account to reset password .<b><?php $email;?></b></label>
                    <label <?php echo ($isFail)?'':'hidden' ?> class="error reset-email" >Email has not been sent.</label>
                    <input class="form-control" type="email" name="email" id="email" placeholder="example@gmail.com" value="<?php echo isset($email)?$email:'' ?>">
                </div>
                <div class="reset-footer form-group">
                    <input class="cancel btn btn-default" type="submit" name="back" id="cancel" value="Back">
                    <input class="send btn btn-primary" type="submit" name="send" id="send" value="Send">
                </div>
            </form>
        </div>
    </div>


    <script src="../assets/js/jquery.min.js"></script>

    <script>
        /*$("#email").focus(function () {
            $(".error").prop('hidden',true);
        })*/
    </script>

</body>
</html>


