<?php

include('header.php');
include_once "../config/DbConfig.php";
include_once "../model/Book.php";
include_once "../model/BookDetail.php";

$bookGenres = array("Business", "Cartoon", "Economic", "Health", "Law", "Legend", "Moeys", "Novel", "Political", "Science");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$userLoginId = isset($_SESSION['userLoginId']) ? $_SESSION['userLoginId'] : 0;
?>
    <script>
        var gUserLoginId = <?php echo $userLoginId;?>;
        console.log('convert id is executed');
    </script>
    <section>
        <div class="container-fluid d-inline-block float-none">
            <div class="row">
                <div class="col-8 col-md-8 wrapper8" style="height:100%">
                    <?php
                    foreach ($bookGenres as $genres) {
                        $items = Book::book_info($genres, $userLoginId);
                        if (!$items) continue; /*If there is no book in genres we will not show the content*/
                        ?>
                        <div class="main-block">
                            <div class="block-title">
                                <a class="title-link-a" href="category.php?genres=<?php echo $genres; ?>">
                                    <h5 class="title-link"><?php echo $genres; ?> book</h5>
                                </a>
                            </div>
                            <div class="block">
                                <div class="book-wrapper" style="position: relative">
                                    <?php foreach ($items as $item) {
                                        $meanRate = intval($item->mean_rate);
                                        ?>
                                        <div id="book-container<?php echo $item->id; ?>" class="book-container">
                                            <a href="book_detail.php?book=<?php echo $item->id ?>">
                                                <div class="img-container">
                                                    <img src="../images/<?php echo $item->book_cover; ?>" alt="book">
                                                </div>
                                            </a>
                                            <div id="heart<?php echo $item->id; ?>" class="mark-icon <?php echo ($item->markId > 0) ? 'active' : ''; ?>">
                                                <a title="Mark" class="favorite heart"
                                                   href="#id=<?php echo $item->id ?>">
                                                    <i class="fas fa-heart"></i>
                                                </a>

                                                <div <?php echo ($userLoginId == $item->user_id)?'':'hidden';?> class="modify-dropdown">
                                                    <button  class="modify-button">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </button>
                                                    <div class="modify-dropdown-content">
                                                        <div class="modify-shape"></div>
                                                        <a title="Delete" class="last-favorite delete" href="#id=<?php echo $item->id ?>">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                        <a title="Edit" class="last-favorite" href="upload.php?book=<?php echo $item->id ?>">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="info-container">
                                                <div class="emoji">
                                                    <img src="../images/free.png" alt="free" width="40px" height="40px">
                                                </div>
                                                <div class="rating-group">
                                                    <section class="rating-widget">
                                                        <div class="rating-stars text-center">
                                                            <ul id="stars">
                                                                <li title="Poor"
                                                                    data-value="1" <?php echo ($meanRate >= 1) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Fair"
                                                                    data-value="2" <?php echo ($meanRate >= 2) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Good"
                                                                    data-value="3" <?php echo ($meanRate >= 3) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Very Good"
                                                                    data-value="4" <?php echo ($meanRate >= 4) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Excellent"
                                                                    data-value="5" <?php echo ($meanRate >= 5) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </section>
                                                </div>
                                                <div class="info">
                                                    <div class="info_group">
                                                        <ul>
                                                            <li><?php echo $item->title; ?></li>
                                                            <li><?php ?></li>
                                                            <li><?php echo $item->published_date; ?></li>
                                                        </ul>
                                                    </div>
                                                    <div class="info_group">
                                                        <p><?php echo $item->view_number . (($item->view_number > 1) ? " views" : " view"); ?></p>
                                                        <p><?php echo $item->download_number . (($item->download_number > 1) ? " downloads" : " download"); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-4 col-md-4 wrapper4 " style="height:100%;">
                    <div class="side-block">
                        <div class="block-title">
                            <h5 class="title-link">Popular book</h5>
                        </div>

                        <?php
                        $populars = Book::popular_book();
                        foreach ($populars as $popular) { ?>
                            <a href="book_detail.php?book=<?php echo $popular->id ?>">
                                <div class="side-b-container">
                                    <div class="side-b-img">
                                        <img src="../images/<?php echo $popular->book_cover ?>" alt="book">
                                    </div>
                                    <div class="side-b-info">
                                        <div class="b-info-group">
                                            <p>Title : <?php
                                                echo $popular->title;
                                                ?></p>
                                        </div>
                                        <div class="b-info-group">
                                            <p>Author : <?php
                                                echo $popular->author
                                                ?></p>
                                        </div>
                                        <div class="b-info-group">
                                            <p>Published : <?php
                                                echo $popular->published_date
                                                ?></p>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        <?php } ?>
                    </div>

                    <div class="adveritisement">
                        <img src="https://mk0ninjaoutreacgog6y.kinstacdn.com/wp-content/uploads/2017/03/Advertising-strategy.jpg"
                             width="100%">
                    </div>

                    <div class="adveritisement">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhMVFhUVFRYVFRgVFRUVGBcVFhUWFhUXFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0mICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKEBOgMBEQACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAEBQACAwEGB//EAFEQAAEDAgQCBgUFCwcLBQAAAAEAAgMEEQUSITFBUQYTImFxkRQygaHRI0Kx4fAVJDNSVHOkssHS4gdiZJOUs8I0U1VydIKEkqKj8SU1RIPD/8QAGgEAAgMBAQAAAAAAAAAAAAAAAwQAAQIFBv/EADkRAAICAQMCAggFAwQBBQAAAAABAgMREiExBEETUSIyYXGRobHwFFKBwdEjQmIzouHxJAU0RHKC/9oADAMBAAIRAxEAPwD51ZegMYCIdkOXIevgushCq0YNom6IcmGgtjUBZyESO2UyXgs0LLNIuGqsmsFgFRrBC4BVjJMpFHVQC0q2zDvSM31QK0q2jDvTBJngo0U0LTkmZsiLtgtOSXIOMJS4OSRlp1UUlLgqcHB7lcy1gzqJmVYJqZpG9U0bhIkgVIuS3OsCjLSNhEeSxqQXRJEUIdULLZVWS8GTgtIG1uQNUyRLc2hbe4WJPGGFgs5QFI1GTFJLBxoVspGhYs5COJkQtIE0RWQhCovBxWZIoWWIuqL5KELRhrBxQoihDZrFhsMomrQsMIlggUIjisoYUcbctylrG84Q9RGOnLJJbhsrjnuSWM7FW67BW9ilu9iwbZZzk0lg0DVWTWDuUqsmsMEqmO4AosGhW2MnwgcwO/FPki64+YDw5+RUQuvayvWuSvDk3jBvHSgb6obsb4DRoS5CAOAHkht92GS7Izq6c5dfZxsRwK1XNZ2MXUtx3BWYdKRcN96K74LuLR6S1rKRcYXLyHmq/EVmvwN3kWGEyd3ms/iYG10FoQzDH21shvqI9g8ejnjcEnjMbrI8GprIrZF1SwyCqda11XhxzkivljB2OIu1UckiRg5blupcOCrVFm/DmuxvSMDjYmyHY3FZC0xU3hvAX9zBzQfxDGvwafct9zBzKn4hl/g4+ZSSkyEOB04q1bqWDMqPDakhfiMVjfmmKZZWBLqq8PIHHujS4FY8jSKEOZ3hKynpkdCFanX7QGqhIR4STE7q3FgxRQBdpVNGkzpaqyW4lC1Xkw0cVlcFsyrBrJLBTcmETIFMk0oYxUTilpWpD8OnkzWWkAaeazG1thJ9OoxyAhHEypVmQmmizcUKctIxVDWHMpm+KA7GORpijdkfIIbkGjDyN2xhDbDKKLZQpll4RLBVlkwissjW7/X5LUYuXBmc4x5ApqonYWHv+pGjWlyKzvb42BwigPcbNpjx5XA+J2Cw7F2CqmXc19HHHb8UfHj4rGthPCiufgaSR5gQdPtusqWHlBJR1JpgsdYYwWuGyK6lN5QtHqHUtMlwatxA/wCbd5fFYdPtQVdU/wArCA91r2A9qHiOQ6lNrOAWeaQtOU68LD4osYwzuLWWWOPovfsIp3OLjmNzxXQiopbHGslJyep7lGlWzCeDeOWxusOOUGjPS8hzcSP4oS7o9o6usfkCSPuboyWFgVlLLybQ1728bjkdViVMZBYdVOHcc0VR1jc1rJKyGiWDq0W+LDUbSC4ssJ4YWSysC2siuMvLZM1yw8iF8MrSJntLSnU8o5Uk4sZ4c+9wlbljc6PTS1ZiWq47tOiquWJF3QzETvTiOXIotAywkVNGlJosHqsGtR0tNr2VZXBeHjJaGAvNgpKaissuup2PCDvuaGtu4pfx3J4Q5+DUI5kwKzUfLFcRPQMXOZ20VljuFcXgzOOUJnDVPLg5LWGVIURGHYZHcoF7whvpI5Y2awBJts6aikXuqNA1VVBlr/O2CLCtzzjsAuvVeM9zE1/aLbbW27xdb8H0cgn1SU3HBJKsnbT6fqUVaXJUr5PjYGP25ooAIipSd9B7/qQ5WJcB4USfOwVHGG7D7eKC5N8jEYxhwUlaTxsFqLSMzUn3LNbYWWW8mksLB0BQgNWxfOG438PqRapdmL9RD+5dgeoxBxHZYdN+SJCmK5YGzqptejECmxSQgjQDwR49PBPIpPrbZLAM6dx+cUVQiuwu7ZvllFZgihDt1WDWTSOMn1dVlyS5NxhJ+qExUEjuFvFDldCIeHS2z7YDocHHzjfwS8uqfYcr/wDT4r1mMYYQ0WaLBLyk5PLH4VxgsRLuWUaB52XRIvAGyORZiVPpcJqmfY5/VVbZQHQy5XhGtjqixTp56JpnoHR3XPUsHbccnnqyKxK6Ncso4l8MSZ2lw979dhzKqd8YEp6Sdm64D48GA9ZyA+qb4Q7H/wBOivWZvaJgtp9KH/UkGxTWsGbqhrtA0n2LSg1u2DdsZbKJeHMNmH3LMsPlm4alxEqKJ7/XNhyWvFjH1UZXT2Weuzv3JbzKr8S/I1+Aj5mxesYCuRrEbhYlsEg9SFNZDZxTlcsxOZfW4zMchPBbyC0tjWijDRc6JS2Wp4R0unioRyzU1sf4wWPCn5BPxNX5i7qhoF7+Hf4LKg28G3bFLIqqGdY/M7wA5BNweiOlHNtj4s9cv0RcCyo0kkbxUxO+n0+SHKxLgPGmT52C44mt2GvPihOTlyMxhGHA6nMETIc1PnL4RI53XPZqXvbbKBb5iwAXiTlLEsJPHC9gFNi9I3QUlzyFRJ7zbREjVJ99gc5zjsp5fuRpBiNK5oIpP0iTQ8Rssyg4vGTdfiTjqU/ki/ptN+SfpEnwVYfmb8O38/yRPTab8k/SJPgph+ZPDt/P8kT02m/JP0iT4KYfmV4dv5/kgGXEKaInLRbj8pl8vVR4xdi3fyFJqyl+jLn2Cp+KUZJJw/U7/fc37qbUJpYUvkjmybbyyv3Sov8AR/6ZN+6r0Wfn+SKOjEaL/R/6XN+6qcZ/n+SIt+DZlRSnbDT/AGub91Yba5n8kGjTZLhBEbaY74fb/jJv3UN3Nf3/ACQaPRWPkKifSRm4o7E7ffUxHgbhClOU1hv5IYh0s6nlS39wbBiVK4f5LYjQjr5ND5IEoOI1X4k16/8AtRr6ZTfkv/fk+CyF8O38/wDtRPTKb8l/78nwUJ4dv5/9qNKWeme9jPRbZntbfr5NMzgL7d6hicbYxb18LyQrrow2V7Rs17mjwDiB9C0gsHmKb8kCzx3C3GWGZshlHnKhmVxXSg9UThWx0THdBVZx7NUjbXpZ1+mu8RGklODvqsqbXBuVMXyY1Ndk7LRqtwq1bsFb1Kr9GKBAJZDubeQRf6cBZeNawyDDmj1tUGV7fA1X0kY7yNnzRs5BYUJzCysrrB34q3gCURdO+4CXXR7IwlxN3AAe9Ej08e4KfWSfBh6ZJzK34UPID+It8xiG3S+cD2MhUMdghSlkYhDSjr4gd1SlguUFLk42AclbmylVFFatrchBNrggfUrrb1LBm5R0NPYR01MRqbJ+c87I49VLjuwoBBGTaKnJ7h3/AAWJTSCxqlIKjhDfid/qQXJyGY1qBoCqNlXusCbXtyVpZZUnhZNOkc5LaXXKDSMJA3/DTjdGqgt++5zfFb1b4Wf4FdPSuI/FHh2j7OC1OyKfmEqonJeS+bGEUIaLAfEnvS8puT3HYVqCwiyo2QqFHFCGVRHmFvLxW4S0vIKyGuOBDURWNyOOoT8JZWxxrYaZZf6jKGmh9awykaG9/YeSWlZZx3H66aPWxsbCaJugA9n1LGicuQviVQWEVfiI4BWqH3ZmXVx7IzZXOcbAe9adMYrLMR6qc3hBrWc0BvyHIx8yNp7OzAkHj3jvUc8rDKVSUtSYSHhCwMZRa6hYThn4aL87H+uFQO7/AE5e5/Qpig+Wl/OP/XK0iq/9OPuX0BhqobW4kxiKxBT3TyysHI66vTLJhSylhPIhbsipoDTN1SZrHiLwddQsyoi+AsOrnF7h0FTG7U796BKucdhuu6ue7LSYiwaDXwVKiT5NS6uuOyAZ8Qc7uCPGmMRKzqpz9hjHTPfsPaVt2RiCjTOb2QbDhX4xQJdT5DkOh/MwuOjY3ghO2TGo9PXHsa9juWPSN+gWjjsqk8moxwaBZNkJUIAzVx2b5n9g+KPGn8wnPqW9ofEDc65uTc8yjpY2Qq228s1ip3O7hzKxKaQWFUpG5yR6u8zuf9Vqx6U9kGfh1LMvnz+iF1TiRN8gy9/zvqTEKEvW3ELeslJ+ht9QnDKnMLOuXNGnG4+IQr69LyuGMdJdqWJcr6BkcoOvnc/buQXHA1GxSWTaywFwHYxC21M6wuKVovyHXTnTzKtSeMC1FcVKTx/c/ohLJW9rKB8UVVbZZUupxLSketw3FYPQZZ3UkTnU5ijNx6+YtaXOJGh7V1h1PWop8iNl01P1ngIp6SJ/3OeIm2mzmTs+sLtsHc7XKHJOLafYNG5yhY1LywbyRQwwyyejRykVksQBbswFxFrDYZQPaq5InOc1HW16KYn6OxRzVzQ+NvVvdIertdoGR5DbchotN4QfqNUKXvxjcf0NBTzGCQQMZeeWFzBq1wbFK4EjndgWcsVnOytSjqb2T+aAajBaaq9Hk6hsd6swyBhID42teT72AXGo1RIWyims9gVsZJtN52TAqrD6aojeW00cBhxFlLeLTPE6RkZLrAdqz734EeKMpSi1l5zHPyF8yiVxQ0L5ZKfqYYZIKumjiy3zTMdKxsoeLdrsl2/MK4KaSlltNP8ATbYzrl5gnSh9P6V6H6FFFH6RBGZ2gtdkL4y+xtYdkuGhW6VLRr1Z2e3xMtth+Osoo5HUzqZlO8TwNjdG0t6yB8jGyOe//VLzc31HMFBjrmtWc7P4jFcnXHUnzn/gw6W1lNEZ6c0whljdGadzGn5WM2zOe7Y6ZueotuCrrqckpJ5Xf2GquompbsK6PmBtJBM6Fkrp6kQnPrlbcjsjnp70KyDUmvIO7pWSeJYSWdg+Wmp6aOqeYGSdXUhjRJwa5kZtfewzFD3eDUZztlBamsrt+p5SqnDnuc1oYCbhrdm9wWsHRgnFJZyb4U/5aL87H+uFlolr/py9z+h3Evw0v5x/65URqr1I+5fQEOis1wDYlBmYeY1CLTPTIX6qrXWwTD5mWDTui2wlnKFumshpUXyWqcOB1aqhe1szVvSKW8RXNE5p1Fk3GSktjmzhKD3JTRZnAXUnLSsl1Q1ywO4aJjfrSMrZM60OmrgdmrWM4+SqNUpFz6iusBmxUn1RbxTEemS5E59dJ+qgR9U47koyriuELSunLlmVytYB5Z6lck9GdUIQhREazsJJIyxxaeGoPNvD4J6MtS1I5EouuTg/09wTBkaMzrW5k7HkAhT1N6UMV+HFapfPz9xhPiJPqafz3f4QiRoX93wQGzrG/U+L/ZC6V19dSTxO/kmYrAjOWrd7+0rl8+SvJnBIZ8rgRuPtZSUNUcMkLdEk0ejgkDmhzdj7vZ4rmzTTwzvVzUoqUS7pQNz9Z7llRb4NOaitwvGakfenJ1Izx7U0wGnALahs/YxSq5an5N/rulg81JTy5iLE+zceO3BNqdenIpKm7W1jJ6nBKInC6xr7gGaA95AfGfZsgWWpWKUfIzHppO1Qntk9Zh1bJEzDIoyQyQPa8ANNw0ttckXG52Sr3y2blVD+rtxjHsNfvrqJvRL9Z6dNe2T1Luv6+m+VVtncv+l4kfF40rz/AGEHRSJzcQY14s8OkDhpo7q33203WpcDXVNPp21xt9UH4PjEs9dCx2UMY+WzWNyi/VyC55n4lU1hAraYwolJctLn3oYQVhlFPLKRmZWuiBGgylrwARte+XVUBlDQ5Rj3in9BbJSOpo5evswS4tFJHcjtM66N5dodgGu8kwpa2sdotfJi00m1pedgPEejJFVUVUzXtPptL6NZzbSB00eclupNh4bHktxuWhRXk8/AFGLk9jDp5h9dNOWzuLaH0iIMc7qsrOsyxZrCzjrI/QnitdPOuMfRXpYf8lqOVyEY9hEho3Q1zheGrhhpqhwbmfDI+Njy4ZtQGF5sT8wX2uqrmlPMO6ba9plsy6T4XOMPmZV9t9NOxlJM6wfLE4sDgbEk9ku3/FF9W3V1WR8ROOya3XtJGLlsjLAaCaXD6JsLS50OIh8oBAyx3cSTc7Wc3zUnKCsk5d47e81iUD0FVizo466WBwu2say9g4X6uJrhrpuCEq4cZ8hquMbJQjLy/dnh5JS5xcd3Ek+JNytYOtHZYCcKd8vD+dj/AF2qmtirH6Evc/ocxJ/y0v52T9cqsbF1S9Be5fQHL1MG9WTNznWIC1hZMNyxhCGdhadRZdCLUlscWyMoPc3p8Sc3fULE6FLgPV1koc7oYsmjlCVcZ1sejZXcgSow8jVhRoX52kLWdK4vVADlnk2JKNGEOUKzts4kzNkROwJW3JLkGoSlwGQ4a476IMr4rgah0c5chkWHMG+qBK+T4G4dJCPJv1bO5D1SC6KwtCGSKEIoQGr6bO3T1m6jv5j2otU9L34Yv1NWuOVyuP4Ez25hcb8P2gp1PDOXJallAzdDw+HsRHuhdYi9zu59u6nBOWMMLexhOaw4hx94ul71KS2+A90koVt6viYCkjklOV1m77bnjlW/EnXWsrcF4Fd170vC5/6C4fkZMh9R2rSef208kGX9WGruhmH/AI9uh+q+Pv77BUtPmIN7HYm24307wUKM8DE6tTz38/v2jfFWANptP/itF+P4aZYy3krp4pOXvf0QCqGR1g/RmoqGZmuDIifnF3bI02G4B+hVqSEr+phXLD3938gmPYJU0xY1x7BuI3MJtfS411aftrZbjKPLMwujamoLHsHcfQusaCOuj3J0klG/+6sOSZhdbTjdP4L+RRJg1QyobC45ZHnsuzGxvfXMNefC6mVgZV9cq3NcIat6D1QNxJEDzD5AfPIq1IX/AB9Xk/l/Iknw+Rs/oxcC4va3QuyZnWsdR/OGtlrO2RqNkXDxPZ+oR0p6L1EcJkmkY8NIAs97iMxsB2miwJsPJbpsxLYTd1V3oxWH+gfRdDqx0bM8sZsAW3klNgQCN2aHw5LFkouTaJT1tcI6ZR39yFvSjonVjqWumYWyzNhA6yUjO+5aXAt9UZTzPcjdNZGOW0L9VdC3GhYx7hVUdFKv0uOidLG6QxdYwukldG1vaFhdt2nsHQN5JhXQ0a8bCqeODTEOidTHLFBUTBxkyiPK97mDM7JbtAEW02GxCH+IjjMEM01RnFzk3t5BEnR2annFM17nSSAaRyOAIN9zZu1iShO3V6WEM1ql16nnHtHJ6BVLI7MdGeORrnAEjkSLE+NkN26nlmY9VSnhRweVfcEgixBIIO4I0IPet4HdWTfCXffEP56L9dqtrZg7H6D9zJiZ+Xl/OyfruVJbF1v0V7kCmRWomnPAEcQId3I/gJoT/FuMvYGNkjlHBBanWxtSruQBV4aRq3ZHrvT5Eruja3iA6tPEFMbNCe8WG0+JEaO1QJ9OnwN1dZJbSDWmN/JAanAcTrt3NXSMYOCyoykwjnXWgObFfxQjR6fzFJ9d+UClrHnj5I8aooUn1Fku5j1h5lb0oFqfmesXIPSkUIdUIRQsUYlT5XZhs7fud9f0pumepaX2OZ1VWiWtcP6/8iyVmunH6U1F7HPnHL27mlHTukNhYW1JPD4rNk1Ddm6KpWvSgz0Vrbh2p5nkdiOX1IPiN4aG/AjHMZfH2AULSHXadjofqR5NNYYnXFqWYvjuGzNa9lydeZ1OYcPqCBFuMsLgbmo2V5b39vn5f9BOHVGduvrN0PO3P3e5Cuhpe3DGOlt8SO/KHuM+rTf7M3+9mQUbp5n/APZ/RC4FQYR7jF3ubhcORxbcRAlpsSCCSPaQsL1jk1xUurln2i/EsaikooYcznTNMd7hxPZuCS87mx3vxVpPOQ0KJV3SkltuMun9TIySm6t7mkl/quIuQ6O1wPW3271I8MB0MIyjLK8v3DekdvTKHnnk8uxb9qyuGD6f/Rs/QQ9OaiVtVZj5AOrZo1zgL3dwBWo4wNdFGLq3S5AOiTDJWxFxLiC55JJJ7LDa5PflVy4DdU1Gl4PW9JZRNRVg36ovBA5xZJLe5VWvTRyo/wBOcX7PqeX/AJLq+WSpmzve4dTo0ucWjttGgJsNEx1EFGCwVdLU84OwzufXtBe5zW1ZsC4kAtlIFgdBbZL7o60oQfTZSXq/seiqoG/deJ9u0Kci/d8r8VNb0aexzo1xfTOeN8/wX6UwdY6klaLmKsYx3c10ga4/8zW+autrdPyMVOUdUfNfsZXP3Ztl0FJcO4Xz2t5FXheFz3Ky/BxjbPIgb0l6jFZzO6QRAvZpneNA3KAweH080XwtVaxjJreVaio/qeax3Eo5KiWSPMWPeXNORwvfW9iNNbrca2lv9RquxRgk08+4zwib74gs134aLgf841XKGz3Rc7MxeE+PIris56+bsP8Aw0nD+e5SMFhbokLZKK9F8GcEodoQR4rMotboNXOMtmsA1XhxGrfJFrvXDF7uka3iLtQeIKZ2aEN4sOpcTI0dr3oE6E90O09Y1tIMfGyQaWQVKVbGpQruWULamhc3bUJmFykIW9NKHAM1xB00RGkxdNxex2SUu3UUUi5TcuTSGkc7gsysigkKJyDosMHzigS6jyG4dEv7mbehRofizDfhqholToEUIRQh1QhSWMOBB2KuMnF5RmcVNOLPP1EJaS13/kcCujCWpZRxbK3FuEjGCUscHAbb963KKnHDA1zlVPUuwRUvMguT4cB4IcEobIPbKVqy2Z0sLt9m8T3cwOK1OUeO4Omub9LsMKilawBwO25PI8Ql4WOT0setojXHWv1BTmY4SAabEcx4cEXaa0MX9KuXipe89TizwWUpGxpWEf1sySw02mPdPJS1Nfm/ZC9QYPb41/7XB4Q/qlD7nMo/93L9TzdVhEkUDKlxaY3lgABJddx0uLW4c1uK1PCGp9XWpOG+T3nSTH46aWFr4i/rC7tDL8mGlgJ1/wBYHfgswhqTeeDkVVTsyoi7GqMtxGmkzucJDYNJ0Zk3y8gcwPjdVn0cDNDz09iNOknSx9LN1Yja4BrXXc4jcn4K4wyYp6WNkNbeBB/Jq8SVUrgNGR7973i1vY1yJbBxism+rvUo6YjvojQTZa5k8bmCeaR7c1u0Jg4G3sA81LZL0WnwhOT4PM/yRRuFRLcEfI29vWNuEbq2sI3ZF4yUwyO2I6Em9a89wvMdEGcspbdjpRhp6Z4fbPyPX1Tv/Voh/Rz/APqhY9HIpF/+K17f4N8IlEk1XC75k7ZG+bXDydGD7VTWyM2pxhCa7rH38SoP/qn/AA3+JTsa/wDif/r9gbBqtorqmEsBzvc7MbaZRta2t781HwEuhnp4TT4R5bpI0elTW/HP7FaOj0v+jH3A+Fj5aL87H+uFYS3/AE5e5/Qrif4WX85J+uVEVX/px9y+h5Wue7OSdOS6VSWk4/USlrbZrS4kRo/Uc1iyhPeISnrHHaYZLCyUaIMZSrY1Ouu5ZQrqaJze8JqFsZHOt6aUAdkhadDZFcU+QEZSi9hhT4jwclp0d4jtXV9pms1Mx4uFiNkobMLOmFizEXuiLHajRMqSmthF1uuW4a7EgBoEDwG3uOPrIpbAstc88bIsaYoWn1M5GHWu5lE0oDrl5nrFyD0pFCEUIRQhFCAeI02dtx6zfeOIRqbNLw+GK9VTrjlcoRyDj9rJ9M48knuE4Y5mfKdR82/PwQrtWnK/UP0jr8TS9/LPmHOeM+VpBv5A8Rf328UBL0cyHXJa9MO/wT7/APRx0WU2cbi2hOw5gDh8PBRS1cFOGh4l+n397e4HfJpl4cCeI7h+3wRFHfIGU3jSuP2HFdPl9EjOxooyD39fUfsAQ5wynP2/wV0dmiTrfnsUQTpHt8MrqWoo2088oiczKNXNZ6vquaXaG40I8e4obTTObZC2q52QWcgfSusg9HhpIJM7WOYXOBzdlgNhn2JJNyRyPNbhJpuTRmnpZzk5z2+pT+UStjmdD1UjXholzZXA2uY7Xt4FSv0eTXR1WJSTWOBj0hxymNVRuE8ZDZH5yHt7AIbYu10Gh3UjXJp7AK811zjJbvBj0gpMLq5etkr2tOUNsyeEN0vrZzSb681ut2QW0fkwKlNR09gLovV0lH6d1dQx2oEOZ7C6QMizDLawdd7yNBwW7VOenK95GpSxsa9C+lE7pHemztyFl2lwjYA8ObpdoFyQT5IdsYcQQefSSUNSR3o5X08FXUOfLEyM5wxznAB15A4WJ30WNMpcLIbq0/AhlY/6PLxYm04iHl7REKvPmv2cnW3zk7WtrfZNKrEPbgFZdKVenthHparGKc4zFOJ4zEKUtL87cgdebTNtfUeawoS8Fxxvn+BXS8AGFY/HHi80hlb1EpkZnzjJawe119vWba/85alVmpJLf5mnqcEmG4h0qhjxRkrXtkgMIje9hzBty7bLvYhtxyKxGhuv2mlKTq0e3I8p6jDmTvq21THOIN2tkY+2a1yGN7VzbY8ygSjNLDQRSutrVSjsvv3Hg8UxJsk8j72L3kgcr7Ana9rLarljODqVyhXFV53RrhLvlofzsf67VloJY81y9z+gv6QV+WeQD/Ovv/zlMVU6lkTn1WiMUvJfQwzMlaqxOthM13xFlVRObwuE1C2Mjn3dPOHuB4pnNOhRZQUuReNkoPKGNPiQOj0rOhreJ0KusUtpmk9C14u1ZjbKOzN2dNCxZiK56dzdwm4TUuDm2VSg9zkMxbsVcoKXJULJQ4D46prxZ26Xdbjuh6N8bFiRhPRm/ZW42+YCzp3n0S8WHcyqlf5G4dJ+Y39BZzQ/GmH/AA1Y5SR1CKEJdQhFCEsoQ6oQS4nTZXZh6rvceI/b5p2izUsPlHK6qnRLUuH9RXI2xTSeTnSWGGQPuBbcey1tigyWHuNVy1LbkO1eO0dR5AjY24+3nwQNoPYc3sXpPdfBAs09xoNefAH9qLGGHuL2Waltz9/EK6Rk2ozx9Cj8+vqFuvGJe9/RCeXz3ybUNTnbfiND4pS2GiWDs9Pd4sM9+56KgwRslLLOXEOZnLW6Wc1gYXE8fnILe+DM73G1Qxs/3Cqjo41kbZM7sppZJzto9jGPy+BD/conl4Brq3lpriSX6b7/ACFnRnDG1vXDO5mQNDCAO094eQNRyYUWUfDw2D6jrHxAxwfozBPDTkzSNnqmzmMZWujvCTfMfWFwB70adsot7bLHzEpSeWZRdGWOMbs7+rfQGrJs24eAbx7WtfL36q5XYz78G4pyeF54Gx6NUkc7KcSTOlc6MElrMoa8Akg87HklpWyksjFLlGDswu5efCqVroj1srmSzPpxZrC5szXhmutsnrXO4sqUZPPs3NvrJrsm+fZjH7C+v6PRO6/qppi6GeCA9YGBvWSzmJxAbrlboRsjwnjCaXDfyyKSunLd9y7OidI6p6llRIXNfLHK14YJAY4y/rGN4xm1rnmrd01HOAWru0C4V0epar0h1PJMRFA2RvWNY0ulIl7LrfNsxnfqeS1K2UMKS5f8GpNp7hdD0LieylfJJIOvhfK8DL2csbXhrfY5Zle05JdmZct9wbCej1NVNqH08s2WKJjmZ2xtLnnrSW2F+zZjbHmSrnbOGMpZCJ7pS2T+gXD0ZpeuliM1QHQxCo7DY7GPq43EB3F132t3brDtlp1NLfYtSllaPP73ErKKNjnFlyMzspcBmy30vbja1+9Zna5LB1KOnUN3yG4fKBPDfjNGP+tqGotp4N3zUYNPyf0FvSKiJmmc3X5WS4/3ymabcJJiFvTNwUo+S+giY8tNxoU20pLcRjKUHlDSmxMHR4Ss+na3idGrrE9pmlRQNfq3RYhdKOzN2dLCxZiKZ4C02KcjNSWxzbKpVvDOwVTm7FVKtS5LrulDgYxVjHizt0tKqUN0PQ6iFixIxqaDi1Ehd2YK3pe8Re5hG6YTTEXFxe5vDVuCxKpMNDqJROSVbjxUVUUVLqJyMusPNb0oFrl5nrVxz05LKEIoQihCKEJdQhnPGHNLTsfdyIWoycXlGLIKcXFnnqiEglp3H2uulCWVlHCtrcW4syp5cp+lanHUgdVjhIYOb847cQNrftS6fZDzT9Z/f8mFVK2+h148u5bhF43A2zintyM8ddpSE/kUev8A99QqhxL3v6IHB4bbFcFRkfmHqnR3xWpw1xx3N12+FZqXD5Pp+CzQCKngdJaSaGps0C7XCW5Bc6/Z7MYtzXOcZbvHBu+z05SXmvl/2VOJNdBPE4i4wyOVmo9Z0D2SD+70RFDDT/ywLSeZZ9op6EVEUMEbpZCwyVzcuVubMGx5AHa9lt5HdruRbk5SaXkSW7HeH4f1bqWQuY2OlkxDOS5os18j2x2F7m4HBBlPOV54L0t5XfbAGyVjcKz3Ac2J9P35XTNOg39ULO7mO6VHqNPCyn8EG4u+oNbTOtGKYSwnOHR5y4sy2Oua1yVcYx0PPIkrMRwu+c+0UYtVCQ0Uh6tpZiL4yGdlmX0hpDy0aZzlGvG5ujQWNS/x/YpbZS8gaYsLcUaXtYHV0AL73ytNU4OfwuADffhutJPMPc/oUnjGD0FJDlqKd87ojVXqIxICy81O2N3VSvAJAPqjfj7EFv0Wo8bfozLa7C3AJpoZZ3VYibI/0AERmMMyPqHxXAYbcXX+1yWJSSUeN/oW0sbe0dxyt62FrHNIjNVE25bb5OGONoN+Zag74efYZEOEy1EJqZakRtkLqF5EZjymMSva+wjJHqh1/HvRbHDCUeNw1dTskkvb9BjVtY2srMtreguaDca2jiAAPsS+W0huuOKYPH96/c8ddaOoCQF3pdPfbr4bf1rU1Xjw37mcrrNWrfgrW11qiYO266X+8crdOYpryKo6nTiMuDs9Ix4uN+YQo2yg8MbsorsWUJZ4S02KejJSWxybK3B4ZanrHM2OnJVOqMuS6+onXwMIqpkmjt0tKuUN0PQvhasSMqjDeLVuHUdpA7ej7wFz2EHVMpp8CEouLwzenrHN7whzqUg1XUyhyH5WSjvS+ZVsdxC5bC6opi0pqFikhC2mUGbwUN9SUOV2NkGr6XKyzf0SPmh+JMP4FQ7SJ1iKEIoQihDhKshLKFEULAcUpswzDdvvH1fFHonh4Yn1dOuOpcr6CKVvFPxZxpruW9Idly3096rRHOS/Glp0plIoi42CuUlFbmYQc3hHo8ejAbSZuFFGL8Pw1RsloSbzjz/gahCKypCI+5GBMllZZaOIuNgFmUlHk1GLk8Ia01CG6u1clJ3OWy4OjV06hvLdl6qpawXIuRsBsOVzwWa63N7G7r1Wt935CqoqS5xLjawsNbW8r/bim41qMdkc2y+U5PU/v9M/fcHIHGw9nBE37AW/M6PYL9w58Dy71CZ+/v6kI8LC4H1c1CfQjQOGW2l+yPbfmpv3LT32x8PqcH209yszlhtFh2bVwsOA4lAsvxsuRyjpXLeXA4jjAFgAANgEk23ydSMUlhFso5KF4FOJVRDrDgm6a01lnN6q+SlhDDA6sSTQg6HrovdI1DnW4ZaL8eNtbT5wA4/h7hPM4agzSnze4o9V0WkmKz6WaipLfYVxzObsUaUIy5AQsnDhjKkna/R9rpayEobxH6bYWbT5K1eGjdnkrrv7SKu6NcwFjmkHXRNJpnPacXuE09c5veEKdMZB6+qlDYMEjJN90HTOsa113cglRRFuo1CNC5PkVt6Zx3QNHIWm4RJRUluAhNweUM4qprxZ26WlW4PKOhC+NixIDqZCDYHRGhFNZFbZyi8Jg2Y80TCF9TPXLjnqCKEIrIRQh1UQ4VZDihRUlWU2JMQp8rrj1Tt3cwnqZ6lvycfqatEsrhi8hMCLWBoahmQZRY7gDge/7apXRLVudHxq/DWnny8n9/EcYhStqGUzm1NKzJTNjc2SbI5rxLK49mx4PapD0MrD58hRzbbyBfcD+l0X9o/hW/E/xfwJkIh6Lk71NJbumJ/wocuoS7MNXXJ8xePcMYsDa0dmope89d/ClpTcnuPQsjBejGXvwDz0QI+Tq6L+cTUa29jdPFEjB/3RfwAWdamsVp+1/fAudgd9fTKG5JJPpH8P2umVPH9svgIuedyDAgT/AJZQ3O1p/Owyqa9vVfwJqy/b7jn3B/plDf8A2i1rHj2dVNf+L+BNR12Bc6yh/tFr+PZupr8ov4FuWHudGA/0yi5/hwPI5dAp4n+LJq7v6EGAE2Aq6I8h6Qf3VXiJb4fwJly2SYdR9FwNX1FKTy642H/SgWdRnaOcDlFenecZN+4YDB/6RTf1v8KXyOq7/GXwO/cj+kU39b/CqyX4/wDhL4GNVhRA0qaUHvmt/hW4Yb3TBW9Q1HaMvgC1OANlbcVNJccRN/CjQsdb3TF7ZRujlRefcDYfgnVTxPfV0QDJY3u++DfK14cbDLvYFMOxSi8RfwOe1KOzQPV4sDNLsWmWQtI2LS91j5WQn0+yaHqetwlGRhNRCTtMVxtcNpG7OmVvpQFk0LmmxFkzGcZLY5865QeJBFNXObodQhzpUuA1XVShs+Ax5jkHegrXWxuTquQvno3DbUJiNqYjZ08o8A+yLyA3QdSV1tHbJeynuhyjqcbSNKykB7TVmuzG0gl9Ca1QFhuCmtmc/dMLhgB1cUGU2tkMwqUlmTNurjWMzC6KR+uedo5ZQh2yhDishFCEUIcKhRk8LSByQNUR5hYosHpeUL2Q1rDE0jLGx4J6Lyso5M44eGcuoQsxpJsFTaXJqKcnhDSloANX+SUsub2idCnpVH0phNRM1gu7bg0blDhByeF8Ri2yNccy+HdiqrqXPOpsBsBce3UX9tvYm661FbHNuulY8N4Xl97/AC/QHzmx1FtufHhf6UTCAanj2HfA6C2trjXffj3Kv0L9z2IOQPDjrfx5AclPeWs8L7/hHL6bjfgPoH2KsrO2z+/YQm2o9thYDl4lTGSm2t0Wghc82APidvNVKcYrc1XXOx4ih1SUjWDmeJSNlrmdanp41L2+YSEMYOqiyEqEYPJqiLYBL0hZIHRuu3bkmlixYZz5aqZZjwEZ2SjXf3oeJ1MPqrvjh8i6po3M7xzTMLVIQt6eVfuJS1jmbajkpOpTJT1Eq+OA81LJRY7pfw5VvYd8au5YYDUURGo1CPC1PkTs6aS3iC6juRtmLbxYTT1pG+oQZ1J8DFfUuPIaYWSC43QdUoPcbdddyyhdUU5YdUzCxSEbapVvcvT1ZbpuFmdSkaq6iUNuxKlwdqFIJx2ZLXGe6BsxRcIXyyaqE3PYLjHqSKEIoQihDishFCHFCjhCsrBm+NWpGJQFuIU/HiN/BNUz7HP6mrK1AEMRcbAX+geJTEpKKyxOFcpvCG1JThnG54nh7Ak7JuZ0qaVWvaaVU5bbK0uJ48B7FmEFLl4N22yh6qy/oJJH5iS49q9zf/xdPJYW3ByZScnmT3JbjYfsv3338FPYTD5+/wDkswm3v0GvnwGqppFxbx95IDpc24AcfYBxV9yZysv7/Qq12tgBbv8ApJA3Ua2KTy8JLH3yQv3HsvYbd3IK8FOXYIoqEv1Nw36UK21R45GOn6Z2bvZDqKINFmiwSUpOTyzrQhGCxEusmyKFHVCzN7lpIHKWSisyUkaCtJ4MyimhZU0xBu1NQsT2ZzraXF5ia02I/NePasTo7xCVdX/bYVqqNp7TD7FddrW0irunjL0oC5zSN0ymmINOL3N4K1zd9QhzqTDV9TKPIY3q5O4oL11jS8O73g1TQObqNQiwuUhe3pZQ3XANFIWm4RJRT5AQm4vKGcdS2QWdulnW4PKOhG6NsdMgCqp8vgjwnqErqtBlA4A6rcltsDrkk9wgPYOCHiTGFKtFvSWclXhy8y/Gh5HpFzDvEUIRQhFCHFCEVkIoQihCWUIVfHdWpYMygmCQ0pjvYXaTe3Ec0WVinjPIrCl1Zwsp7mLJXBxzEW1tcFpHIciiOKcdufiCjOSliWMfB/wa002b5pB79vYRusTjp7harNfZr78zQsa7gDw+Oqzlo24xkDvw1h2uOWtwPYURXyXICXR1vgHkw19tCD3bXPeURXxyAl0c0tnn5Ac0Dxq4EAacPdZGjOL2QrOqcd5IyPAD3a+fetg32SGlFh+oc/xA+KUsu/tidGjpN1KYzASx0UdUIRUWdUIZvcrSMSkUWgZy6shUlWU2UcrRhgVTTAo8LGhO2lPgD7Tdkb0ZC3pw4No6hrtHBYcGt4hY2xltMtLQcWm6qN3aRc+l2zADsWnkUbaSFd4sYUuI8HpeyjvEdp6vtMvU0bXDMxVC1x2kbt6eM1qgK3tIOuhTSaZzpJxe5cOLgVWEmaTlJbmbYytakYUGWEDlnWjXhSLeiuVeIjXgSPVrknpCKEIoQ4oQihDqhCKEO2UIRQsihCKEOOaCpkppPkwdRt4Xaf5psiK2XfcC6IdtvcYPojuLE87ZXf8AM2xW1auPv5gpdO85WH8n8Uda8t0c13j6yjSlwy1KUFiSf1NGTNOxH0HyKy4SXY3GyD4ZpZZN4MxTsvfKL87LWuWMZBqqGdWFk1WQpFCEUIRUQq9ytIy2ZEreAZy6hMlSVZlsoStYMtlS5Xgw2ZuK0kYbMHi6ItgMlkFkiRVIWlAlPVOYe7kpOtSLrulWxj8nKORS3p1Me/p3r2i6ppXNPcmYWKQhbRKDKwVLmbK5VqRVd0q+C9TVBw21WYVuJq29TXBg12miJgCpbbEAPBTYiUux27lPRLzMmZ3epiJWqZ61cc9OcUIRQhFCHVCEUIRQh1QsihCKEIoQihCKEIoUcKhBXiKapOf1XJvQ+qsW8hen9U3KGHZ1Qh0KFkUIQqEZiVpAmVK0UVKhllCtGGVKsplCtIGzNy0jLM3LQNmMi2gUgR+6KhaXJvResFiz1QvT+uhrUeqk4cnTt9QRu3T6ONLkqVZk4rKNGLLCRNgsMKiyos//2Q=="
                             width="100%">
                    </div>

                </div>
                </a>
            </div>
        </div>
        </div>
    </section>


    <script>
        /* When the user clicks on the button,
        toggle between hiding and showing the dropdown content */
        function myFunction() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        // Close the dropdown if the user clicks outside of it
        window.onclick = function (event) {
            if (!event.target.matches('.dropbtn')) {

                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
    </script>

<?php
include "footer.php";
?>
