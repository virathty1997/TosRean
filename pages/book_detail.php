<?php
include('header.php');
require_once "../config/DbConfig.php";
require_once "../model/Comment.class.php";
require_once "../model/Book.php";
require_once "../model/Rate.class.php";
require_once "../model/BookDetail.php";
require_once "../model/User.class.php";

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$userLoginId = isset($_SESSION['userLoginId'])?$_SESSION['userLoginId']:0;
$limitedDisplayComments = 5;
$commentCount = 0;

if (isset($_GET['book']) && is_numeric($_GET['book']) && $_GET['book'] > 0) {
    $uploadId = $_GET['book'];

    /*update view number */
    Book::update_view_number($uploadId);

    /* get upload book detail information */
    $book = Book::get_book_by_Id($uploadId);

    if ($book){
        $posterInfo = User::getUserById($book->user_id);

        /*Get all book files*/
        $bookFiles = BookDetail::getBookDetailById($uploadId);

        if (isset($_GET['container']) && is_numeric($_GET['container'])) {
            $comments = Comment::getAll($uploadId);
        } else {
            $comments = Comment::getLimitedRows($uploadId, $limitedDisplayComments);
        }
        $commentCount = Comment::countAllComments($uploadId);

        $rateCount = Rate::getRateCount($uploadId);
        $rateChart = Rate::getRates($uploadId);

        $rateByUser = Rate::getRateByUser($userLoginId, $uploadId);
    }
}

?>

<link rel="stylesheet" href="../assets/css/book_detail.css">
<!--<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>-->
<script src="../assets/js/rate_chart.js"></script>
<script src="../assets/js/canvasjs.min.js"></script>

<section>
    <div class="container-fluid d-inline-block float-none">
        <div class="row">
            <div class="col-8 col-md-8 wrapper8" style="height:100%">
                <div class="main-block">
                    <?php
                        if (!$book){ ?>
                            <div class="block-title">
                                <div class="title-container">
                                    <h5>Book Details</h5>
                                </div>
                            </div>
                                <div style="min-height: 500px; width: 100%; display: flex; justify-content: center; align-items: center;">
                                    <div>
                                       <center><img width="60px" src="../images/not-found.png" alt=""></center>
                                       <center><p style="font-weight: 600; margin-top: 5px; color: #bc0000;">This book is not found!</p></center>
                                    </div>
                                </div>
                            <div class="block-title"></div>
                        <?php }else{ ?>
                            <div class="block-title">
                                <div class="title-container">
                                    <h5><?php echo $book->genres; ?> book</h5>
                                </div>
                                <div class="poster-dropdown">
                                    <img class="poster-profile" src="<?php echo $posterInfo->profile; ?>" alt="" onmouseover="doPosterHover(<?php echo $posterInfo->id;?>)">
                                    <div <?php echo ($posterInfo->id == $userLoginId)?'hidden':'' ?> class="poster-dropdown-content">
                                        <div class="profile-user">
                                            <img src="<?php echo $posterInfo->profile; ?>" alt="">
                                            <p><?php echo $posterInfo->fullName; ?></p>
                                        </div>
                                        <div class="all-send">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="checkbox_notify">
                                                <label style="user-select: none;" class="custom-control-label" for="checkbox_notify">Notify to your email</label>
                                            </div>
                                        </div>
                                        <div class="follow-button">
                                            <button class="btn btn-success" id="button_follow">Follow</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="book-detail">

                                <div class="image-view">

                                    <div class="image-wrapper">
                                        <a title="Click to download cover"
                                           href="../images/<?php echo $book->book_cover; ?>"
                                           download>
                                            <img src="../images/<?php echo $book->book_cover; ?>"
                                                 alt="">
                                        </a>
                                    </div>

                                    <div class="rate-number">
                                        <label id="rate-view" class="mark-label">
                                            <strong><?php echo $rateCount; ?></strong> <?php echo ($rateCount > 1) ? ' rates' : ' rate' ?>
                                        </label>
                                    </div>
                                    <div class="download">
                                        <label id="download_number" class="mark-label"><strong
                                                    id="down-number"><?php echo $book->download_number; ?></strong> <?php echo ($book->download_number > 1) ? ' downloads' : ' download' ?>
                                        </label>
                                    </div>

                                    <div class="view">
                                        <label class="mark-label"><strong><?php echo $book->view_number; ?></strong><?php echo ($book->view_number > 1) ? ' views' : ' view' ?>
                                        </label>
                                    </div>

                                    <div class="download-wrapper">
                                        <div class="down select-down">
                                            <select class="select-file" id="select-file" name="select-file">
                                                <option value="">Select</option>
                                                <?php
                                                foreach ($bookFiles as $file) {
                                                    $extArray = explode('.', $file->book_file);
                                                    $ext = strtolower(end($extArray));
                                                    ?>
                                                    <option value="../books/<?php echo $file->book_file; ?>"><?php echo $ext; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="down click-down">
                                            <a id="download_file" class="button-down" href="" download>Download</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="des-detail">

                                    <div class="title">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label class="mark-label">Title</label>
                                            </div>

                                            <div class="col-sm-10">
                                                <label><strong><?php echo $book->title ?></strong></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="authors">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label class="mark-label">Authors</label>
                                            </div>

                                            <div class="col-sm-10">
                                                <label class="mark-label"><?php echo $book->author; ?></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="genres">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label class="mark-label">Genres</label>
                                            </div>

                                            <div class="col-sm-10">
                                                <label class="mark-label"><?php echo $book->genres; ?></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="published">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label class="mark-label">Published</label>
                                            </div>

                                            <div class="col-sm-10">
                                                <label class="mark-label"><?php echo $book->published_date; ?></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="height: 110px" class="description">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label class="mark-label">Description</label>
                                            </div>

                                            <div class="col-sm-10">
                                                <div class="des">
                                                    <label class="mark-label"><?php echo $book->description; ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="rate-report">
                                        <div class="rating-group">
                                            <div class="rate-text">
                                                <h1 id="rate-text"
                                                    class="mark-label"><?php echo(($rateByUser > 0) ? ($rateByUser . '.0') : '0.0'); ?></h1>
                                            </div>

                                            <!-- Rating Stars Box -->
                                            <div class="rating-stars text-center">
                                                <ul id="rate-star">
                                                    <li class="star" title="Poor" data-value="1">
                                                        <i class="fa fa-star fa-fw"></i>
                                                    </li>
                                                    <li class="star" title="Fair" data-value="2">
                                                        <i class="fa fa-star fa-fw"></i>
                                                    </li>
                                                    <li class="star" title="Good" data-value="3">
                                                        <i class="fa fa-star fa-fw"></i>
                                                    </li>
                                                    <li class="star" title="Very Good" data-value="4">
                                                        <i class="fa fa-star fa-fw"></i>
                                                    </li>
                                                    <li class="star" title="Excellent" data-value="5">
                                                        <i class="fa fa-star fa-fw"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="total">
                                                <p id="total-rate" class="mark-label">
                                                    Total <?php echo($rateCount . (($rateCount > 1) ? ' raters' : ' rater')); ?></p>
                                            </div>
                                        </div>

                                        <div class="chart">
                                            <div id="chartContainer" style="height: 160px; width: 400px;"></div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                </div> <!--main-book-->

                <div <?php echo (!$book)?'style="visibility: hidden"':'' ?> class="comment-wrapper">
                    <div class="title-title">
                        <label class="mark-label"
                               id="totalComnent"><strong><?php echo $commentCount; ?></strong> <?php echo ($commentCount > 1) ? ' Comments' : ' Comment' ?>
                        </label>
                    </div>

                    <div class="bar"></div>

                    <div class="comment-container">
                        <form role="form" method="post" id="form-comment">
                            <div class="write-comment">
                                <img id="profile" style="border-radius: 50px"
                                     src="<?php echo isset($_SESSION['profile']) ? $_SESSION['profile'] : '../images/no_profile.png'; ?>"
                                     alt="">
                                <textarea id="comment-textarea" name="comment-textarea" class="form-control" cols=""
                                          rows="4" placeholder="write comment here"></textarea>
                            </div>

                            <div class="button-comment">
                                <input class="btn-cancel" type="button" value="Cancel" onclick="doCancel()">
                                <input class="btn-submit" type="submit" id="send" name="send" value="Comment">
                            </div>
                        </form>
                    </div>

                    <!--Start generating comment lists-->
                    <div class="message-list" id="message-list" name="message-list">

                        <?php foreach ($comments as $com) { ?>

                            <div id="container<?php echo $com->id ?>" class="message-container">
                                <div class="profile">
                                    <div class="follow-dropdown">
                                        <img src="<?php echo $com->profile ?>" alt="" onmouseover="doProfileHover(<?php echo $com->id.','.$com->userId ?>)">
                                        <div <?php echo ($com->userId == $userLoginId || $userLoginId == 0)?'hidden':'' ?> class="follow-dropdown-content">
                                            <div class="profile-user">
                                                <img style="width: 35px; height: 35px; border-radius: 50px;" src="<?php echo $com->profile ?>" alt="">
                                                <p><?php echo $com->fullName; ?></p>
                                            </div>
                                            <div class="all-send">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="checkbox_notify<?php echo $com->id; ?>">
                                                    <label style="user-select: none;" class="custom-control-label" for="checkbox_notify<?php echo $com->id; ?>">Notify to your email</label>
                                                </div>
                                            </div>
                                            <div class="follow-button">
                                                <button class="btn btn-success" id="button_follow<?php echo $com->id; ?>">Follow</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="comment-description" style="width: 100%;">
                                    <div class="comment-show">
                                        <div style="width: 100%;" class="name">
                                            <label><?php echo $com->fullName; ?></label>
                                            <label id="created_date<?php echo $com->id ?>"
                                                   style="float:right;"><?php echo ($com->isEdited == 1) ? 'Edited ' : '' ?><?php echo $com->createdDate; ?></label>
                                        </div>

                                        <!-- Rating Stars Box -->
                                        <div style="display: flex">
                                            <div <?php echo ($com->rateType >= 1) ? 'class="com-star"' : 'class="white-star"' ?>>
                                                <i class="fa fa-star fa-fw"></i>
                                            </div>
                                            <div <?php echo ($com->rateType >= 2) ? 'class="com-star"' : 'class="white-star"' ?>>
                                                <i class="fa fa-star fa-fw"></i>
                                            </div>
                                            <div <?php echo ($com->rateType >= 3) ? 'class="com-star"' : 'class="white-star"' ?>>
                                                <i class="fa fa-star fa-fw"></i>
                                            </div>
                                            <div <?php echo ($com->rateType >= 4) ? 'class="com-star"' : 'class="white-star"' ?>>
                                                <i class="fa fa-star fa-fw"></i>
                                            </div>
                                            <div <?php echo ($com->rateType >= 5) ? 'class="com-star"' : 'class="white-star"' ?>>
                                                <i class="fa fa-star fa-fw"></i>
                                            </div>
                                        </div>

                                        <div class="comment-text">
                                            <p id="comment_text<?php echo $com->id ?>"
                                                   class="mark-label"><?php echo $com->commentText ?></p>
                                        </div>
                                    </div>

                                    <div class="operation">
                                        <div class="like operation">
                                            <div class="react">
                                                <label class="mark-label"
                                                       id="like<?php echo $com->id; ?>"> <?php echo $com->likeNumber; ?><?php echo ($com->likeNumber > 1) ? " likes" : " like"; ?></label>
                                            </div>
                                            <div style="margin-left: 5px;" class="react">
                                                <i style="color: rgba(45,45,45,0.83);"
                                                   id="like_icon<?php echo $com->id; ?>" class="fas fa-thumbs-up"
                                                   onclick="doLike(<?php echo $com->id; ?>)"
                                                   onmouseover="doLikeHover(<?php echo $com->id; ?>)"></i>
                                            </div>
                                        </div>

                                        <div class="dislike operation">
                                            <div class="react">
                                                <label class="mark-label"
                                                       id="dislike<?php echo $com->id; ?>"> <?php echo $com->dislikeNumber; ?><?php echo ($com->dislikeNumber > 1) ? " dislikes" : " dislike"; ?></label>
                                            </div>
                                            <div style="margin-left: 5px;" class="react">
                                                <i style="color: rgba(45,45,45,0.83); margin-top: 4px;"
                                                   id="dislike_icon<?php echo $com->id; ?>" class="fas fa-thumbs-down"
                                                   onclick="doDislike(<?php echo $com->id; ?>)"
                                                   onmouseover="doDislikeHover(<?php echo $com->id; ?>)"></i>
                                            </div>
                                        </div>

                                        <div class="edit-comment operation">
                                            <label <?php echo ($com->userId != $userLoginId) ? 'hidden' : '' ?>
                                                    class="mark-label reaction"
                                                    onclick="doEdit(<?php echo $com->id; ?>)">edit</label>
                                        </div>

                                        <div class="delete-comment operation">
                                            <label <?php echo ($com->userId != $userLoginId) ? 'hidden' : '' ?>
                                                    class="mark-label reaction"
                                                    onclick="doDelete(<?php echo $com->id; ?>)">delete</label>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        <?php } ?>

                    </div> <!--End generate comment lists-->
                    <div class="show-more" <?php echo ($commentCount <= $limitedDisplayComments) ? 'hidden' : '' ?> >
                        <label id="show-more" class="mark-label more">Show More</label>
                    </div>
                </div>
                <div id="snackbar">Please click follow button first to get notification</div>
            </div>

            <div class="col-4 col-md-4 wrapper4 " style="height:100%;">
                <div class="side-block">
                    <div class="block-title">
                        <h5>Recommended book</h5>
                    </div>

                    <div class="recommended-container">
                        <?php
                        $bookType = ($book)?$book->genres:'';
                        if (empty($bookType)) $populars = Book::popular_book();
                        else $populars = Book::getRecommendedBook($bookType,$uploadId);
                        foreach ($populars as $popular) { ?>
                            <a href="book_detail.php?book=<?php echo $popular->id ?>">
                                <div class="side-b-container">
                                    <div class="side-b-img">
                                        <img src="../images/<?php echo $popular->book_cover ?>" alt="book">
                                    </div>
                                    <div class="side-b-info">
                                        <div class="b-info-group">
                                            <p>Title : <?php echo $popular->title; ?></p>
                                        </div>
                                        <div class="b-info-group">
                                            <p>Author : <?php echo $popular->author ?></p>
                                        </div>
                                        <div class="b-info-group">
                                            <p>Published : <?php echo $popular->published_date ?></p>
                                        </div>
                                        <div class="b-info-group">
                                            <p style="font-size: 12px;"><strong><?php echo $popular->view_number; ?></strong> Views &ensp;<strong><?php echo $popular->download_number?></strong> Downloads</p>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>

                <!--<div class="ad_video">
                    <iframe width="430" height="250" src="https://www.youtube.com/embed/KaYPAuTEqAQ?start=60"
                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
-->
                <div class="adveritisement">
                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhMVFhUVFRYVFRgVFRUVGBcVFhUWFhUXFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0mICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKEBOgMBEQACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAEBQACAwEGB//EAFEQAAEDAgQCBgUFCwcLBQAAAAEAAgMEEQUSITFBUQYTImFxkRQygaHRI0Kx4fAVJDNSVHOkssHS4gdiZJOUs8I0U1VydIKEkqKj8SU1RIPD/8QAGgEAAgMBAQAAAAAAAAAAAAAAAwQAAQIFBv/EADkRAAICAQMCAggFAwQBBQAAAAABAgMREiExBEETUSIyYXGRobHwFFKBwdEjQmIzouHxJAU0RHKC/9oADAMBAAIRAxEAPwD51ZegMYCIdkOXIevgushCq0YNom6IcmGgtjUBZyESO2UyXgs0LLNIuGqsmsFgFRrBC4BVjJMpFHVQC0q2zDvSM31QK0q2jDvTBJngo0U0LTkmZsiLtgtOSXIOMJS4OSRlp1UUlLgqcHB7lcy1gzqJmVYJqZpG9U0bhIkgVIuS3OsCjLSNhEeSxqQXRJEUIdULLZVWS8GTgtIG1uQNUyRLc2hbe4WJPGGFgs5QFI1GTFJLBxoVspGhYs5COJkQtIE0RWQhCovBxWZIoWWIuqL5KELRhrBxQoihDZrFhsMomrQsMIlggUIjisoYUcbctylrG84Q9RGOnLJJbhsrjnuSWM7FW67BW9ilu9iwbZZzk0lg0DVWTWDuUqsmsMEqmO4AosGhW2MnwgcwO/FPki64+YDw5+RUQuvayvWuSvDk3jBvHSgb6obsb4DRoS5CAOAHkht92GS7Izq6c5dfZxsRwK1XNZ2MXUtx3BWYdKRcN96K74LuLR6S1rKRcYXLyHmq/EVmvwN3kWGEyd3ms/iYG10FoQzDH21shvqI9g8ejnjcEnjMbrI8GprIrZF1SwyCqda11XhxzkivljB2OIu1UckiRg5blupcOCrVFm/DmuxvSMDjYmyHY3FZC0xU3hvAX9zBzQfxDGvwafct9zBzKn4hl/g4+ZSSkyEOB04q1bqWDMqPDakhfiMVjfmmKZZWBLqq8PIHHujS4FY8jSKEOZ3hKynpkdCFanX7QGqhIR4STE7q3FgxRQBdpVNGkzpaqyW4lC1Xkw0cVlcFsyrBrJLBTcmETIFMk0oYxUTilpWpD8OnkzWWkAaeazG1thJ9OoxyAhHEypVmQmmizcUKctIxVDWHMpm+KA7GORpijdkfIIbkGjDyN2xhDbDKKLZQpll4RLBVlkwissjW7/X5LUYuXBmc4x5ApqonYWHv+pGjWlyKzvb42BwigPcbNpjx5XA+J2Cw7F2CqmXc19HHHb8UfHj4rGthPCiufgaSR5gQdPtusqWHlBJR1JpgsdYYwWuGyK6lN5QtHqHUtMlwatxA/wCbd5fFYdPtQVdU/wArCA91r2A9qHiOQ6lNrOAWeaQtOU68LD4osYwzuLWWWOPovfsIp3OLjmNzxXQiopbHGslJyep7lGlWzCeDeOWxusOOUGjPS8hzcSP4oS7o9o6usfkCSPuboyWFgVlLLybQ1728bjkdViVMZBYdVOHcc0VR1jc1rJKyGiWDq0W+LDUbSC4ssJ4YWSysC2siuMvLZM1yw8iF8MrSJntLSnU8o5Uk4sZ4c+9wlbljc6PTS1ZiWq47tOiquWJF3QzETvTiOXIotAywkVNGlJosHqsGtR0tNr2VZXBeHjJaGAvNgpKaissuup2PCDvuaGtu4pfx3J4Q5+DUI5kwKzUfLFcRPQMXOZ20VljuFcXgzOOUJnDVPLg5LWGVIURGHYZHcoF7whvpI5Y2awBJts6aikXuqNA1VVBlr/O2CLCtzzjsAuvVeM9zE1/aLbbW27xdb8H0cgn1SU3HBJKsnbT6fqUVaXJUr5PjYGP25ooAIipSd9B7/qQ5WJcB4USfOwVHGG7D7eKC5N8jEYxhwUlaTxsFqLSMzUn3LNbYWWW8mksLB0BQgNWxfOG438PqRapdmL9RD+5dgeoxBxHZYdN+SJCmK5YGzqptejECmxSQgjQDwR49PBPIpPrbZLAM6dx+cUVQiuwu7ZvllFZgihDt1WDWTSOMn1dVlyS5NxhJ+qExUEjuFvFDldCIeHS2z7YDocHHzjfwS8uqfYcr/wDT4r1mMYYQ0WaLBLyk5PLH4VxgsRLuWUaB52XRIvAGyORZiVPpcJqmfY5/VVbZQHQy5XhGtjqixTp56JpnoHR3XPUsHbccnnqyKxK6Ncso4l8MSZ2lw979dhzKqd8YEp6Sdm64D48GA9ZyA+qb4Q7H/wBOivWZvaJgtp9KH/UkGxTWsGbqhrtA0n2LSg1u2DdsZbKJeHMNmH3LMsPlm4alxEqKJ7/XNhyWvFjH1UZXT2Weuzv3JbzKr8S/I1+Aj5mxesYCuRrEbhYlsEg9SFNZDZxTlcsxOZfW4zMchPBbyC0tjWijDRc6JS2Wp4R0unioRyzU1sf4wWPCn5BPxNX5i7qhoF7+Hf4LKg28G3bFLIqqGdY/M7wA5BNweiOlHNtj4s9cv0RcCyo0kkbxUxO+n0+SHKxLgPGmT52C44mt2GvPihOTlyMxhGHA6nMETIc1PnL4RI53XPZqXvbbKBb5iwAXiTlLEsJPHC9gFNi9I3QUlzyFRJ7zbREjVJ99gc5zjsp5fuRpBiNK5oIpP0iTQ8Rssyg4vGTdfiTjqU/ki/ptN+SfpEnwVYfmb8O38/yRPTab8k/SJPgph+ZPDt/P8kT02m/JP0iT4KYfmV4dv5/kgGXEKaInLRbj8pl8vVR4xdi3fyFJqyl+jLn2Cp+KUZJJw/U7/fc37qbUJpYUvkjmybbyyv3Sov8AR/6ZN+6r0Wfn+SKOjEaL/R/6XN+6qcZ/n+SIt+DZlRSnbDT/AGub91Yba5n8kGjTZLhBEbaY74fb/jJv3UN3Nf3/ACQaPRWPkKifSRm4o7E7ffUxHgbhClOU1hv5IYh0s6nlS39wbBiVK4f5LYjQjr5ND5IEoOI1X4k16/8AtRr6ZTfkv/fk+CyF8O38/wDtRPTKb8l/78nwUJ4dv5/9qNKWeme9jPRbZntbfr5NMzgL7d6hicbYxb18LyQrrow2V7Rs17mjwDiB9C0gsHmKb8kCzx3C3GWGZshlHnKhmVxXSg9UThWx0THdBVZx7NUjbXpZ1+mu8RGklODvqsqbXBuVMXyY1Ndk7LRqtwq1bsFb1Kr9GKBAJZDubeQRf6cBZeNawyDDmj1tUGV7fA1X0kY7yNnzRs5BYUJzCysrrB34q3gCURdO+4CXXR7IwlxN3AAe9Ej08e4KfWSfBh6ZJzK34UPID+It8xiG3S+cD2MhUMdghSlkYhDSjr4gd1SlguUFLk42AclbmylVFFatrchBNrggfUrrb1LBm5R0NPYR01MRqbJ+c87I49VLjuwoBBGTaKnJ7h3/AAWJTSCxqlIKjhDfid/qQXJyGY1qBoCqNlXusCbXtyVpZZUnhZNOkc5LaXXKDSMJA3/DTjdGqgt++5zfFb1b4Wf4FdPSuI/FHh2j7OC1OyKfmEqonJeS+bGEUIaLAfEnvS8puT3HYVqCwiyo2QqFHFCGVRHmFvLxW4S0vIKyGuOBDURWNyOOoT8JZWxxrYaZZf6jKGmh9awykaG9/YeSWlZZx3H66aPWxsbCaJugA9n1LGicuQviVQWEVfiI4BWqH3ZmXVx7IzZXOcbAe9adMYrLMR6qc3hBrWc0BvyHIx8yNp7OzAkHj3jvUc8rDKVSUtSYSHhCwMZRa6hYThn4aL87H+uFQO7/AE5e5/Qpig+Wl/OP/XK0iq/9OPuX0BhqobW4kxiKxBT3TyysHI66vTLJhSylhPIhbsipoDTN1SZrHiLwddQsyoi+AsOrnF7h0FTG7U796BKucdhuu6ue7LSYiwaDXwVKiT5NS6uuOyAZ8Qc7uCPGmMRKzqpz9hjHTPfsPaVt2RiCjTOb2QbDhX4xQJdT5DkOh/MwuOjY3ghO2TGo9PXHsa9juWPSN+gWjjsqk8moxwaBZNkJUIAzVx2b5n9g+KPGn8wnPqW9ofEDc65uTc8yjpY2Qq228s1ip3O7hzKxKaQWFUpG5yR6u8zuf9Vqx6U9kGfh1LMvnz+iF1TiRN8gy9/zvqTEKEvW3ELeslJ+ht9QnDKnMLOuXNGnG4+IQr69LyuGMdJdqWJcr6BkcoOvnc/buQXHA1GxSWTaywFwHYxC21M6wuKVovyHXTnTzKtSeMC1FcVKTx/c/ohLJW9rKB8UVVbZZUupxLSketw3FYPQZZ3UkTnU5ijNx6+YtaXOJGh7V1h1PWop8iNl01P1ngIp6SJ/3OeIm2mzmTs+sLtsHc7XKHJOLafYNG5yhY1LywbyRQwwyyejRykVksQBbswFxFrDYZQPaq5InOc1HW16KYn6OxRzVzQ+NvVvdIertdoGR5DbchotN4QfqNUKXvxjcf0NBTzGCQQMZeeWFzBq1wbFK4EjndgWcsVnOytSjqb2T+aAajBaaq9Hk6hsd6swyBhID42teT72AXGo1RIWyims9gVsZJtN52TAqrD6aojeW00cBhxFlLeLTPE6RkZLrAdqz734EeKMpSi1l5zHPyF8yiVxQ0L5ZKfqYYZIKumjiy3zTMdKxsoeLdrsl2/MK4KaSlltNP8ATbYzrl5gnSh9P6V6H6FFFH6RBGZ2gtdkL4y+xtYdkuGhW6VLRr1Z2e3xMtth+Osoo5HUzqZlO8TwNjdG0t6yB8jGyOe//VLzc31HMFBjrmtWc7P4jFcnXHUnzn/gw6W1lNEZ6c0whljdGadzGn5WM2zOe7Y6ZueotuCrrqckpJ5Xf2GquompbsK6PmBtJBM6Fkrp6kQnPrlbcjsjnp70KyDUmvIO7pWSeJYSWdg+Wmp6aOqeYGSdXUhjRJwa5kZtfewzFD3eDUZztlBamsrt+p5SqnDnuc1oYCbhrdm9wWsHRgnFJZyb4U/5aL87H+uFlolr/py9z+h3Evw0v5x/65URqr1I+5fQEOis1wDYlBmYeY1CLTPTIX6qrXWwTD5mWDTui2wlnKFumshpUXyWqcOB1aqhe1szVvSKW8RXNE5p1Fk3GSktjmzhKD3JTRZnAXUnLSsl1Q1ywO4aJjfrSMrZM60OmrgdmrWM4+SqNUpFz6iusBmxUn1RbxTEemS5E59dJ+qgR9U47koyriuELSunLlmVytYB5Z6lck9GdUIQhREazsJJIyxxaeGoPNvD4J6MtS1I5EouuTg/09wTBkaMzrW5k7HkAhT1N6UMV+HFapfPz9xhPiJPqafz3f4QiRoX93wQGzrG/U+L/ZC6V19dSTxO/kmYrAjOWrd7+0rl8+SvJnBIZ8rgRuPtZSUNUcMkLdEk0ejgkDmhzdj7vZ4rmzTTwzvVzUoqUS7pQNz9Z7llRb4NOaitwvGakfenJ1Izx7U0wGnALahs/YxSq5an5N/rulg81JTy5iLE+zceO3BNqdenIpKm7W1jJ6nBKInC6xr7gGaA95AfGfZsgWWpWKUfIzHppO1Qntk9Zh1bJEzDIoyQyQPa8ANNw0ttckXG52Sr3y2blVD+rtxjHsNfvrqJvRL9Z6dNe2T1Luv6+m+VVtncv+l4kfF40rz/AGEHRSJzcQY14s8OkDhpo7q33203WpcDXVNPp21xt9UH4PjEs9dCx2UMY+WzWNyi/VyC55n4lU1hAraYwolJctLn3oYQVhlFPLKRmZWuiBGgylrwARte+XVUBlDQ5Rj3in9BbJSOpo5evswS4tFJHcjtM66N5dodgGu8kwpa2sdotfJi00m1pedgPEejJFVUVUzXtPptL6NZzbSB00eclupNh4bHktxuWhRXk8/AFGLk9jDp5h9dNOWzuLaH0iIMc7qsrOsyxZrCzjrI/QnitdPOuMfRXpYf8lqOVyEY9hEho3Q1zheGrhhpqhwbmfDI+Njy4ZtQGF5sT8wX2uqrmlPMO6ba9plsy6T4XOMPmZV9t9NOxlJM6wfLE4sDgbEk9ku3/FF9W3V1WR8ROOya3XtJGLlsjLAaCaXD6JsLS50OIh8oBAyx3cSTc7Wc3zUnKCsk5d47e81iUD0FVizo466WBwu2say9g4X6uJrhrpuCEq4cZ8hquMbJQjLy/dnh5JS5xcd3Ek+JNytYOtHZYCcKd8vD+dj/AF2qmtirH6Evc/ocxJ/y0v52T9cqsbF1S9Be5fQHL1MG9WTNznWIC1hZMNyxhCGdhadRZdCLUlscWyMoPc3p8Sc3fULE6FLgPV1koc7oYsmjlCVcZ1sejZXcgSow8jVhRoX52kLWdK4vVADlnk2JKNGEOUKzts4kzNkROwJW3JLkGoSlwGQ4a476IMr4rgah0c5chkWHMG+qBK+T4G4dJCPJv1bO5D1SC6KwtCGSKEIoQGr6bO3T1m6jv5j2otU9L34Yv1NWuOVyuP4Ez25hcb8P2gp1PDOXJallAzdDw+HsRHuhdYi9zu59u6nBOWMMLexhOaw4hx94ul71KS2+A90koVt6viYCkjklOV1m77bnjlW/EnXWsrcF4Fd170vC5/6C4fkZMh9R2rSef208kGX9WGruhmH/AI9uh+q+Pv77BUtPmIN7HYm24307wUKM8DE6tTz38/v2jfFWANptP/itF+P4aZYy3krp4pOXvf0QCqGR1g/RmoqGZmuDIifnF3bI02G4B+hVqSEr+phXLD3938gmPYJU0xY1x7BuI3MJtfS411aftrZbjKPLMwujamoLHsHcfQusaCOuj3J0klG/+6sOSZhdbTjdP4L+RRJg1QyobC45ZHnsuzGxvfXMNefC6mVgZV9cq3NcIat6D1QNxJEDzD5AfPIq1IX/AB9Xk/l/Iknw+Rs/oxcC4va3QuyZnWsdR/OGtlrO2RqNkXDxPZ+oR0p6L1EcJkmkY8NIAs97iMxsB2miwJsPJbpsxLYTd1V3oxWH+gfRdDqx0bM8sZsAW3klNgQCN2aHw5LFkouTaJT1tcI6ZR39yFvSjonVjqWumYWyzNhA6yUjO+5aXAt9UZTzPcjdNZGOW0L9VdC3GhYx7hVUdFKv0uOidLG6QxdYwukldG1vaFhdt2nsHQN5JhXQ0a8bCqeODTEOidTHLFBUTBxkyiPK97mDM7JbtAEW02GxCH+IjjMEM01RnFzk3t5BEnR2annFM17nSSAaRyOAIN9zZu1iShO3V6WEM1ql16nnHtHJ6BVLI7MdGeORrnAEjkSLE+NkN26nlmY9VSnhRweVfcEgixBIIO4I0IPet4HdWTfCXffEP56L9dqtrZg7H6D9zJiZ+Xl/OyfruVJbF1v0V7kCmRWomnPAEcQId3I/gJoT/FuMvYGNkjlHBBanWxtSruQBV4aRq3ZHrvT5Eruja3iA6tPEFMbNCe8WG0+JEaO1QJ9OnwN1dZJbSDWmN/JAanAcTrt3NXSMYOCyoykwjnXWgObFfxQjR6fzFJ9d+UClrHnj5I8aooUn1Fku5j1h5lb0oFqfmesXIPSkUIdUIRQsUYlT5XZhs7fud9f0pumepaX2OZ1VWiWtcP6/8iyVmunH6U1F7HPnHL27mlHTukNhYW1JPD4rNk1Ddm6KpWvSgz0Vrbh2p5nkdiOX1IPiN4aG/AjHMZfH2AULSHXadjofqR5NNYYnXFqWYvjuGzNa9lydeZ1OYcPqCBFuMsLgbmo2V5b39vn5f9BOHVGduvrN0PO3P3e5Cuhpe3DGOlt8SO/KHuM+rTf7M3+9mQUbp5n/APZ/RC4FQYR7jF3ubhcORxbcRAlpsSCCSPaQsL1jk1xUurln2i/EsaikooYcznTNMd7hxPZuCS87mx3vxVpPOQ0KJV3SkltuMun9TIySm6t7mkl/quIuQ6O1wPW3271I8MB0MIyjLK8v3DekdvTKHnnk8uxb9qyuGD6f/Rs/QQ9OaiVtVZj5AOrZo1zgL3dwBWo4wNdFGLq3S5AOiTDJWxFxLiC55JJJ7LDa5PflVy4DdU1Gl4PW9JZRNRVg36ovBA5xZJLe5VWvTRyo/wBOcX7PqeX/AJLq+WSpmzve4dTo0ucWjttGgJsNEx1EFGCwVdLU84OwzufXtBe5zW1ZsC4kAtlIFgdBbZL7o60oQfTZSXq/seiqoG/deJ9u0Kci/d8r8VNb0aexzo1xfTOeN8/wX6UwdY6klaLmKsYx3c10ga4/8zW+autrdPyMVOUdUfNfsZXP3Ztl0FJcO4Xz2t5FXheFz3Ky/BxjbPIgb0l6jFZzO6QRAvZpneNA3KAweH080XwtVaxjJreVaio/qeax3Eo5KiWSPMWPeXNORwvfW9iNNbrca2lv9RquxRgk08+4zwib74gs134aLgf841XKGz3Rc7MxeE+PIris56+bsP8Aw0nD+e5SMFhbokLZKK9F8GcEodoQR4rMotboNXOMtmsA1XhxGrfJFrvXDF7uka3iLtQeIKZ2aEN4sOpcTI0dr3oE6E90O09Y1tIMfGyQaWQVKVbGpQruWULamhc3bUJmFykIW9NKHAM1xB00RGkxdNxex2SUu3UUUi5TcuTSGkc7gsysigkKJyDosMHzigS6jyG4dEv7mbehRofizDfhqholToEUIRQh1QhSWMOBB2KuMnF5RmcVNOLPP1EJaS13/kcCujCWpZRxbK3FuEjGCUscHAbb963KKnHDA1zlVPUuwRUvMguT4cB4IcEobIPbKVqy2Z0sLt9m8T3cwOK1OUeO4Omub9LsMKilawBwO25PI8Ql4WOT0setojXHWv1BTmY4SAabEcx4cEXaa0MX9KuXipe89TizwWUpGxpWEf1sySw02mPdPJS1Nfm/ZC9QYPb41/7XB4Q/qlD7nMo/93L9TzdVhEkUDKlxaY3lgABJddx0uLW4c1uK1PCGp9XWpOG+T3nSTH46aWFr4i/rC7tDL8mGlgJ1/wBYHfgswhqTeeDkVVTsyoi7GqMtxGmkzucJDYNJ0Zk3y8gcwPjdVn0cDNDz09iNOknSx9LN1Yja4BrXXc4jcn4K4wyYp6WNkNbeBB/Jq8SVUrgNGR7973i1vY1yJbBxism+rvUo6YjvojQTZa5k8bmCeaR7c1u0Jg4G3sA81LZL0WnwhOT4PM/yRRuFRLcEfI29vWNuEbq2sI3ZF4yUwyO2I6Em9a89wvMdEGcspbdjpRhp6Z4fbPyPX1Tv/Voh/Rz/APqhY9HIpF/+K17f4N8IlEk1XC75k7ZG+bXDydGD7VTWyM2pxhCa7rH38SoP/qn/AA3+JTsa/wDif/r9gbBqtorqmEsBzvc7MbaZRta2t781HwEuhnp4TT4R5bpI0elTW/HP7FaOj0v+jH3A+Fj5aL87H+uFYS3/AE5e5/Qrif4WX85J+uVEVX/px9y+h5Wue7OSdOS6VSWk4/USlrbZrS4kRo/Uc1iyhPeISnrHHaYZLCyUaIMZSrY1Ouu5ZQrqaJze8JqFsZHOt6aUAdkhadDZFcU+QEZSi9hhT4jwclp0d4jtXV9pms1Mx4uFiNkobMLOmFizEXuiLHajRMqSmthF1uuW4a7EgBoEDwG3uOPrIpbAstc88bIsaYoWn1M5GHWu5lE0oDrl5nrFyD0pFCEUIRQhFCAeI02dtx6zfeOIRqbNLw+GK9VTrjlcoRyDj9rJ9M48knuE4Y5mfKdR82/PwQrtWnK/UP0jr8TS9/LPmHOeM+VpBv5A8Rf328UBL0cyHXJa9MO/wT7/APRx0WU2cbi2hOw5gDh8PBRS1cFOGh4l+n397e4HfJpl4cCeI7h+3wRFHfIGU3jSuP2HFdPl9EjOxooyD39fUfsAQ5wynP2/wV0dmiTrfnsUQTpHt8MrqWoo2088oiczKNXNZ6vquaXaG40I8e4obTTObZC2q52QWcgfSusg9HhpIJM7WOYXOBzdlgNhn2JJNyRyPNbhJpuTRmnpZzk5z2+pT+UStjmdD1UjXholzZXA2uY7Xt4FSv0eTXR1WJSTWOBj0hxymNVRuE8ZDZH5yHt7AIbYu10Gh3UjXJp7AK811zjJbvBj0gpMLq5etkr2tOUNsyeEN0vrZzSb681ut2QW0fkwKlNR09gLovV0lH6d1dQx2oEOZ7C6QMizDLawdd7yNBwW7VOenK95GpSxsa9C+lE7pHemztyFl2lwjYA8ObpdoFyQT5IdsYcQQefSSUNSR3o5X08FXUOfLEyM5wxznAB15A4WJ30WNMpcLIbq0/AhlY/6PLxYm04iHl7REKvPmv2cnW3zk7WtrfZNKrEPbgFZdKVenthHparGKc4zFOJ4zEKUtL87cgdebTNtfUeawoS8Fxxvn+BXS8AGFY/HHi80hlb1EpkZnzjJawe119vWba/85alVmpJLf5mnqcEmG4h0qhjxRkrXtkgMIje9hzBty7bLvYhtxyKxGhuv2mlKTq0e3I8p6jDmTvq21THOIN2tkY+2a1yGN7VzbY8ygSjNLDQRSutrVSjsvv3Hg8UxJsk8j72L3kgcr7Ana9rLarljODqVyhXFV53RrhLvlofzsf67VloJY81y9z+gv6QV+WeQD/Ovv/zlMVU6lkTn1WiMUvJfQwzMlaqxOthM13xFlVRObwuE1C2Mjn3dPOHuB4pnNOhRZQUuReNkoPKGNPiQOj0rOhreJ0KusUtpmk9C14u1ZjbKOzN2dNCxZiK56dzdwm4TUuDm2VSg9zkMxbsVcoKXJULJQ4D46prxZ26Xdbjuh6N8bFiRhPRm/ZW42+YCzp3n0S8WHcyqlf5G4dJ+Y39BZzQ/GmH/AA1Y5SR1CKEJdQhFCEsoQ6oQS4nTZXZh6rvceI/b5p2izUsPlHK6qnRLUuH9RXI2xTSeTnSWGGQPuBbcey1tigyWHuNVy1LbkO1eO0dR5AjY24+3nwQNoPYc3sXpPdfBAs09xoNefAH9qLGGHuL2Waltz9/EK6Rk2ozx9Cj8+vqFuvGJe9/RCeXz3ybUNTnbfiND4pS2GiWDs9Pd4sM9+56KgwRslLLOXEOZnLW6Wc1gYXE8fnILe+DM73G1Qxs/3Cqjo41kbZM7sppZJzto9jGPy+BD/conl4Brq3lpriSX6b7/ACFnRnDG1vXDO5mQNDCAO094eQNRyYUWUfDw2D6jrHxAxwfozBPDTkzSNnqmzmMZWujvCTfMfWFwB70adsot7bLHzEpSeWZRdGWOMbs7+rfQGrJs24eAbx7WtfL36q5XYz78G4pyeF54Gx6NUkc7KcSTOlc6MElrMoa8Akg87HklpWyksjFLlGDswu5efCqVroj1srmSzPpxZrC5szXhmutsnrXO4sqUZPPs3NvrJrsm+fZjH7C+v6PRO6/qppi6GeCA9YGBvWSzmJxAbrlboRsjwnjCaXDfyyKSunLd9y7OidI6p6llRIXNfLHK14YJAY4y/rGN4xm1rnmrd01HOAWru0C4V0epar0h1PJMRFA2RvWNY0ulIl7LrfNsxnfqeS1K2UMKS5f8GpNp7hdD0LieylfJJIOvhfK8DL2csbXhrfY5Zle05JdmZct9wbCej1NVNqH08s2WKJjmZ2xtLnnrSW2F+zZjbHmSrnbOGMpZCJ7pS2T+gXD0ZpeuliM1QHQxCo7DY7GPq43EB3F132t3brDtlp1NLfYtSllaPP73ErKKNjnFlyMzspcBmy30vbja1+9Zna5LB1KOnUN3yG4fKBPDfjNGP+tqGotp4N3zUYNPyf0FvSKiJmmc3X5WS4/3ymabcJJiFvTNwUo+S+giY8tNxoU20pLcRjKUHlDSmxMHR4Ss+na3idGrrE9pmlRQNfq3RYhdKOzN2dLCxZiKZ4C02KcjNSWxzbKpVvDOwVTm7FVKtS5LrulDgYxVjHizt0tKqUN0PQ6iFixIxqaDi1Ehd2YK3pe8Re5hG6YTTEXFxe5vDVuCxKpMNDqJROSVbjxUVUUVLqJyMusPNb0oFrl5nrVxz05LKEIoQihCKEJdQhnPGHNLTsfdyIWoycXlGLIKcXFnnqiEglp3H2uulCWVlHCtrcW4syp5cp+lanHUgdVjhIYOb847cQNrftS6fZDzT9Z/f8mFVK2+h148u5bhF43A2zintyM8ddpSE/kUev8A99QqhxL3v6IHB4bbFcFRkfmHqnR3xWpw1xx3N12+FZqXD5Pp+CzQCKngdJaSaGps0C7XCW5Bc6/Z7MYtzXOcZbvHBu+z05SXmvl/2VOJNdBPE4i4wyOVmo9Z0D2SD+70RFDDT/ywLSeZZ9op6EVEUMEbpZCwyVzcuVubMGx5AHa9lt5HdruRbk5SaXkSW7HeH4f1bqWQuY2OlkxDOS5os18j2x2F7m4HBBlPOV54L0t5XfbAGyVjcKz3Ac2J9P35XTNOg39ULO7mO6VHqNPCyn8EG4u+oNbTOtGKYSwnOHR5y4sy2Oua1yVcYx0PPIkrMRwu+c+0UYtVCQ0Uh6tpZiL4yGdlmX0hpDy0aZzlGvG5ujQWNS/x/YpbZS8gaYsLcUaXtYHV0AL73ytNU4OfwuADffhutJPMPc/oUnjGD0FJDlqKd87ojVXqIxICy81O2N3VSvAJAPqjfj7EFv0Wo8bfozLa7C3AJpoZZ3VYibI/0AERmMMyPqHxXAYbcXX+1yWJSSUeN/oW0sbe0dxyt62FrHNIjNVE25bb5OGONoN+Zag74efYZEOEy1EJqZakRtkLqF5EZjymMSva+wjJHqh1/HvRbHDCUeNw1dTskkvb9BjVtY2srMtreguaDca2jiAAPsS+W0huuOKYPH96/c8ddaOoCQF3pdPfbr4bf1rU1Xjw37mcrrNWrfgrW11qiYO266X+8crdOYpryKo6nTiMuDs9Ix4uN+YQo2yg8MbsorsWUJZ4S02KejJSWxybK3B4ZanrHM2OnJVOqMuS6+onXwMIqpkmjt0tKuUN0PQvhasSMqjDeLVuHUdpA7ej7wFz2EHVMpp8CEouLwzenrHN7whzqUg1XUyhyH5WSjvS+ZVsdxC5bC6opi0pqFikhC2mUGbwUN9SUOV2NkGr6XKyzf0SPmh+JMP4FQ7SJ1iKEIoQihDhKshLKFEULAcUpswzDdvvH1fFHonh4Yn1dOuOpcr6CKVvFPxZxpruW9Idly3096rRHOS/Glp0plIoi42CuUlFbmYQc3hHo8ejAbSZuFFGL8Pw1RsloSbzjz/gahCKypCI+5GBMllZZaOIuNgFmUlHk1GLk8Ia01CG6u1clJ3OWy4OjV06hvLdl6qpawXIuRsBsOVzwWa63N7G7r1Wt935CqoqS5xLjawsNbW8r/bim41qMdkc2y+U5PU/v9M/fcHIHGw9nBE37AW/M6PYL9w58Dy71CZ+/v6kI8LC4H1c1CfQjQOGW2l+yPbfmpv3LT32x8PqcH209yszlhtFh2bVwsOA4lAsvxsuRyjpXLeXA4jjAFgAANgEk23ydSMUlhFso5KF4FOJVRDrDgm6a01lnN6q+SlhDDA6sSTQg6HrovdI1DnW4ZaL8eNtbT5wA4/h7hPM4agzSnze4o9V0WkmKz6WaipLfYVxzObsUaUIy5AQsnDhjKkna/R9rpayEobxH6bYWbT5K1eGjdnkrrv7SKu6NcwFjmkHXRNJpnPacXuE09c5veEKdMZB6+qlDYMEjJN90HTOsa113cglRRFuo1CNC5PkVt6Zx3QNHIWm4RJRUluAhNweUM4qprxZ26WlW4PKOhC+NixIDqZCDYHRGhFNZFbZyi8Jg2Y80TCF9TPXLjnqCKEIrIRQh1UQ4VZDihRUlWU2JMQp8rrj1Tt3cwnqZ6lvycfqatEsrhi8hMCLWBoahmQZRY7gDge/7apXRLVudHxq/DWnny8n9/EcYhStqGUzm1NKzJTNjc2SbI5rxLK49mx4PapD0MrD58hRzbbyBfcD+l0X9o/hW/E/xfwJkIh6Lk71NJbumJ/wocuoS7MNXXJ8xePcMYsDa0dmope89d/ClpTcnuPQsjBejGXvwDz0QI+Tq6L+cTUa29jdPFEjB/3RfwAWdamsVp+1/fAudgd9fTKG5JJPpH8P2umVPH9svgIuedyDAgT/AJZQ3O1p/Owyqa9vVfwJqy/b7jn3B/plDf8A2i1rHj2dVNf+L+BNR12Bc6yh/tFr+PZupr8ov4FuWHudGA/0yi5/hwPI5dAp4n+LJq7v6EGAE2Aq6I8h6Qf3VXiJb4fwJly2SYdR9FwNX1FKTy642H/SgWdRnaOcDlFenecZN+4YDB/6RTf1v8KXyOq7/GXwO/cj+kU39b/CqyX4/wDhL4GNVhRA0qaUHvmt/hW4Yb3TBW9Q1HaMvgC1OANlbcVNJccRN/CjQsdb3TF7ZRujlRefcDYfgnVTxPfV0QDJY3u++DfK14cbDLvYFMOxSi8RfwOe1KOzQPV4sDNLsWmWQtI2LS91j5WQn0+yaHqetwlGRhNRCTtMVxtcNpG7OmVvpQFk0LmmxFkzGcZLY5865QeJBFNXObodQhzpUuA1XVShs+Ax5jkHegrXWxuTquQvno3DbUJiNqYjZ08o8A+yLyA3QdSV1tHbJeynuhyjqcbSNKykB7TVmuzG0gl9Ca1QFhuCmtmc/dMLhgB1cUGU2tkMwqUlmTNurjWMzC6KR+uedo5ZQh2yhDishFCEUIcKhRk8LSByQNUR5hYosHpeUL2Q1rDE0jLGx4J6Lyso5M44eGcuoQsxpJsFTaXJqKcnhDSloANX+SUsub2idCnpVH0phNRM1gu7bg0blDhByeF8Ri2yNccy+HdiqrqXPOpsBsBce3UX9tvYm661FbHNuulY8N4Xl97/AC/QHzmx1FtufHhf6UTCAanj2HfA6C2trjXffj3Kv0L9z2IOQPDjrfx5AclPeWs8L7/hHL6bjfgPoH2KsrO2z+/YQm2o9thYDl4lTGSm2t0Wghc82APidvNVKcYrc1XXOx4ih1SUjWDmeJSNlrmdanp41L2+YSEMYOqiyEqEYPJqiLYBL0hZIHRuu3bkmlixYZz5aqZZjwEZ2SjXf3oeJ1MPqrvjh8i6po3M7xzTMLVIQt6eVfuJS1jmbajkpOpTJT1Eq+OA81LJRY7pfw5VvYd8au5YYDUURGo1CPC1PkTs6aS3iC6juRtmLbxYTT1pG+oQZ1J8DFfUuPIaYWSC43QdUoPcbdddyyhdUU5YdUzCxSEbapVvcvT1ZbpuFmdSkaq6iUNuxKlwdqFIJx2ZLXGe6BsxRcIXyyaqE3PYLjHqSKEIoQihDishFCHFCjhCsrBm+NWpGJQFuIU/HiN/BNUz7HP6mrK1AEMRcbAX+geJTEpKKyxOFcpvCG1JThnG54nh7Ak7JuZ0qaVWvaaVU5bbK0uJ48B7FmEFLl4N22yh6qy/oJJH5iS49q9zf/xdPJYW3ByZScnmT3JbjYfsv3338FPYTD5+/wDkswm3v0GvnwGqppFxbx95IDpc24AcfYBxV9yZysv7/Qq12tgBbv8ApJA3Ua2KTy8JLH3yQv3HsvYbd3IK8FOXYIoqEv1Nw36UK21R45GOn6Z2bvZDqKINFmiwSUpOTyzrQhGCxEusmyKFHVCzN7lpIHKWSisyUkaCtJ4MyimhZU0xBu1NQsT2ZzraXF5ia02I/NePasTo7xCVdX/bYVqqNp7TD7FddrW0irunjL0oC5zSN0ymmINOL3N4K1zd9QhzqTDV9TKPIY3q5O4oL11jS8O73g1TQObqNQiwuUhe3pZQ3XANFIWm4RJRT5AQm4vKGcdS2QWdulnW4PKOhG6NsdMgCqp8vgjwnqErqtBlA4A6rcltsDrkk9wgPYOCHiTGFKtFvSWclXhy8y/Gh5HpFzDvEUIRQhFCHFCEVkIoQihCWUIVfHdWpYMygmCQ0pjvYXaTe3Ec0WVinjPIrCl1Zwsp7mLJXBxzEW1tcFpHIciiOKcdufiCjOSliWMfB/wa002b5pB79vYRusTjp7harNfZr78zQsa7gDw+Oqzlo24xkDvw1h2uOWtwPYURXyXICXR1vgHkw19tCD3bXPeURXxyAl0c0tnn5Ac0Dxq4EAacPdZGjOL2QrOqcd5IyPAD3a+fetg32SGlFh+oc/xA+KUsu/tidGjpN1KYzASx0UdUIRUWdUIZvcrSMSkUWgZy6shUlWU2UcrRhgVTTAo8LGhO2lPgD7Tdkb0ZC3pw4No6hrtHBYcGt4hY2xltMtLQcWm6qN3aRc+l2zADsWnkUbaSFd4sYUuI8HpeyjvEdp6vtMvU0bXDMxVC1x2kbt6eM1qgK3tIOuhTSaZzpJxe5cOLgVWEmaTlJbmbYytakYUGWEDlnWjXhSLeiuVeIjXgSPVrknpCKEIoQ4oQihDqhCKEO2UIRQsihCKEOOaCpkppPkwdRt4Xaf5psiK2XfcC6IdtvcYPojuLE87ZXf8AM2xW1auPv5gpdO85WH8n8Uda8t0c13j6yjSlwy1KUFiSf1NGTNOxH0HyKy4SXY3GyD4ZpZZN4MxTsvfKL87LWuWMZBqqGdWFk1WQpFCEUIRUQq9ytIy2ZEreAZy6hMlSVZlsoStYMtlS5Xgw2ZuK0kYbMHi6ItgMlkFkiRVIWlAlPVOYe7kpOtSLrulWxj8nKORS3p1Me/p3r2i6ppXNPcmYWKQhbRKDKwVLmbK5VqRVd0q+C9TVBw21WYVuJq29TXBg12miJgCpbbEAPBTYiUux27lPRLzMmZ3epiJWqZ61cc9OcUIRQhFCHVCEUIRQh1QsihCKEIoQihCKEIoUcKhBXiKapOf1XJvQ+qsW8hen9U3KGHZ1Qh0KFkUIQqEZiVpAmVK0UVKhllCtGGVKsplCtIGzNy0jLM3LQNmMi2gUgR+6KhaXJvResFiz1QvT+uhrUeqk4cnTt9QRu3T6ONLkqVZk4rKNGLLCRNgsMKiyos//2Q=="
                         width="100%">
                </div>
                <div class="adveritisement">
                    <img src="https://mk0ninjaoutreacgog6y.kinstacdn.com/wp-content/uploads/2017/03/Advertising-strategy.jpg"
                         width="100%">
                </div>

            </div>
        </div>
    </div>
</section>
<script>

    var isEdit = false;

    var gUploadId = <?php echo isset($uploadId)?$uploadId:0;?>;
    var gUserLoginId = <?php echo $userLoginId;?>;
    var gLimitDisplay = <?php echo $limitedDisplayComments;?>;

    var gCommentId = 0;
    /* It is used whenever user clicks edit any comment*/
    var gCommentText = '';
    var gRateByUser = <?php echo ($rateByUser) ? $rateByUser : 0;?>;
    var gRateChart = <?php echo json_encode($rateChart, JSON_FORCE_OBJECT)?>;
    var gFollowerValue = [];
    var gPosterId = 0;
    var gCheckHoveredId = '';

    /*It is executed when the page is loaded*/
    window.onload = function () {
        /*var online = navigator.onLine;
        if (!online) alert('User is offline');
        else alert('User is online');
        */
        setRateStar();
        libraryChart();
    }

    window.onoffline = function () {
        updateIndicator();
    }

    function updateIndicator() {
        alert('on connection');
    }

    function refreshTotalComment() {
        var data = new FormData();
        data.append("uploadId", gUploadId);
        $.ajax({
            url: "../action/count_comments.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (totalComments) {
                /*Change Total Comment Status*/
                $("#totalComnent").html('<strong>' + totalComments + '</strong>' + ' ' + ((totalComments > 1) ? 'Comments' : 'Comment'));

                /*If comment count is less than displayed comment number => hide Show More button*/
                var commentDisplayed = $("#message-list > div").length;
                if (commentDisplayed >= Number(totalComments)) {
                    $(".show-more").hide();
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }


    /*Begin SUBMIT (Add NEW && EDIT)comment with AJAX*/
    $(document).ready(function () {
        $('#form-comment').on('submit', function (event) {
            event.preventDefault();
            var commentText = $("#comment-textarea").val();
            if (commentText == '' || commentText == undefined || gUploadId == 0) return;

            var data = new FormData();
            data.append("userId", gUserLoginId);
            data.append("uploadId", gUploadId);
            data.append("commentText", commentText);
            data.append("commentId", gCommentId);

            if (isEdit) {
                /*User clicked on Edit to Update Comment*/

                if ($("#comment-textarea").val() == gCommentText) return;

                $.ajax({
                    url: "../action/update_comment.php",
                    method: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (affectedRows) {
                        if (affectedRows > 0) {
                            /*Clear Text Area*/
                            var commentLocationId = '#comment_text' + gCommentId;
                            var createdDateId = '#created_date' + gCommentId;
                            $(commentLocationId).text(commentText);
                            var d = new Date();
                            var formatted = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                            $(createdDateId).text('Edited ' + formatted);
                            $("#comment-textarea").val('');
                            navigateToComment();
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            } else {
                /* ADD NEW Comment into Table */
                $.ajax({
                    url: "../action/add_comment.php",
                    method: "POST",
                    data: data,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (result) {
                        if (result.length > 0) {
                            refreshTotalComment();
                            appendNewCommentToUI(result);
                            /*Clear Text Area*/
                            $("#comment-textarea").val('');
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }
        });
    });

    /*End Submit Comment*/

    function changeEditStatus(status, id) {
        /* if status is false cancel edit comment else comment is being edited*/
        isEdit = status;
        gCommentId = id;
        gCommentText = '';
    }

    /*Click on Edit of each comment*/
    function doEdit(commentId) {
        changeEditStatus(true, commentId)
        $("#send").val('Save');
        var data = new FormData();
        data.append("commentId", commentId);
        $.ajax({
            url: "../action/get_comment.php",
            method: "POST",
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                /*id = 0;  createdDate = 1; userId = 2; fullName = 3 ; profile = 4 ;
                uploadId = 5 ;commentText = 6 ; likeNumber = 7; dislikeNumber = 8;*/
                if (result.length > 0) {
                    $("#comment-textarea").val(result[6]);
                    $("#profile").attr("src", '../images/' + result[4]);
                    gCommentText = result[6];
                }
            },
            error: function (e) {
                console.log(e);
            }
        });

        //removeCommentFromUI(commentId);
        $("#comment-textarea").focus();
    }

    function doCancel() {
        $("#send").val('Comment');
        if (gCommentId != 0) {
            navigateToComment();
        }
    }

    function navigateToComment() {

        $("#comment-textarea").val('');

        /*cancel editing comment*/
        changeEditStatus(false, 0);

        /*Navigate to comment location*/
        /*var containerId = '#container' + gCommentId;
        location.href = containerId;*/

    }

    /*Click on Delete of each comment*/
    function doDelete(commentId) {
        var data = new FormData();
        data.append("commentId", commentId);
        $.ajax({
            url: "../action/delete_comment.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (affectedRows) {
                if (Number(affectedRows) > 0) {
                    var containerId = '#container' + commentId;
                    $(containerId).remove();
                    refreshTotalComment();
                }
            }, error: function () {
                console.log('Delete Error');
            }
        });
    }

    /*End on Delete of each comment*/


    /*Begin Show more Comment*/
    $(document).ready(function () {
        var limitedDisplayComments = gLimitDisplay;
        $("#show-more").click(function () {
            limitedDisplayComments += gLimitDisplay;
            renderLimitedComment(gUserLoginId, gUploadId, limitedDisplayComments);
        })
    });
    /*End Show more Comment*/


    /*Begin Render Comment*/
    function renderLimitedComment(userLoginId, uploadId, limitedDisplay) {
        $("#message-list").load("../action/load_more_comments.php", {
            newCommentPosition: limitedDisplay,
            uploadId: uploadId,
            userLoginId: userLoginId
        }, function () {
            refreshTotalComment();
        });
    }

    /*End Render Comment*/


    /*Click on LIKE Icon of each comment*/
    function doDislike(commentId) {
        requiredSignIn();
        doReaction(commentId, 1);
    }


    /*Click on DISLIKE Icon of each comment*/
    function doLike(commentId) {
        requiredSignIn();
        doReaction(commentId, 0);
    }


    /*Begin submit Reaction (Like and Dislike)*/
    function doReaction(commentId, reaction) {
        var data = new FormData();

        data.append("userId", gUserLoginId);
        data.append("commentId", commentId);
        data.append("reactType", reaction);

        $.ajax({
            url: "../action/modify_reaction.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (reactionId) {
                var react = reactionId.split("|");
                var likeId = '#like' + commentId;
                var dislikeId = '#dislike' + commentId;
                $(likeId).html(react[0] + ((Number(react[0]) > 1) ? ' likes' : ' like'));
                $(dislikeId).html(react[1] + ((Number(react[1]) > 1) ? ' dislikes' : ' dislike'));
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    /*End submit Reaction (Like and Dislike)*/


    function doLikeHover(commentId) {
        var data = new FormData();
        data.append("userId", gUserLoginId);
        data.append("commentId", commentId);
        data.append("reactType", 0);

        /*User clicked on Edit to Update Comment*/
        $.ajax({
            url: "../action/check_already_react.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (existId) {
                var likeIconId = 'like_icon' + commentId;
                if (existId == 0) {
                    document.getElementById(likeIconId).title = 'I like this';
                } else {
                    document.getElementById(likeIconId).title = 'unlike this';
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function doDislikeHover(commentId) {
        var data = new FormData();
        data.append("userId", gUserLoginId);
        data.append("commentId", commentId);
        data.append("reactType", 1);

        /*User clicked on Edit to Update Comment*/
        $.ajax({
            url: "../action/check_already_react.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (existId) {
                var dislikeIconId = 'dislike_icon' + commentId;
                if (existId == 0) {
                    document.getElementById(dislikeIconId).title = 'I dislike this';
                } else {
                    document.getElementById(dislikeIconId).title = 'I disliked this';
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function appendNewCommentToUI(comment) {
        /*id = 0;  createdDate = 1; userId = 2; fullName = 3 ; profile = 4 ;
        uploadId = 5 ;commentText = 6 ; likeNumber = 7; dislikeNumber = 8; isEdited = 9*/
        if (comment.length == 0) return;
        $("#message-list").prepend($(
            '<div id="container' + comment[0] + '" class="message-container"> ' +
                '<div hidden><p id="comment-id">' + comment[0] + '</p></div>' +
                '<div class="profile">' +
                    '<div class="follow-dropdown">' +
                        '<img src="' + comment[4] + '" alt="">' +
                        '<div '+ ((comment[2] == gUserLoginId || gUserLoginId == 0)?'hidden':'') +' class="follow-dropdown-content">' +
                            '<div class="profile-user">' +
                                '<img style="width: 35px; height: 35px; border-radius: 50px;" src="' + comment[4] + '" alt="">' +
                                '<p>' + comment[3] + '</p>' +
                            '</div>' +
                            '<div class="all-send">' +
                                '<div class="custom-control custom-checkbox">' +
                                    '<input type="checkbox" class="custom-control-input" id="checkbox_notify' + comment[0] + '">' +
                                    '<label style="user-select: none;" class="custom-control-label" for="checkbox_notify' + comment[0] + '">Notify to your email</label>' +
                                '</div>' +
                            '</div>' +
                            '<div class="follow-button">' +
                                '<button id="button_follow'+comment[0]+'" class="btn btn-success">Follow</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                    '<div class="comment-description" style="width: 100%;">' +
                        '<div class="comment-show">' +
                            '<div style="width: 100%;" class="name">' +
                            '<label>' + comment[3] + '</label>' +
                            '<label id="created_date' + comment[0] + '" style="float:right;">' + ((Number(comment[9]) == 1) ? 'Edited  ' : '') + comment[1] + '</label>' +
                            '</div>' +
                            '<div style="display: flex">' +
                            '<div' + ((gRateByUser >= 1) ? ' class="com-star"' : ' class="white-star"') + '>' +
                            '<i class="fa fa-star fa-fw"></i>' +
                            '</div>' +
                            '<div' + ((gRateByUser >= 2) ? ' class="com-star"' : ' class="white-star"') + '>' +
                            '<i class="fa fa-star fa-fw"></i>' +
                            '</div>' +
                            '<div' + ((gRateByUser >= 3) ? ' class="com-star"' : ' class="white-star"') + '>' +
                            '<i class="fa fa-star fa-fw"></i>' +
                            '</div>' +
                            '<div' + ((gRateByUser >= 4) ? ' class="com-star"' : ' class="white-star"') + '>' +
                            '<i class="fa fa-star fa-fw"></i>' +
                            '</div>' +
                            '<div' + ((gRateByUser >= 5) ? ' class="com-star"' : ' class="white-star"') + '>' +
                            '<i class="fa fa-star fa-fw"></i>' +
                            '</div>' +
                            '</div>' +
                            '<div class="comment-text">' +
                            '<label id="comment_text' + comment[0] + '" class="mark-label">' + comment[6] + '</label>' +
                            '</div>' +
                            '</div>' +
                            '<div class="operation">' +
                            '<div class="like operation">' +
                            '<div class="react">' +
                            '<label class="mark-label" id="like' + comment[0] + '">' + comment[7] + '' + ((comment[7] > 1) ? " likes" : " like") + ' </label>' +
                            '</div>' +
                            '<div style="margin-left: 5px;" class="react">' +
                            '<i style="color: rgba(45,45,45,0.83);" id="like_icon' + comment[0] + '" class="fas fa-thumbs-up" onclick="doLike(' + comment[0] + ')" onmouseover="doLikeHover(' + comment[0] + ')"></i>' +
                            '</div>' +
                            '</div>' +
                            '<div class="dislike operation">' +
                            '<div class="react">' +
                            '<label class="mark-label" id="dislike' + comment[0] + '">' + comment[8] + '' + ((comment[8] > 1) ? " dislikes" : " dislike") + ' </label>' +
                            '</div>' +
                            '<div style="margin-left: 5px;" class="react">' +
                            '<i style="color: rgba(45,45,45,0.83); margin-top: 4px;" id="dislike_icon' + comment[0] + '" class="fas fa-thumbs-down" onclick="doDislike(' + comment[0] + ')" onmouseover="doDislikeHover(' + comment[0] + ')"></i>' +
                            '</div>' +
                            '</div>' +
                            '<div class="edit-comment operation">' +
                            '<label' + ((comment[2] == gUserLoginId) ? 'hidden' : '') + ' class ="mark-label reaction" onclick="doEdit(' + comment[0] + ')">edit</label>' +
                            '</div>' +
                            '<div class="delete-comment operation">' +
                            '<label' + ((comment[2] == gUserLoginId) ? 'hidden' : '') + ' class ="mark-label reaction" onclick="doDelete(' + comment[0] + ')">delete</label>' +
                            '</div>' +
                    '</div>' +
                '</div>' +
            '</div>').fadeIn('slow'));
    }


    $(document).ready(function () {
        /* 1. Visualizing things on Hover - See next part for Model on click */
        $('#rate-star li').on('mouseover', function () {
            var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

            // Now highlight all the stars that's not after the current hovered star
            $(this).parent().children('li.star').each(function (e) {
                if (e < onStar) {
                    $(this).addClass('hover');
                }
                else {
                    $(this).removeClass('hover');
                }
            });
            //document.getElementById("rate-text").innerHTML = onStar + '.0';

        }).on('mouseout', function () {
            $(this).parent().children('li.star').each(function (e) {
                $(this).removeClass('hover');
            });
        });


        /* 2. Action to perform on click */
        $('#rate-star li').on('click', function () {
            requiredSignIn();
            var onStar = parseInt($(this).data('value'), 10); // The star currently selected
            var stars = $(this).parent().children('li.star');

            for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('selected');
            }

            for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('selected');
            }

            // JUST RESPONSE (Not needed)
            var ratingValue = parseInt($('#rate-star li.selected').last().data('value'), 10);

            document.getElementById("rate-text").innerHTML = ratingValue + '.0';

            addRate(ratingValue);

        });

    });


    function addRate(rateType) {
        gRateByUser = rateType;
        var data = new FormData();
        data.append("userId", gUserLoginId);
        data.append("uploadId", gUploadId);
        data.append("rateType", rateType);
        $.ajax({
            url: "../action/modify_rate.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                var modify = result.split('|');
                /*index 0 is get Modify ID; index 1 is Total of rate Count*/
                if (modify[0] == 0) { /*Modification is not successful*/
                    var stars = $('#rate-star li').parent().children('li.star');
                    for (i = 0; i < stars.length; i++) {
                        $(stars[i]).removeClass('selected');
                    }
                }
                if (Number(modify[1]) > 0) {
                    /*Update Rate Count*/
                    var total = 'Total ' + modify[1] + ((Number(modify[1]) > 1) ? ' raters' : ' rater');
                    $("#total-rate").text(total);
                    $("#rate-view").html('<strong>' + modify[1] + '</strong>' + ((Number(modify[1]) > 1) ? ' rates' : ' rate'));

                    refreshRateChart();

                    /*Update All rate Star in Comments that commented by User Login*/
                    renderLimitedComment(gUserLoginId, gUploadId, gLimitDisplay);
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function setRateStar() {
        var ratedStar = "<?php echo $rateByUser;?>";
        var stars = $('#rate-star li').parent().children('li.star');
        for (i = 0; i < ratedStar; i++) {
            $(stars[i]).addClass('selected');
        }
    }

    function setRateStar() {
        var ratedStar = "<?php echo $rateByUser;?>";
        var stars = $('#rate-star li').parent().children('li.star');
        for (i = 0; i < ratedStar; i++) {
            $(stars[i]).addClass('selected');
        }
    }

    /*Select File to donwload*/
    $('#select-file').on('change', function () {
        if (this.value == '') $('#download_file').enabled = false;
        $('#download_file').attr('href', this.value);
    })


    function libraryChart() {
        if(gRateChart == null || gRateChart == undefined || gRateChart.length == 0) return;

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            columnFill: false,
            columnSpan: false,

            toolTip: {
                content: toolTipFormatter
            },

            data: [{
                type: "bar",
                dataPoints: [
                    {y: Number(gRateChart[0]), label: "1", color: "#af710f"},
                    {y: Number(gRateChart[1]), label: "2", color: "#AF7F3A"},
                    {y: Number(gRateChart[2]), label: "3", color: "#c1b710"},
                    {y: Number(gRateChart[3]), label: "4", color: "#87aa0d"},
                    {y: Number(gRateChart[4]), label: "5", color: "#4da477"},
                ]
            }]
        });
        chart.render();

        function toolTipFormatter(e) {
            var str = "";
            var total = 0;
            var str3;
            var str2;
            for (var i = 0; i < e.entries.length; i++) {
                str = "<strong>" + e.entries[i].dataPoint.y + "</strong>";
            }
            return str;
        }
    }


    function refreshRateChart() {
        var data = new FormData();
        data.append("uploadId", gUploadId);
        $.ajax({
            url: "../action/get_rates.php",
            method: "POST",
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                gRateChart = result;
                libraryChart();
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function updateDownloadNumber() {
        var data = new FormData();
        data.append("uploadId", gUploadId);
        $.ajax({
            url: "../action/update_download_number.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                console.log(result);
                if (Number(result) > 0 || result > 0) {
                    var downloadedNumber = Number($('#down-number').text());
                    console.log('Strong value : ' + downloadedNumber);

                    downloadedNumber += Number(result);
                    console.log('Strong value added : ' + downloadedNumber);
                    $("#download_number").html('<strong id="down-number">' + downloadedNumber + '</strong>' + ' ' + ((downloadedNumber > 1) ? 'Downloads' : 'Download'));
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    $('#download_file').click(function (e) {
        if ($(this).attr('href') == '') {
            alert('please select file type to download');
            e.preventDefault();
        } else {
            updateDownloadNumber();
        }
    });

    function doPosterHover(posterId) {
        if (posterId == gUserLoginId || gUserLoginId == 0){
            $('#checkbox_notify').prop('checked',false);
            $('#button_follow').html('Follow');
            return;
        }
        gPosterId = posterId;
        var data = new FormData();
        data.append("userLoginId", gUserLoginId);
        data.append("posterId",posterId);
        $.ajax({
            url: "../action/get_follower.php",
            method: "POST",
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                console.log('Poster Hover' +gPosterId+' : '+ result[0] + ' ; ' + result[1])
                gFollowerValue = result;
                $('#checkbox_notify').prop('checked',(result[0]==0)?false:true);
                $('#button_follow').html((result[1]==0)?'Follow':'Unfollow');
                gCheckHoveredId = '#checkbox_notify';
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function  doProfileHover(commentId, posterId) {
        if (posterId == gUserLoginId || gUserLoginId == 0) return;
        gPosterId = posterId;
        console.log('Hover Event : '+commentId + ' ; ' + posterId + ' ; '+gUserLoginId);
        var data = new FormData();
        data.append("userLoginId", gUserLoginId);
        data.append("posterId",posterId);
        $.ajax({
            url: "../action/get_follower.php",
            method: "POST",
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                gFollowerValue = result;
                //console.log('Follower Value : ' + gFollowerValue[0]+' ; ' + gFollowerValue[1]);
                var checkboxNotify = '#checkbox_notify'+commentId;
                var buttonFollowId = '#button_follow'+commentId;
                $(checkboxNotify).prop('checked',(result[0]==0)?false:true);
                $(buttonFollowId).html((result[1]==0)?'Follow':'Unfollow');

                gCheckHoveredId = checkboxNotify;
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    /*Follow button is clicked*/
    $(".follow-button button.btn-success").click(function () {
        buttonFollowClicked(this);
    })

    /*Follow button is clicked from load more comment*/
    function  doFollow(id) {
        var buttonId = '#button_follow'+id;
        buttonFollowClicked(buttonId);
    }

    function buttonFollowClicked(buttonId){
        requiredSignIn();
        if (gUserLoginId == 0) return;

        $(buttonId).html((gFollowerValue[1]==0)?'Unfollow':'Follow');

        var status = (gFollowerValue[1]==0)?1:0;
        if (status == 0){
            gFollowerValue[0] = 0; //if user unfollow the poster => remove allow send notification to User Loign Email
            $(gCheckHoveredId).prop('checked',false);
            showToastMessage('You will not get any notification anymore, unless you follow again.');
        }else{
            showToastMessage('You will get notification whenever he/she uploads new book.');
        }
        gFollowerValue[1] = status;
        modifyFollower(gFollowerValue[0],status);
    }

    $('.all-send input[type=checkbox]').on('change', function() {

        checkBoxIsChanged(this);
    });

    function doChanged(id) {
        var checkBoxId = '#checkbox_notify'+id;
        checkBoxIsChanged(checkBoxId);
    }

    function  checkBoxIsChanged(checkBoxId) {
        requiredSignIn();
        if (gUserLoginId == 0) return;
        
        var notify = ($(checkBoxId).is(':checked'))?1:0;
        var status = gFollowerValue[1]; //index 1 is assigned button status ("Follow = 1 / Unfollow = 0")
        if (gFollowerValue[1] != 0){
            gFollowerValue[0] = notify;
            modifyFollower(notify,status);
            if (notify == 1){
                showToastMessage('You will get new mail whenever he/she uploads new book.');
            }else{
                showToastMessage('You will not get new mail whenever he/she uploads new book.');
            }
        }else if (notify == 1 && gFollowerValue[1] == 0){
            //User Never follows the Over Profile
            $(checkBoxId).prop('checked', false);
            showToastMessage('Please click follow button first to get notification.');
        }
    }


    function  showToastMessage(msg) {
        var x = document.getElementById("snackbar");
        x.innerHTML = msg;
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }

    function modifyFollower(notify, status) {
        var data = new FormData();
        data.append("posterId",gPosterId);
        data.append("followerId", gUserLoginId);
        data.append("notify",notify);
        data.append("status",status);
        $.ajax({
            url: "../action/modify_follower.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                console.log(result);
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    $("#comment-textarea").click(function () {
        requiredSignIn();
    })

    function requiredSignIn() {
        if (gUserLoginId == undefined || gUserLoginId == null || gUserLoginId == '' || gUserLoginId == 0) {
            event.preventDefault();
            $('#loginModal').modal('show');
        }
    }

</script>

<?php
include "footer.php";
?>
