<?php
include('header.php');
require_once 'error_message.php';
include_once('../action/loginWithGoogle.php');
require_once '../node_modules/vendor/autoload.php';

$fullNameEmpty = false;
$genderEmpty = false;
$dobEmpty = false;
$emailEmpty = false;
$passwordEmpty = false;

/*Validate variables*/
$genderInvalid = false;
$dobInvalid = false;
$emailInvalid = false;
$passwordInvalid = false;

$fullNameInvalidLength = false;
/*Exist variables*/
$emailExist​​​ = false;

$insertError = false;

if (isset($_POST['signup'])){
    require_once '../config/DbConfig.php';
    require_once '../model/User.class.php';

    $fullName = $_POST['fullname'];
    $gender = $_POST['gender'];
    $dob = $_POST['dob'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    if (empty($fullName)) $fullNameEmpty = true;
    if (empty($gender)) $genderEmpty = true;
    if (empty($dob)) $dobEmpty = true;
    if (empty($email)) $emailEmpty = true;
    if (empty($password)) $passwordEmpty = true;

    if (!empty($fullName) && !empty($gender) && !empty($dob) && !empty($email) && !empty($password)){

        $current = date('y-m-d');
        $selectedDate = strtotime($dob);
        $currentDate = strtotime($current);
        if(strlen($fullName) > 50){
            $fullNameInvalidLength = true;
        }else if ($gender != 'male' && $gender != 'female'){
            $genderInvalid = true;
            }/*else if ($selectedDate > $current){
                $dobInvalid = true;
            }*/else if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                /* Validate Email */
                $emailInvalid = true;
            }else if(User::isExistEmail($email) > 0){
                $emailExist​​​ = true;
            }else if(strlen($password) < 8 || strlen($password) > 50){
                $passwordInvalid = true;
            }else{
                $hashPWD = password_hash($password,PASSWORD_DEFAULT);

                $user = new User();
                $user->userType = 'owner';
                $user->fullName = $fullName;
                $user->gender = $gender;
                $user->dob = $dob;
                $user->email = $email;
                $user->password = $hashPWD;
                $user->profile = '../images/no_profile.png'; /*set default profile for first creation*/
                $insertedId =  User::add($user);
                if ($insertedId > 0){ /*Inserted successfully*/
                    header("location: ../pages/sign_in.php");
                }else{
                    $insertError = true;
                }
            }
        }
    }
    ?>

    <link rel="stylesheet" href="../assets/css/sign_up.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <div class="sign-wrapper">
        <div class="sign-container border">

            <div class="sign-title">
                <h3 class="mark-label">Sign Up</h3>
            </div>

            <!--show error Area-->
            <div style="margin-top: 15px;" class="error-container" id="error-container">
                <?php
                if ($fullNameEmpty){
                    errorMessage('Full name is required.');
                }else if($fullNameInvalidLength){
                    errorMessage('Full name maximum is 50 characters.');
                }

                if ($genderEmpty) errorMessage('Gender is required.');
                else if ($genderInvalid) errorMessage('Please input gender again.');


                if ($dobEmpty) errorMessage('Date of Birth is required.');
                else if($dobInvalid) errorMessage('Date of birth cannot be greater than current date.');

                if ($emailEmpty) errorMessage('Email address is required.');
                else{
                    if($emailInvalid) errorMessage('Email address is invalid.');
                    else if ($emailExist​​​) errorMessage('Email has already been taken!');
                }

                if ($passwordEmpty) errorMessage('Password is required');
                else if ($passwordInvalid) errorMessage('Password is at least 8 characters and less than 50 characters');

                if ($insertError) errorMessage('Sign up was not successful.');
                ?>
            </div>

            <div class="sign-form">
                <form action="" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full name" value="<?php echo isset($fullName)?$fullName:''; ?>">
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="gender" id="gender" name = "gender">
                            <option value="male" selected>Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="date" class="form-control" id="dob" name="dob" value="<?php echo isset($dob)?$dob:''; ?>">
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="example@gmail.com" value="<?php echo isset($email)?$email:''; ?>">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo isset($password)?$password:''; ?>">
                    </div>
                    <div class="submit form-group">
                        <button type="submit" class="btn btn-primary form-control" name = "signup">Sign Up</button>
                    </div>
                </form>
            </div>

            <div class="social-buttons">
                <?php
                    if (isset($authUrl)) {
                        print "<a href='$authUrl' class='btn btn-fb'><i class='fa fa-google'></i> Google</a>";
                        print "<a href='javascript:void(0)' class='btn btn-tw'  id='btn_facebook'><i class='fa fa-facebook'></i> Facebook</a>";
                    }else{
                        print "<a href='#' class='btn btn-fb'><i class='fa fa-google'></i> Google</a>";
                        print "<a href='#' class='btn btn-tw'  id='btn_facebook'><i class='fa fa-facebook'></i> Facebook</a>";
                    }
                ?>
            </div>

        </div>
    </div>

    <script>
        window.onload = function (){
           // setScreenSize();
           setSelectedValueToGender();
       }

       function setSelectedValueToGender() {
        var gender = "<?php echo $gender?>";
        console.log(gender);
        $("#gender").val(gender);
    }

    function setScreenSize() {
        console.log('Navbar Height : ' + $(".navbar").height());
        console.log('Window Height : ' + $(window).height());
        console.log('Screen height : ' + screen.height );
        console.log('Body height : '+$('body').height());

        var wrapperHeight = $(window).height() - $(".navbar").height();
        console.log('Wrapper hegiht : ' + wrapperHeight);
        $('.sign-wrapper').height(wrapperHeight);
    }

</script>
<?php
include "footer.php";
?>