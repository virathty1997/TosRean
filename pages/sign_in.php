<?php
require_once '../node_modules/vendor/autoload.php';


if (session_status() == PHP_SESSION_NONE) {
    session_start(); //abc
}
$emailInvalid = false;
$emailEmpty = false;
$passwordEmpty = false;
$signInError = false;

if (isset($_SESSION['email'])) {
    if (empty($_SESSION['email'])) {
        $email = null;
        $emailEmpty = true;
    } else {
        $errors = explode('|', $_SESSION['email']);
        if (count($errors) == 2 && end($errors) == 'invalid') {
            $emailInvalid = true;
            $email = $errors[0];
        } else {
            $email = $_SESSION['email'];
        }
    }
}

if (isset($_SESSION['password'])) {
    if (empty($_SESSION['password'])) {
        $password = null;
        $passwordEmpty = true;
    } else {
        $password = $_SESSION['password'];
    }
}

if (isset($_SESSION['sign_in'])) {
    if ($_SESSION['sign_in'] == 'success') $signInError = false;
    else if ($_SESSION['sign_in'] == 'error') $signInError = true;
}

include_once('../action/loginWithGoogle.php');

?>

<?php
include('header.php');
require_once 'error_message.php';
?>
<link rel="stylesheet" href="../assets/css/sign_in.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<div class="login-clean">
    <div class="sign-container border">
        <div class="error-container" id="error-container">
            <?php
            if ($emailEmpty) errorMessage('Email address is required.');
            else if ($emailInvalid) errorMessage('Email is in valid');

            if ($passwordEmpty) errorMessage('Password is required.');

            if ($signInError && !$emailEmpty && !$emailInvalid && !$passwordEmpty)
                errorMessage('Email or Password is incorrect')
            ?>
        </div>

        <div class="sign-title">
            <h3 class="title-text">Sign in</h3>
        </div>
        <div class="bar"></div>

        <div class="sign-form">
            <form action="../action/compare_sign_in.php" method="post">
                <div class="form-group">

                    <input class="form-control" type="email" id="email" name="email" placeholder="Username or email"
                           value="<?php echo isset($email) ? $email : '' ?>">
                </div>
                <div class="form-group">
                    <input class="form-control" type="password" id="password" name="password" placeholder="Password"
                           value="<?php echo isset($password) ? $password : '' ?>">
                </div>
                <div class="help">
                    <input type="checkbox" value="Remember me"> Remember me</input>
                    <a class="forgot-password" href="forgot_password.php">forgot password?</a>
                </div>
                <div class="form-group submit">
                    <input class="btn btn-primary form-control" type="submit" id="signin" name="signin" value="Sign In"
                           style="border-radius: 4px;background: transparent;color: #1ea291;border: 1px solid #1da391;">
                </div>
            </form>
        </div>
        <div class="new-account" style="width: 100%; text-align: center;">
            <a href="sign_up.php">Create new account </a>
        </div>
        <div class="social-buttons">


            <?php
                if (isset($authUrl)) {
                    print "<a href='$authUrl' class='btn btn-fb'><i class='fa fa-google'></i> Google</a>";
                    print "<a href='javascript:void(0)' class='btn btn-tw'  id='btn_facebook'><i class='fa fa-facebook'></i> Facebook</a>";
                }else{
                    echo '<a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>';
                    echo '<a href="#" class="btn btn-tw"><i class="fa fa-google"></i> Google</a>';
                }
            ?>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {

        $(window).on('keydown', function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });

        $("#btn_facebook").click(function () {
            $("#loginModal").modal('show');

        })
    })


</script>
<?php
require_once 'footer.php';
?>
