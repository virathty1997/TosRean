<?php
/*check whether session was started*/
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once '../model/User.class.php';
require_once '../config/DbConfig.php';
require_once 'error_message.php';

$userLoginId = (isset($_SESSION['userLoginId'])) ? $_SESSION['userLoginId'] : 0;
if ($userLoginId == 0) {
    echo 'Profile ID is not found';
    return;
}

$user = User::getUserById($userLoginId);
if (!$user) {
    echo 'Profile information is not found';
    return;
}
/*Boolean empty variable*/
$fullNameEmpty = false;
$genderEmpty = false;
$dobEmpty = false;
$emailEmpty = false;

/*Validate variables*/
$fullNameInvalidLength = false;
$genderInvalid = false;
$dobInvalid = false;
$emailInvalid = false;
$newPasswordInvalid = false;
$retypePasswordInvalid = false;

/*Exist variables*/
$emailExist​​​ = false;

$email = '';

/*error variable*/
$uploadFileInvalid = false;
$uploadFileError = false;
$uploadFileLarger = false;

$fileError = false;
$fileInvalid = false;
$fileLarger = false;
$updateError = false;
$updateSuccess = false;
/*Start of Validation*/
if (isset($_SESSION['update']) && $_SESSION['update'] == 'error') {
    $updateError = true;
} else if (isset($_SESSION['update']) && $_SESSION['update'] == 'success') {
    $updateSuccess = true;
}

if (isset($_SESSION['full_name'])) {
    if (empty($_SESSION['full_name'])) {
        $fullName = null;
        $fullNameEmpty = true;
    } else {
        $errors = explode('|', $_SESSION['full_name']);
        if (count($errors) == 2 && end($errors) == 'invalid') {
            $fullNameInvalidLength = true;
            $fullName = $errors[0];
        } else {
            $fullName = $_SESSION['full_name'];
        }
    }
}


if (isset($_SESSION['gender'])) {
    if (empty($_SESSION['gender'])) {
        $gender = null;
        $genderEmpty = true;
    } else {
        $errors = explode('|', $_SESSION['gender']);
        if (count($errors) == 2 && end($errors) == 'invalid') {
            $genderInvalid = true;
            $gender = $errors[0];
        } else {
            $gender = $_SESSION['gender'];
        }
    }
}

if (isset($_SESSION['dob'])) {
    if (empty($_SESSION['dob'])) {
        $dob = null;
        $dobEmpty = true;
    } else {
        $errors = explode('|', $_SESSION['dob']);
        if (count($errors) == 2 && end($errors) == 'invalid') {
            $dobInvalid = true;
            $dob = $errors[0];
        } else {
            $dob = $_SESSION['dob'];
        }
    }
}

if (isset($_SESSION['email'])) {
    if (empty($_SESSION['email'])) {
        $email = null;
        $emailEmpty = true;
    } else {
        $errors = explode('|', $_SESSION['email']);
        if (end($errors) == 'invalid') {
            $emailInvalid = true;
        } else if (end($errors) == 'exist') {
            $emailExist = true;
        }
        $email = $errors[0];
    }
}

$oldPassword = (isset($_SESSION['old_password'])) ? $_SESSION['old_password'] : '';

if (isset($_SESSION['new_password'])) {

    $errors = explode('|', $_SESSION['new_password']);
    if (count($errors) == 2 && $errors[1] == 'invalid') {
        $newPasswordInvalid = true;
        $newPassword = $errors[0];
    } else {
        $newPassword = $_SESSION['new_password'];
    }
}

if (isset($_SESSION['retype_password'])) {
    if (empty($_SESSION['retype_password'])) {
        $retypePassword = null;
        $passwordEmpty = true;
    } else {
        $errors = explode('|', $_SESSION['retype_password']);
        if (count($errors) == 2 && $errors[1] == 'invalid') {
            $retypePasswordInvalid = true;
            $retypePassword = $errors[0];
        } else {
            $retypePassword = $_SESSION['retype_password'];
        }
    }
}

if (isset($_SESSION['upload_file'])) {

    if ($_SESSION['upload_file'] == 'invalid') {
        $fileInvalid = true;
    } else if ($_SESSION['upload_file'] == 'larger') {
        $fileLarger = true;
    } else if ($_SESSION['upload_file'] == 'error') {
        $fileError = true;
    }
}
?>

<?php
require_once 'header.php';
?>

<link rel="stylesheet" href="../assets/css/profile.css">

<div class="profile-wrapper">

    <div class="profile-container border">

        <div class="title">
            <h4><?php echo $user->fullName ?>'s Profile</h4>
        </div>

        <div class="photo-container">
            <div class="form-container">
                <form action="../action/update_profile.php" method="POST" enctype="multipart/form-data">
                    <div class="photo">

                        <img id="image-profile" src="<?php echo $user->profile; ?>" width="150px" height="150px"
                             style="border-radius: 150px;" alt="" onError="this.onerror=null;this.src='../images/no_profile.png';">
                    </div>
                    <div class="upload">
                        <div class="choose-wrapper">
                                <span class="btn btn-default btn-file choose middle">
                                    Select new profile<input type="file" width="300px" id="photo" name="photo"
                                                             accept="image/jpg, image/jpeg, image/png, image/gif">
                                    <!--<input type="file" name="photo" onchange="readSelectedURL(this)" accept="image/jpeg,image/jpg,image/png">-->
                                </span>

                            <div class="help" style="margin-top: 10px;">
                                <lebel>The maximum file size should be 2 MB</lebel>
                                <br><label <?php echo ($fileInvalid && $updateError) ? '' : 'hidden'; ?>
                                        class="mark-label error">Image file is not allowed.</label>
                                <label <?php echo ($fileLarger && $updateError) ? '' : 'hidden'; ?>
                                        class="mark-label error">Image size is to large.</label>
                                <label <?php echo ($fileError && $updateError) ? '' : 'hidden'; ?>
                                        class="mark-label error">Error while upload image file.</label>
                                <label <?php echo ($updateSuccess) ? '' : 'hidden'; ?> class="mark-label success">Profile
                                    has been updated successfully.</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="mark-label" for="fullname">Full name</label>
                        <br><label <?php echo ($fullNameEmpty) ? '' : 'hidden'; ?> class="mark-label error">Full name is
                            required.</label>
                        <label <?php echo ($fullNameInvalidLength) ? '' : 'hidden'; ?> class="mark-label error">Full
                            name is maximum 50 characters length</label>
                        <input type="text" class="form-control" id="fullname" name="fullname"
                               value="<?php echo (isset($_SESSION['full_name'])) ? $fullName : $user->fullName; ?>">
                    </div>
                    <div class="form-group">
                        <label class="mark-label" for="gender">Gender</label>
                        <br><label <?php echo ($genderEmpty) ? '' : 'hidden'; ?> class="mark-label error">Gender is
                            required.</label>
                        <label <?php echo ($genderInvalid) ? '' : 'hidden'; ?> class="mark-label error">Selected gender
                            is not correct.</label>
                        <select class="form-control" name="gender" id="gender" name="gender">
                            <option value=""></option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="mark-label" for="birth-date">Date of Birth</label>
                        <br><label <?php echo ($dobEmpty) ? '' : 'hidden'; ?> class="mark-label error">Date of birth is
                            required.</label>
                        <label <?php echo ($dobInvalid) ? '' : 'hidden'; ?> class="mark-label error">Date of birth
                            cannot be greater than current date.</label>
                        <input type="date" class="form-control" id="dob" name="dob"
                               value="<?php echo (isset($_SESSION['dob'])) ? $dob : $user->dob; ?>">
                    </div>

                    <div class="form-group">
                        <label class="mark-label" for="email">Email address</label>
                        <br><label <?php echo ($emailEmpty) ? '' : 'hidden'; ?> class="mark-label error">Email address
                            is required.</label>
                        <label <?php echo ($emailInvalid) ? '' : 'hidden'; ?> class="mark-label error">Email address is
                            invalid format.</label>
                        <label <?php echo ($emailExist​​​) ? '' : 'hidden'; ?> class="mark-label error">Email address
                            has already been taken.</label>
                        <input type="email" class="form-control" id="email" name="email"
                               value="<?php echo (isset($_SESSION['email'])) ? $email : $user->email; ?>">
                    </div>

                    <div class="change-password">
                        <div class="description">
                            <label for=""><strong>Change Password</strong></label>
                        </div>
                        <div class="bar"></div>
                    </div>
                    <div class="form-group">
                        <label class="mark-label" for="password">Old password</label>
                        <input type="password" class="form-control" id="old_password" name="old_password"
                               value="<?php echo isset($_SESSION['old_password']) ? $_SESSION['old_password'] : ''; ?>">
                    </div>

                    <div class="form-group">
                        <label class="mark-label" for="password">New password</label>
                        <br><label <?php echo ($newPasswordInvalid) ? '' : 'hidden'; ?> class="mark-label error">New
                            password length is at least 8 characters and less than 50 characters.</label>
                        <input type="password" class="form-control" id="new_password" name="new_password" readonly
                               value="<?php echo isset($_SESSION['new_password']) ? $newPassword : ''; ?>">
                    </div>

                    <div class="form-group">
                        <label class="mark-label" for="password">Re-type password</label>
                        <br><label <?php echo ($retypePasswordInvalid) ? '' : 'hidden'; ?> class="mark-label error">Password
                            enter does not match. Please enter again!</label>
                        <input type="password" class="form-control" id="retype_password" name="retype_password" readonly
                               value="<?php echo isset($_SESSION['retype_password']) ? $retypePassword : ''; ?>">
                    </div>

                    <div class="submit form-group">
                        <button type="submit" class="btn btn-primary update" name="update">Update Profile</button>
                        <a class="btn btn-primary" href="profile.php">Cancel Update</a>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>

<script>

    var gUserLoginId = <?php echo $userLoginId;?>;
    window.onload = function () {
        setSelectedValueToGender();
        checkReadOnlyPasswordBox();
    }

    function checkReadOnlyPasswordBox() {
        var oldPassword = '<?php echo isset($oldPassword) ? $oldPassword : '';?>';
        if (oldPassword == '') return;
        $('#new_password').prop('readonly', false);
        $('#retype_password').prop('readonly', false);
    }

    /*Set selected gender ON LOAD event*/
    function setSelectedValueToGender() {
        var gender = "<?php echo (isset($_SESSION['gender'])) ? $gender : $user->gender?>";
        console.log(gender);
        $("#gender").val(gender);
    }

    /*Display image in Picture Box*/
    function readSelectedURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image-profile').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    /*set selected image from Local Machine to Image Chooser*/
    $(document).on('change', ':file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
        readSelectedURL(this);
    });

    /*Prevent Submit when user clicks ENTER KEY*/
    $('#old_password').on('keydown', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });

    /*Check text change on Old Password field*/
    $('#old_password').on('input', function () {
        verifyOldPassword();
    })


    function verifyOldPassword() {
        var oldPassword = $("#old_password").val();
        var data = new FormData();
        data.append("userId", gUserLoginId);
        data.append("oldPassword", oldPassword);
        $.ajax({
            url: "../action/verify_old_password.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                console.log(result);
                if (result == 1 || Number(result) == 1) {
                    $('#new_password').prop('readonly', false);
                    $('#retype_password').prop('readonly', false);
                } else {
                    $('#new_password').prop('readonly', true);
                    $('#retype_password').prop('readonly', true);

                    $('#new_password').val('');
                    $('#retype_password').val('');
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

</script>

<?php
require_once 'footer.php';

unset($_SESSION['full_name']);
unset($_SESSION['gender']);
unset($_SESSION['dob']);
unset($_SESSION['email']);
unset($_SESSION['old_password']);
unset($_SESSION['new_password']);
unset($_SESSION['retype_password']);
unset($_SESSION['upload_file']);
unset($_SESSION['update']);
?>
