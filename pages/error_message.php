<?php
    function errorMessage($text){

        if(empty($text)) return;

        echo '      
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" id="button-close" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <strong>'.$text.'</strong>
              </div>';
        }
?>