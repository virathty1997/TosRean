<?php
include('header.php');
include_once "../config/DbConfig.php";
include_once "../model/Book.php";

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$searchText = isset($_REQUEST['search_value'])?$_REQUEST['search_value']:$_SESSION['general_search_text'];

$_SESSION['general_search_text'] = $searchText;

$perPage = isset($_SESSION['per_page']) ? $_SESSION['per_page'] : 18;

if ($perPage == 0) {
    $_SESSION['per_page'] = 1;
    $perPage = 1;
}
if (isset($_GET['p'])) {
    $pageNumber = $_GET['p'];
} else {
    $pageNumber = 1;
}
$offset = (($pageNumber - 1) * $perPage);

$totalItems = Book::getCountSearchResult($searchText);

$totalPage = floor($totalItems / $perPage);
if ($totalItems % $perPage != 0) {
    $totalPage++;
}
?>
    <style>
        .search-title{
            width: 220px;
            background: #1aba9b;
            color: whitesmoke;
            clip-path: polygon(0 0%, 87% 0%, 100% 100%, 0% 100%);
            padding: 2px 10px;
            margin: 1px;
        }
    </style>
    <section>
        <div style="min-height: 450px;" class="container-fluid d-inline-block float-none">
            <div class="row">
                <div class="col-12 col-md-12 wrapper8" style="height:100%">
                    <div class="main-block">
                        <div class="block-title">
                            <h5 style="float: right;">page <?php echo (($totalPage == 0)?0:$pageNumber) . '/'. $totalPage;?></h5>
                            <h5 class="search-title">Search Result (<?php echo $totalItems;?>)</h5>
                        </div>
                        <div class="block">
                            <?php
                                if (!empty($searchText)){
                                    $items = Book::getSearchBookResult($perPage,$offset,$userLoginId,'date',$searchText);
                                    if (!$items){
                                        echo '<div style="min-height: 400px; width: 100%; display: flex; justify-content: center; align-items: center;">
                                            <div>
                                                <center><img width="60px" src="../images/not-found.png" alt=""></center>
                                                <center><p style="font-weight: 600; margin-top: 5px; color: #bc0000;">There is no book found!</p></center>                                          
                                            </div>
                                         </div>';
                                    }
                                }else{
                                    $items = false;
                                    echo '<div style="min-height: 400px; width: 100%; display: flex; justify-content: center; align-items: center;">
                                            <div>
                                                <center><img width="60px" src="../images/not-found.png" alt=""></center>
                                                <center><p style="font-weight: 600; margin-top: 5px; color: #bc0000;">There is no book found!</p></center>                                          
                                            </div>
                                         </div>';
                                }
                            ?>
                            <div class="book-wrapper" style="position: relative">
                            <?php
                                if($items) {
                                    foreach ($items as $item) {
                                        $meanRate = intval($item->mean_rate);
                                        ?>
                                        <div id="book-container<?php echo $item->id; ?>" class="book-container">
                                            <a href="book_detail.php?book=<?php echo $item->id ?>">
                                                <div class="img-container">
                                                    <img src="../images/<?php echo $item->book_cover;?>" alt="book">
                                                </div>
                                            </a>

                                            <div id="heart<?php echo $item->id;?>" class="mark-icon <?php echo ($item->markId > 0)?'active':''; ?>">

                                                <a title="Mark" class="favorite heart" href="#id=<?php echo $item->id ?>">
                                                    <i class="fas fa-heart"></i>
                                                </a>
                                                <div <?php echo ($userLoginId == $item->user_id)?'':'hidden';?> class="modify-dropdown">
                                                    <button  class="modify-button">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </button>
                                                    <div class="modify-dropdown-content">
                                                        <div class="modify-shape"></div>
                                                        <a title="Delete" class="last-favorite delete" href="#id=<?php echo $item->id ?>">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                        <a title="Edit" class="last-favorite" href="upload.php?book=<?php echo $item->id ?>">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="info-container">
                                                <div class="emoji">
                                                    <img src="../images/free.png" alt="free" width="40px" height="40px">
                                                </div>
                                                <div class="rating-group">
                                                    <section class="rating-widget">
                                                        <div class="rating-stars text-center">
                                                            <ul id="stars">
                                                                <li title="Poor" data-value="1" <?php echo ($meanRate >= 1)?'class="star hover"':'class="star white-star"'?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Fair" data-value="2" <?php echo ($meanRate >= 2)?'class="star hover"':'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Good" data-value="3" <?php echo ($meanRate >= 3)?'class="star hover"':'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Very Good" data-value="4" <?php echo ($meanRate >= 4)?'class="star hover"':'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                                <li title="Excellent" data-value="5" <?php echo ($meanRate >= 5)?'class="star hover"':'class="star white-star"' ?>>
                                                                    <i class="fa fa-star fa-fw"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </section>
                                                </div>
                                                <div class="info">
                                                    <div class="info_group">
                                                        <ul>
                                                            <li><?php echo $item->title; ?></li>
                                                            <li><?php ?></li>
                                                            <li><?php echo $item->published_date; ?></li>
                                                        </ul>
                                                    </div>
                                                    <div class="info_group">
                                                        <p><?php echo $item->view_number .(($item->view_number > 1)?" views":" view");?></p>
                                                        <p><?php echo $item->download_number .(($item->download_number > 1)?" downloads":" download"); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            ?>
                            </div>
                        </div>

                        <?php
                        if($items > 0) { ?>
                            <div style="display: flex; align-items: center;" class="page-info">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <?php
                                        if ($pageNumber > 1) {
                                            echo "<li class='page-item'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=1'>First</a></li>";
                                            echo "<li class='page-item'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=" . ($pageNumber - 1) . "'>Previous</a></li>";
                                            echo "<li class='page-item'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=" . ($pageNumber - 1) . "'>" . ($pageNumber - 1) . "</a></li>";
                                        }
                                        echo "<li class='page-item active'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=" . $pageNumber . "'>$pageNumber</a></li>";
                                        if ($pageNumber < $totalPage) {
                                            echo "<li class='page-item'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=" . ($pageNumber + 1) . "'>" . ($pageNumber + 1) . "</a></li>";
                                            echo "<li class='page-item'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=" . ($pageNumber + 1) . "'>Next</a></li>";
                                            echo "<li class='page-item'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=" . $totalPage . "'>Last</a></li>";
                                        }
                                        if ($pageNumber > $totalPage) {
                                            echo "<li class='page-item'><a class='page-link' href='search_result.php?search_value=" . $searchText . "&p=" . $totalPage . "'>Last</a></li>";
                                        }
                                        ?>
                                    </ul>
                                </nav>
                            </div>
                            <?php
                        }
                        ?>

                    </div>

                </div>
            </div>
        </div>
        </div>
    </section>
    <script>
        window.onload = function () {
            var searchText = "<?php echo $searchText; ?>";
            $("#txtSearch").val(searchText);
        }
    </script>
<?php
include "footer.php";
?>