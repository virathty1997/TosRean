<?php
include 'header.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

/*Get User Logged in ID*/
$userLoginId = isset($_SESSION['userLoginId']) ? $_SESSION['userLoginId'] : 0;

$maxCoverFileSize = 10000000; /*10M*/ /*allow equal or less than 10 MB*/
$maxBookFileSize = 10000000; /*10M*/ /*allow equal or less than 10 MB*/

include_once "../config/DbConfig.php";
include_once "../model/BookDetail.php";
include_once "../model/Book.php";

$titleEmpty = false;
$authorEmpty = false;
$genresEmpty = false;
$publishEmpty = false;

$coverError = false;
$coverInvalid = false;
$coverLarger = false;

$bookError = false;
$bookInvalid = false;
$bookLarger = false;

$bookFileListEmpty = false;

$uploadSuccess = false;
$updateUploadId = 0;

$title;
$author;
$genres;
$publish;
$description;

$bookCover;
$bookUpdate;

if (isset($_GET['book']) && is_numeric($_GET['book'])) {
    /*Get Data if user click on Edit icon on Upload History Tab of table record*/
    $updateUploadId = $_GET['book'];
    $bookUpdate = Book::get_book_by_Id($updateUploadId);
    if ($bookUpdate) {
        $title = $bookUpdate->title;
        $author = $bookUpdate->author;
        $genres = $bookUpdate->genres;
        $publish = $bookUpdate->published_date;
        $description = $bookUpdate->description;
        $bookCover = '../images/' . $bookUpdate->book_cover;
    }
}
?>

<link rel="stylesheet" href="../assets/css/upload.css">
<div class="upload-container">
    <div class="upload-wrapper">
        <main>
            <input class="tab-checkbox" id="tab1" type="radio" name="tabs" value="tab1" checked>
            <label class="tab-label" for="tab1">Upload</label>

            <input class="tab-checkbox" id="tab2" type="radio" name="tabs" value="tab2">
            <label class="tab-label" for="tab2">Upload History</label>

            <input class="tab-checkbox" id="tab3" type="radio" name="tabs" value="tab3">
            <label class="tab-label" for="tab3">Marked</label>

            <section class="all-content" id="content1">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="img-container">
                            <img src="<?php echo isset($bookCover) ? $bookCover : '../assets/images/broken.png'; ?>"
                                 alt="book" id="broken-img">
                        </div>
                        <div class="form-group">
                            <label class="tab-label"
                                   style="border: 1px solid rgba(47,241,116,0.57); border-radius: 4px; margin-top:10px; width: 100%"
                                   for="cover-file">Choose cover...</label>
                            <input class="tab-checkbox" type="file" name="cover_file" id="cover-file" class="file" accept="image/jpg, image/jpeg, image/png, image/gif">
                        </div>
                        <div class="form-group">
                            <label id="add-label-icon" class="tab-label" for="file-img">
                                <i class="fas fa-plus-circle add-icon"></i>Add File</label>
                            <input class="tab-checkbox" type="file" name="book_files[]" id="file-img" class="file" multiple="multiple"  accept="application/pdf, image/jpg, image/jpeg, image/png, image/gif">
                        </div>
                        <div class="attachment-container">
                            <table id="table-attachment" border="1" class="table">
                                <col width="30px"/>
                                <col width="150px"/>
                                <col width="50px"/>
                                <thead>
                                    <tr>
                                        <th class="header-attachment" scope="col">No</th>
                                        <th class="header-attachment" scope="col">File Name</th>
                                        <th class="header-attachment" scope="col">Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $bookDetails = BookDetail::getBookDetailById($updateUploadId);
                                        if ($bookDetails){
                                            for($i =0; $i < count($bookDetails); $i++){
                                                $recordId = 'attachment'.$i;
                                                echo '<tr id="'.$recordId.'">'.
                                                        '<td align="center">'.($i+1).'</td>'.
                                                            '<td>'.$bookDetails[$i]->book_file .'</td>'.
                                                            '<td align="center"><i class="fas fa-trash-alt attachment-item" onclick="doDeleteAttachment('.$i.')"></i></td>'.
                                                     '</tr>';
                                            }
                                        }else{
                                            echo '<tr><td align="center" colspan="3">No attachment file.</td></tr>';
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <div>
                                <p hidden id="attachment-file-error" class="upload-text-error">Please add attachment file to upload.</p>
                                <p style="color: #555555; font-weight: 400;">You are able to add <strong>5</strong> files in one upload. <br>The accepted files are *.pdf, *.jpg, *.jpeg, *.png, *.gif</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="title" class="bootstrap-label">Title <strong style="color: rgba(255,0,0,0.65); font-size: 15px;">*</strong></label>
                            <p hidden id="title-error" class="upload-text-error">Title field is required.</p>
                            <input type="text" class="form-control" id="title" name="title" value="<?php echo (isset($title) && !$uploadSuccess) ? $title : '' ?>">
                        </div>
                        <div class="form-group">
                            <label for="author" class="bootstrap-label">Author <strong style="color: rgba(255,0,0,0.65); font-size: 15px;">*</strong></label>
                            <p hidden id="author-error" class="upload-text-error">Author field is required.</p>
                            <input type="text" class="form-control" id="author" name="author" value="<?php echo (isset($author) && !$uploadSuccess) ? $author : '' ?>">
                        </div>
                        <div class="form-group">
                            <label for="genres" class="bootstrap-label">Genres <strong style="color: rgba(255,0,0,0.65); font-size: 15px;">*</strong></label>
                            <p hidden id="genres-error" class="upload-text-error">Genres field is required.</p>

                            <select class="form-control" id="genres" name="genres">
                                <option value=""> -- Select --</option>
                                <option value="Business">Business</option>
                                <option value="Cartoon">Cartoon</option>
                                <option value="Political">Political</option>
                                <option value="Novel">Novel</option>
                                <option value="Moeys">Moeys</option>
                                <option value="Science">Science</option>
                                <option value="Law">Law</option>
                                <option value="Economic">Economic</option>
                                <option value="Legend">Legend</option>
                                <option value="Health">Health</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="publish" class="bootstrap-label">Published Date <strong
                                style="color: rgba(255,0,0,0.65); font-size: 15px;">*</strong></label>
                                <p hidden id="publish-error" class="upload-text-error">Published date field is required.</p>
                               <input type="date" class="form-control" id="publish" name="publish" value="<?php echo (isset($publish) && !$uploadSuccess) ? $publish : '' ?>">
                       </div>
                        <div class="form-group">
                            <label for="description" class="bootstrap-label">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3"><?php echo (isset($description) && !$uploadSuccess) ? $description : '' ?></textarea>
                        </div>
                        <div class="form-group">
                            <button type="button" name="submit" class="btn btn-success" id="btn_submit" style="background: #28b1b0; color: #fff;">
                                <i class="fas fa-cloud-upload-alt"></i>
                                <?php echo ($updateUploadId > 0) ? 'Update' : 'Upload'; ?>
                            </button>
                            <button style="color: #555555;" type="button" name="cancel" class="btn" id="btn_cancel" >
                                <i class="fas fa-ban"></i> Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </section>

            <section class="all-content" id="content2">
                <div style="display: flex;" class="filter">
                    <div class="sort">
                        <select id="sortby" class="selectpicker form-control sort-select">
                            <option value="date">Date</option>
                            <option value="title">Title</option>
                            <option value="author">Authors</option>
                            <option value="genres">Genres</option>
                            <option value="published">Published</option>
                            <option value="view">View</option>
                            <option value="download">Download</option>
                            <option value="rate">Rate</option>
                            <option value="mean-rate">Mean Rate</option>
                        </select>
                    </div>
                    <div class="display">
                        <select id="display" class="selectpicker form-control display-select">
                            <option value="grid">Grid</option>
                            <option value="item">Item</option>
                        </select>
                    </div>

                    <div style="align-self: flex-end;" class="search">
                        <input type="text" class="form-control search-input" placeholder="Search" id="search-upload"
                               name="search-upload"/>
                    </div>
                </div>
                <div style="border-top: 2px solid #1aba9b; padding-top: 5px; " class="upload-detail" id="upload-detail">

                </div>
        </section>

            <section class="all-content" id="content3">
                <div class="main-block">
                    <div class="block-title">
                        <h5>Marked Book</h5>
                    </div>
                    <div style="min-height: 200px;" class="block">
                            <?php
                            $items = Book::get_marked_book($userLoginId);
                            if(!$items){?>
                                <div style="min-height: 300px; width: 100%; display: flex; justify-content: center; align-items: center;">
                                    <div>
                                       <center><img width="60px" src="../images/not-found.png" alt=""></center>
                                       <center><p style="font-weight: 600; margin-top: 5px; color: #bc0000;">There is no marked book!</p></center>
                                    </div>
                                </div>
                            <?php
                            }else {?>
                            <div class="book-wrapper" style="position: relative">
                                <?php
                                foreach ($items as $item) {
                                    $meanRate = intval($item->mean_rate);
                                    ?>

                                    <div id="book-container<?php echo $item->id; ?>" class="book-container">
                                        <a href="book_detail.php?book=<?php echo $item->id ?>">
                                            <div class="img-container">
                                                <img src="../images/<?php echo $item->book_cover; ?>" alt="book">
                                            </div>
                                        </a>

                                        <div id="heart<?php echo $item->id; ?>" class="mark-icon active">

                                            <a title="Mark" class="favorite heart" href="#id=<?php echo $item->id ?>">
                                                <i class="fas fa-heart"></i>
                                            </a>
                                            <div <?php echo ($userLoginId == $item->user_id) ? '' : 'hidden'; ?>
                                                    class="modify-dropdown">
                                                <button class="modify-button">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </button>
                                                <div class="modify-dropdown-content">
                                                    <div class="modify-shape"></div>
                                                    <a title="Delete" class="last-favorite delete"
                                                       href="#id=<?php echo $item->id ?>">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </a>
                                                    <a title="Edit" class="last-favorite"
                                                       href="upload.php?book=<?php echo $item->id ?>">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                        <div style="display: block;" class="info-container">
                                            <div class="emoji">
                                                <img src="../images/free.png" alt="free" width="40px" height="40px">
                                            </div>
                                            <div class="rating-group">
                                                <div class="rating-widget">
                                                    <div class="rating-stars text-center">
                                                        <ul id="stars">
                                                            <li title="Poor"
                                                                data-value="1" <?php echo ($meanRate >= 1) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                <i class="fa fa-star fa-fw"></i>
                                                            </li>
                                                            <li title="Fair"
                                                                data-value="2" <?php echo ($meanRate >= 2) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                <i class="fa fa-star fa-fw"></i>
                                                            </li>
                                                            <li title="Good"
                                                                data-value="3" <?php echo ($meanRate >= 3) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                <i class="fa fa-star fa-fw"></i>
                                                            </li>
                                                            <li title="Very Good"
                                                                data-value="4" <?php echo ($meanRate >= 4) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                <i class="fa fa-star fa-fw"></i>
                                                            </li>
                                                            <li title="Excellent"
                                                                data-value="5" <?php echo ($meanRate >= 5) ? 'class="star hover"' : 'class="star white-star"' ?>>
                                                                <i class="fa fa-star fa-fw"></i>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <div class="info_group">
                                                    <ul>
                                                        <li><?php echo $item->title; ?></li>
                                                        <li><?php ?></li>
                                                        <li><?php echo $item->published_date; ?></li>
                                                    </ul>
                                                </div>
                                                <div class="info_group">
                                                    <p><?php echo $item->view_number . (($item->view_number > 1) ? " views" : " view"); ?></p>
                                                    <p><?php echo $item->download_number . (($item->download_number > 1) ? " downloads" : " download"); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                            }?>
                        </div>
                    </div>
                </div>

            </section>
        </main>
    </div>
</div>


<!-- Button trigger modal -->
<button hidden id="popup-success" type="button" class="btn btn-primary" data-toggle="modal"
        data-target="#dialog-success">Pop up
</button>

<!-- Modal -->
<div class="modal fade" id="dialog-success" tabindex="-1" role="dialog" aria-labelledby="dialog-upload-label"
aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dialog-upload-label">Upload Book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div  style="display: flex;" class="modal-body">
                <i style="color: rgba(32,139,33,0.83); font-size: 20px; margin-right: 10px" class="fas fa-check"></i>
                <span id="upload-dialog-msg">You have uploaded new book successfully.</span>
            </div>
            <div class="modal-footer">
                <button type="button" style="background: #0dadb1;" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Button trigger modal -->
<button hidden id="popup-error" type="button" class="btn btn-primary" data-toggle="modal"
        data-target="#dialog-error">Pop up
</button>

<!-- Modal -->
<div class="modal fade" id="dialog-error" tabindex="-1" role="dialog" aria-labelledby="dialog-upload-label"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dialog-upload-label-error">Upload Book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div style="display: flex;"  class="modal-body">
                <i style="color: #bd0000; font-size: 25px; margin-right: 10px" class="fas fa-exclamation-circle"></i>
                <span id="upload-dialog-msg-error">Your book was not uploaded successfully.</span>
            </div>
            <div class="modal-footer">
                <button type="button" style="background: #0dadb1;" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
    var gTab2Selected = false;
    var gUserLoginId = <?php echo $userLoginId;?>;
    var gSearchText = "<?php echo isset($_SESSION['searchText']) ? $_SESSION['searchText'] : '';?>";
    var gSelectedTab = "<?php echo isset($_SESSION['selectedTab']) ? $_SESSION['selectedTab'] : 'tab1';?>";
    var gSortBy = "<?php echo isset($_SESSION['setSortBy']) ? $_SESSION['setSortBy'] : 'date';?>";
    var gViewType = "<?php echo isset($_SESSION['setViewType']) ? $_SESSION['setViewType'] : 'grid';?>";
    var gUploadId = <?php echo $updateUploadId;?>;
    var gNewUploadBooks = [];
    var gUpdateBooks = [];
    var gRemovedUpdateFiles = [];
    var gCoverFile;
    var gMaxFiles = 5;

    window.onload = function () {
        // setScreenSize();
        setSelectedValueToGenres();
        checkSelectedTab();
        loadUpdateBookFileToArray();
    }


    function loadUpdateBookFileToArray(){
        <?php
        $bookDetails = BookDetail::getBookDetailById($updateUploadId);
        if ($bookDetails){
            for($i =0; $i < count($bookDetails); $i++){
            ?>
                gUpdateBooks.push('<?php echo $bookDetails[$i]->book_file;?>');
                <?php
            }
        }
        ?>
    }


    function checkSelectedTab() {
        if (gSelectedTab == 'tab2') {
            loadUploadBook(gUserLoginId, gViewType, gSortBy, gSearchText);
        }
    }

    function setSelectedValueToGenres() {
        var genres = "<?php echo (isset($genres) && !$uploadSuccess) ? $genres : '';?>";
        $("#genres").val(genres);

        $("#search-upload").val(gSearchText);
        $("#sortby").val(gSortBy);
        $("#display").val(gViewType);
    }

    $("document").ready(function () {
        $(".tab-slider--body").hide();
        $(".tab-slider--body:first").show();


        //after choose display image immediately

        function readUrl(url) {
            if (url.files && url.files[0]) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    $('#broken-img').attr('src', event.target.result);
                };
                reader.readAsDataURL(url.files[0]);
            }
        }

        $("#cover-file").change(function () {
            if (!this.files[0].name.match(/.(jpg|jpeg|png|gif)$/i)){
                $("#dialog-upload-label-error").html('Select Image');
                $("#upload-dialog-msg-error").html('Your selected image file is not allowed. <br><br><strong>Note</strong>: The accepted image extension are *.jpg, *.jpeg, *.png, *.gif.');
                $('#popup-error').trigger('click');
                return;
            }else if (this.files[0].size > 2097152) { //2MB
                $("#dialog-upload-label-error").html('Select Image');
                $("#upload-dialog-msg-error").html('Your selected image file is too large. The maximum size is 2 MB');
                $('#popup-error').trigger('click');
                return;
            }
            readUrl(this);
            gCoverFile = this.files[0];
        });
    });


    $(".tab-checkbox").click(function () {

        var selectedTab = $('input:radio[name=tabs]:checked').val();
        if (selectedTab == 'tab1') {

        } else if (selectedTab == 'tab2' && gTab2Selected == false) {
            gTab2Selected = true;
            /*set status to TRUE to not retrieve data again with when click On it later*/
            if (gSelectedTab != selectedTab) {
                //alert('retrieve');
                loadUploadBook(gUserLoginId, gViewType, gSortBy, gSearchText);
            }
        } else if (selectedTab == 'tab3') {

        }

        gSelectedTab = selectedTab;
        setOperationSession(gSelectedTab, gSortBy, gViewType, gSearchText);
    });

    /*When use select Sort ComboBox*/
    $('#sortby').on('change', function () {

        if (gSortBy != this.value) {
            //alert('retrieved');
            gSortBy = this.value;
            loadUploadBook(gUserLoginId, gViewType, gSortBy, gSearchText);
            setOperationSession(gSelectedTab, gSortBy, gViewType, gSearchText);
        }
    })

    /*When use select Display(View Type) ComboBox*/
    $('#display').on('change', function () {

        if (this.value != gViewType) {
            //alert('retrieved');
            gViewType = this.value;
            loadUploadBook(gUserLoginId, gViewType, gSortBy, gSearchText);
            setOperationSession(gSelectedTab, gSortBy, gViewType, gSearchText);
        }
    })


    /*User input text and press key Enter to Search Upload Book*/
    $("#search-upload").on('keydown', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            var searchText = this.value;
            if (searchText == gSearchText) return;
            else {
                //alert('retrieve');
                gSearchText = searchText;
                loadUploadBook(gUserLoginId, gViewType, gSortBy, gSearchText);
                setOperationSession(gSelectedTab, gSortBy, gViewType, gSearchText);
            }
        }
    });

    function loadUploadBook(userLoginId, viewType, sortBy, searchText) {
        $("#upload-detail").load("../action/load_upload_history.php", {
            userLoginId: userLoginId,
            viewType: viewType,
            sortBy: sortBy,
            searchText: searchText
        }, function () {

        });
    }

     function doDelete(uploadId) {
         $('#button-popup-delete').trigger('click'); /*remote to pop up dialog*/

         $("#button-delete").click(function () {

             var data = new FormData();
             data.append("uploadId", uploadId);
             $.ajax({
                 url: "../action/delete_book.php",
                 method: "POST",
                 data: data,
                 contentType: false,
                 cache: false,
                 processData: false,
                 success: function (result) {
                     if (result > 0 || Number(result) > 0) {
                         if (gViewType == 'grid'){
                             var tableIndex = '#book' + uploadId;
                             $(tableIndex).remove();
                             if ($("#upload-table tr").length == 1) {
                                 $("#upload-table").append('<tr><td align="center" colspan="12">There is no record.</td> </tr>');
                             }
                         }else if (gViewType == 'item') {
                             var id = '#book-container'+uploadId;
                             $(id).remove();
                         }
                     }
                 },
                 error: function (e) {
                     console.log(e);
                 }
             });
         })
    }



    function doDeleteClick(uploadId) {
        event.preventDefault();
        doDelete(uploadId);
    }

    function doHeartClick(uploadId) {
        event.preventDefault();

        if (gUserLoginId == undefined || gUserLoginId == null || gUserLoginId == '') {
            alert('Please login your account!');
            return;
        }
        var data = new FormData();
        data.append("uploadId", uploadId);
        data.append("userLoginId", gUserLoginId);
        $.ajax({
            url: '../model/mark.php',
            method: 'post',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data > 0 || Number(data) > 0) {

                    var heartId = '#heart' + uploadId;
                    if ($(heartId).hasClass('active')) {
                        $(heartId).removeClass('active');
                        //alert("You've unmarked this book");
                    } else {
                        $(heartId).addClass('active');
                        //alert("You've marked this book");
                    }
                }
            }
        });
    }

    function setOperationSession(selectedTab, sortBy, viewType, searchText) {
        var data = new FormData();
        data.append('selectedTab', selectedTab);
        data.append('sortBy', sortBy);
        data.append('viewType', viewType);
        data.append('searchText', searchText);
        $.ajax({
            url: "../action/selected_tab_session.php",
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data)
            }
        });
    }

    $('#file-img').change(function(){

        $("#attachment-file-error").prop('hidden','hidden');

        var updateCount = gUpdateBooks.length;
        var uploadCount = gNewUploadBooks.length;
        if ((updateCount + uploadCount) == gMaxFiles) return;

        var selectedLength = this.files.length;
        if (selectedLength == 0) return;

        var notAcceptFile = "";
        var tooLargeFile = "";
        for(var i = 0 ; i < selectedLength ; i++){
            if ((updateCount + gNewUploadBooks.length)  == gMaxFiles) break; //allow only 5 files in One Upload.

            if (!this.files[i].name.match(/.(pdf|jpg|jpeg|png|gif)$/i)){
                notAcceptFile += this.files[i].name + ' ; ';
                continue;
            }
            if (this.files[i].size > 10485760){ //10MB
                tooLargeFile += this.files[i].name + ' ; ';
                continue;
            }

            var element = this.files[i];
            if(jQuery.inArray(element, gNewUploadBooks) === -1){
                gNewUploadBooks.push(element);
            }else{
                alert('This '+element.name + ' is already chosen.');
            }
        }

        loadBookToTable();

        if (notAcceptFile != "" || tooLargeFile != ""){
            var description = "";
            if (notAcceptFile != ""){
                description = 'Not accepted file : '+ notAcceptFile + '<br><br>';
            }
            if (tooLargeFile != ""){
                description += 'Too large files( Max 10MB) : '+ tooLargeFile +'<br><br>';
            }
            description += '* The accepted file are *.pdf, *.jpg, *.jpeg, *.png, *.gif';
            $("#dialog-upload-label-error").html('Select Book File');
            $("#upload-dialog-msg-error").html(description);
            $('#popup-error').trigger('click');
        }
        //$(this).replaceWith($(this).val('').clone(true));

    });

    function doDeleteAttachment(index) {
        var tableIndex = '#attachment' + index;
        $(tableIndex).remove();
        if (gUploadId > 0){
            if (index < gUpdateBooks.length && gUpdateBooks.length > 0){
                var removed = gUpdateBooks.splice(index,1);
                gRemovedUpdateFiles.push(removed);
            }else{
                index = index - gUpdateBooks.length;
                gNewUploadBooks.splice(index,1); //Remove file one by one from array
            }
        }else{
            gNewUploadBooks.splice(index,1); //Remove file one by one from array
        }
        //loadBookToTable();
    }

    function  loadBookToTable() {

        $("#table-attachment tbody tr").remove(); //clear all table rows
        if ((gNewUploadBooks.length + gUpdateBooks.length) <= 0){
            var row =  '<tr> <td align="center" colspan="3">No attachment file.</td></tr>';
            $('#table-attachment >tbody:last-child').append(row);
            return;
        }

        var count = 0;
        for(var i = 0 ; i < gUpdateBooks.length ; i++){
            var recordId = 'attachment'+count; //generate new id for table record => easy to delete

            var row = '<tr id="'+recordId+'">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td>'+gUpdateBooks[i]+'</td>' +
                '<td align="center"><i class="fas fa-trash-alt attachment-item" onclick="doDeleteAttachment('+count+')"></i></td>' +
                '</tr>';
            $('#table-attachment >tbody:last-child').append(row);

            count++;
        }

        for(var i = 0 ; i < gNewUploadBooks.length ; i++){
            var fileName = gNewUploadBooks[i].name;
            var recordId = 'attachment'+count; //generate new id for table record => easy to delete

            var row = '<tr id="'+recordId+'">' +
                '<td align="center">'+(count+1)+'</td>' +
                '<td>'+fileName+'</td>' +
                '<td align="center"><i class="fas fa-trash-alt attachment-item" onclick="doDeleteAttachment('+count+')"></i></td>' +
                '</tr>';
            $('#table-attachment >tbody:last-child').append(row);
            count++;
        }
    }

    $("#add-label-icon").click(function () {
        if ((gUpdateBooks.length + gNewUploadBooks.length)  == gMaxFiles){

            $("#dialog-upload-label-error").html('Add Book File');
            $("#upload-dialog-msg-error").html('You are unable to add more book. You can upload only 5 different book files in one time.');
            $('#popup-error').trigger('click');
            event.preventDefault();
        }
    })


    $("#btn_submit").click(function () {

        if ($("#title").val() == '') $("#title-error").removeAttr('hidden');
        else $("#title-error").prop('hidden','hidden');

        if ($("#author").val() == '') $("#author-error").removeAttr('hidden');
        else $("#author-error").prop('hidden','hidden');

        if ($("#genres").val() == '') $("#genres-error").removeAttr('hidden');
        else $("#genres-error").prop('hidden','hidden');

        if ($("#publish").val() == '') $("#publish-error").removeAttr('hidden');
        else $("#publish-error").prop('hidden','hidden');

        var lengths = gNewUploadBooks.length + gUpdateBooks.length;

        if (lengths == 0) $("#attachment-file-error").removeAttr('hidden');
        else $("#attachment-file-error").prop('hidden','hidden');

        if ($("#title").val() == '' || $("#author").val() == '' || $("#genres").val() == '' || $("#publish").val() == '' || lengths == 0){

        }
        else submitBook();
    });

    function submitBook(){
        var data = new FormData();
        var title = $("#title").val();
        var author = $("#author").val();
        var genres = $("#genres").val();
        var publish = $("#publish").val();
        var description = $("#description").val();

        data.append('userLoginId',gUserLoginId);
        data.append('uploadId',gUploadId);
        data.append('title',title);
        data.append('author',author);
        data.append('genres',genres);
        data.append('publish',publish);
        data.append('description',description);
        data.append('cover_file',gCoverFile);

        for(var i = 0 ; i < gNewUploadBooks.length ; i++){
            var name = 'book_file'+(i+1);
            data.append(name,gNewUploadBooks[i]);
        }

        if (gUploadId > 0){
            var removeCount = gRemovedUpdateFiles.length;
            data.append('removeCount',removeCount);

            for (var i = 0; i< removeCount; i++){
                var name = 'remove'+(i+1);
                data.append(name,gRemovedUpdateFiles[i]);
            }
        }
        console.log(data);
        $.ajax({
            url: '../action/upload_book.php',
            type: 'post',
            data: data,
            dataType:'json',
            contentType: false,
            processData: false,
            beforeSend: function(){
                showPleaseWait();
            },
            complete: function(){
                hidePleaseWait();
            },
            success: function(result) {
                console.log('Resut : '+result);
                showFeedBack(result);
            },error: function (e) {
                console.log(e);
            }
        });
    }

    function showFeedBack(result){
        if (result['success'] == 1){
            if (gUploadId > 0){
                $("#dialog-upload-label").html('Update Book');
                $("#upload-dialog-msg").html('You have updated your book successfully.');
            }else{
                $("#dialog-upload-label").html('Upload Book');
                $("#upload-dialog-msg").html(' You have uploaded new book successfully.');
                ClearAllControls();
            }
            $('#popup-success').trigger('click');

            // set to retrieve result again when user click on tab 2 (Upload History)
            gTab2Selected = false;
            gSelectedTab = 'tab1';
        }else{
            if (gUploadId > 0){
                var description = 'Your book was not updated successfully.';

                /*Update Book*/
                description += checkFeedback(result);

                $("#dialog-upload-label-error").html('Update Book');
                $("#upload-dialog-msg-error").html(description);
            }else{
                var description = 'Your book was not uploaded successfully.';
                description += checkFeedback(result);

                $("#dialog-upload-label-error").html('Upload Book');
                $("#upload-dialog-msg-error").html(description);
            }
            $('#popup-error').trigger('click');
        }
    }

    function checkFeedback(result) {
        var description = '';
        var cover = result['cover'];
        var books = result['book'];

        if (cover != 0 && cover != null && cover != undefined){
            var coverName = cover['name'];
            description += '<br>';
            if(cover['not_accept']){
                description += '<br>'+coverName +' is not accept.';
            }
            if (cover['too_large']){
                description += '<br>Cover file '+coverName +' is too large. Max size is <strong>1</strong> MB.';
            }
            if (cover['error']){
                description += '<br>'+coverName +' is error while uploading.';
            }
            description += '<br>*If cover file is error, it will bet set to default image.';
        }
        if (books != 0 && books != null && books != undefined){
            description += '<br>';
            books.forEach(function (book) {
                var bookFileName = book['name'];
                if(book['not_accept']){
                    description += '<br>'+bookFileName +' is not accept.';
                }
                if (book['too_large']){
                    description += '<br>Book file '+bookFileName +' is too large. Max size is <strong>1</strong> MB.';
                }
                if (book['error']){
                    description += '<br>'+bookFileName +' is error while uploading.';
                }
            })
        }
        return description;
    }


    function ClearAllControls(){
        $("#title").val('')
        $("#author").val('');
        $("#genres").val('');
        $("#publish").val('');
        $("#description").val('');

        gNewUploadBooks = [];
        gRemovedUpdateFiles = [];
        gUpdateBooks = [];
        gCoverFile = null;

        $('#broken-img').attr('src', '../assets/images/broken.png'); //set image to default;
        loadBookToTable();

        console.log('gNewUploadBooks : '+gNewUploadBooks.length);
        console.log('gRemovedUpdateFiles : '+gRemovedUpdateFiles.length);
        console.log('gUpdateBooks : '+gUpdateBooks.length);
        console.log('gCoverFile : '+gCoverFile);
    }

    $("#btn_cancel").click(function () {
        location.reload();
    })


    function showPleaseWait() {
        var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
            <div class="modal-dialog">\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <h4 class="modal-title">Please wait...</h4>\
                    </div>\
                    <div class="modal-body">\
                        <div class="progress">\
                          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                          aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                          </div>\
                        </div>\
                    </div>\
                    </div>\
                </div>\
            </div>';
        $(document.body).append(modalLoading);
        $("#pleaseWaitDialog").modal("show");
    }

    function hidePleaseWait() {
        $("#pleaseWaitDialog").modal("hide");
    }

</script>

<?php
include_once "footer.php";
?>
