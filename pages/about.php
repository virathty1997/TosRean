<?php
include_once "header.php";
?>

<link rel="stylesheet" href="../assets/css/about.css">

<div class="team-boxed">
    <div class="container">

        <div class="row people">
            <div class="col-md-6 col-lg-4 item">
                <div class="box"><img class="rounded-circle" src="../assets/images/viroth.jpg"/>
                    <h3 class="name">Ty Viroth</h3>
                    <p class="title">RUPP</p>
                    <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent
                        aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam
                        dictum feugiat tellus, a semper massa.</p>
                    <div class="social">
                        <a href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-twitter-square"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 item">
                <div class="box"><img class="rounded-circle" src="../assets/images/thoura.jpg"/>
                    <h3 class="name">Lai Thoura</h3>
                    <p class="title">RUPP</p>
                    <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent
                        aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam
                        dictum feugiat tellus, a semper massa.</p>
                    <div class="social">
                        <a href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-twitter-square"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 item">
                <div class="box"><img class="rounded-circle" src="../assets/images/chenda.jpg"/>
                    <h3 class="name">Yorn Chanda</h3>
                    <p class="title">RUPP</p>
                    <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent
                        aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam
                        dictum feugiat tellus, a semper massa.</p>
                    <div class="social">
                        <a href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-twitter-square"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
