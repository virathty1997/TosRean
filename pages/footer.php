<?php
    require_once 'confirm_delete.php';
?>

<footer>
    <h4>Contact us</h4>
    <div class="social-icons">
        <a href="https://www.facebook.com/chanthoeurnfallingheart?hc_ref=ARQv0AqfEqkzsWQn4zqiEd7OKmC4qOJ-11-ji7fFDHZuI9dymEsXNKJcIKrjcNXhOmk">
            <i class="fab fa-facebook-f"></i>
        </a>
        <a href="#">
            <i class="fab fa-twitter"></i>
        </a>
        <a href="#">
            <i class="fab fa-linkedin-in"></i>
        </a>
        <a href="#">
            <i class="fab fa-google"></i>
        </a>
        <a href="#">
            <i class="fab fa-instagram"></i>
        </a>
        <button onclick="topFunction()" id="button-top" title="Go to top">Top</button>
    </div>
    <p>Copyright © 2018 by rean post</p>
</footer>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/index.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/js/notification.js"></script>
</body>

</html>