<?php
require_once "../config/DbConfig.php";
require_once "../model/Comment.class.php";
require_once "../model/Reaction.class.php";
require_once "../model/UploadNotification.class.php";

if (isset($_POST['userLoginId'])){
    $userLoginId = $_POST['userLoginId'];
    if ($userLoginId > 0){
        $comment = Comment::changeAllCommentToSeenByUserId($userLoginId);
        $reaction = Reaction::changeAllReactionToSeenByUserId($userLoginId);
        $upload = UploadNotification::changeAllNewUploadToSeen($userLoginId);
        echo ($comment+$reaction+$upload);
    }
}
