<?php
    if (isset($_POST['submit'])){
        $filename = $_POST['filename'];
        $getExt = explode('.', $_POST['filename']);
        $extension = end($getExt);
        header("Content-disposition: attachment; filename=$filename");
        header("Content-type: application/jpg");
        readfile($filename);
    }

?>