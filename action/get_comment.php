<?php
require_once "../config/DbConfig.php";
require_once "../model/Comment.class.php";

    if (isset($_POST['commentId'])){
        $id = $_POST['commentId'];
        if ($id > 0){
            $comment = Comment::getById($id);
            if ($comment){
                $result = array($comment->id,$comment->createdDate,$comment->userId,$comment->fullName,$comment->profile,$comment->uploadId,$comment->commentText,$comment->likeNumber,$comment->dislikeNumber,$comment->isEdited);
                echo json_encode($result);
            }
        }else {
            echo 0;
        }
    }
?>