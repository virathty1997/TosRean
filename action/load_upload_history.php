<?php
require_once '../config/DbConfig.php';
require_once '../model/Book.php';

$userLoginId = isset($_POST['userLoginId'])?$_POST['userLoginId']:0;
$viewType = isset($_POST['viewType'])?$_POST['viewType']:'grid';
$sortBy = isset($_POST['sortBy'])?$_POST['sortBy']:'date';
$searchText = isset($_POST['searchText'])?$_POST['searchText']:'';
if ($userLoginId > 0){
    $books = Book::getUploadedBookByUser($userLoginId,$sortBy,$searchText);
    if ($books){
        if ($viewType == 'item'){
            echo viewAsItem($books,$userLoginId);
        }else {
            echo viewAsTable($books);
        }
    }else{
        if ($viewType == 'item'){
            echo '<div style="min-height:300px; width: 100%; display: flex; justify-content: center; align-items: center;">
                    <div>
                       <center><img width="60px" src="../images/not-found.png" alt=""></center>
                       <center><p style="font-weight: 600; margin-top: 5px; color: #bc0000;">There is no upload history</p></center>
                    </div>
                </div>';
        }else {
            echo  emptyTable();
        }
    }
}


function viewAsItem($items,$userLoginId){
    $bookItem = '';
            if ($items == false) return;
            foreach ($items as $item) {
                $meanRate = intval($item->mean_rate);

$bookItem .=  '<div id="book-container'. $item->id.'" class="book-container">'.
                '<a href="book_detail.php?book='.$item->id .'">'.
                    '<div class="img-container">'.
                        '<img src="../images/'. $item->book_cover.'" alt="book">'.
                    '</div>'.
                '</a>'.
                '<div id="heart'. $item->id .'" class="mark-icon '. (($item->markId > 0)?'active':'') .'">'.
                    '<a title="Mark" class="favorite heart" onclick="doHeartClick('. $item->id .')" href="#">'.
                        '<i class="fas fa-heart"></i>'.
                    '</a>'.
                    '<div '. (($userLoginId == $item->user_id)?"":"hidden") .' class="modify-dropdown">'.
                        '<button class="modify-button">'.
                            '<i class="fas fa-ellipsis-v"></i>'.
                        '</button>'.
                        '<div class="modify-dropdown-content">'.
                            '<div class="modify-shape"></div>'.
                            '<a title="Delete" class="last-favorite delete" onclick="doDeleteClick('. $item->id .')" href="#">'.
                                '<i class="fas fa-trash-alt"></i>'.
                            '</a>'.
                            '<a title="Edit" class="last-favorite" href="upload.php?book='. $item->id .'">'.
                                '<i class="fas fa-edit"></i>'.
                            '</a>'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div style="display: block;" class="info-container">'.
                    '<div class="emoji">'.
                        '<img src="../images/free.png" alt="free" width="40px" height="40px">'.
                    '</div>'.
                   '<div class="rating-group">'.
                        '<div class="rating-widget">'.
                            '<div class="rating-stars text-center">'.
                                '<ul id="stars">'.
                                    '<li title="Poor" data-value="1" '. (($meanRate >= 1)?'class="star hover"':'class="star white-star"') .'>'.
                                        '<i class="fa fa-star fa-fw"></i>'.
                                    '</li>'.
                                    '<li title="Fair" data-value="2" '. (($meanRate >= 2)?'class="star hover"':'class="star white-star"') .'>'.
                                        '<i class="fa fa-star fa-fw"></i>'.
                                    '</li>'.
                                    '<li title="Good" data-value="3"  '. (($meanRate >= 3)?'class="star hover"':'class="star white-star"') .'>'.
                                        '<i class="fa fa-star fa-fw"></i>'.
                                    '</li>'.
                                    '<li title="Very Good" data-value="4"  '. (($meanRate >= 4)?'class="star hover"':'class="star white-star"') .'>'.
                                        '<i class="fa fa-star fa-fw"></i>'.
                                    '</li>'.
                                    '<li title="Excellent" data-value="5"  '. (($meanRate >= 5)?'class="star hover"':'class="star white-star"') .'>'.
                                        '<i class="fa fa-star fa-fw"></i>'.
                                    '</li>'.
                                '</ul>'.
                            '</div>'.
                        '</div>'.
                    '</div>'.
                    '<div class="info">'.
                        '<div class="info_group">'.
                            '<ul>'.
                                '<li>'. $item->title .'</li>'.
                                '<li></li>'.
                                '<li>'. $item->published_date .'</li>'.
                            '</ul>'.
                        '</div>'.
                        '<div class="info_group">'.
                            '<p>'. $item->view_number .(($item->view_number > 1)?" views":" view").'</p>'.
                            '<p>'. $item->download_number .(($item->download_number > 1)?" downloads":" download") .'</p>'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '</div>';
            }
    return $bookItem;
}

function viewAsTable($books){
    $header = '<table border="1" id="upload-table" class="table table-hover">'.
                '<col width="90px" />'.
                '<col width="200px" />'.
                '<col width="140px" />'.
                '<col width="80px" />'.
                '<col width="100px" />'.
                '<col width="50px" />'.
                '<col width="90px" />'.
                '<col width="50px" />'.
                '<col width="115px" />'.
                '<col width="50px" />'.
                '<col width="50px" />'.
                '<col width="60px" />'.
                '<thead>'.
                    '<tr>'.
                        '<th class="header-text" scope="col">Date</th>'.
                        '<th class="header-text" scope="col">Title</th>'.
                        '<th class="header-text" scope="col">Authors</th>'.
                        '<th class="header-text" scope="col">Genres</th>'.
                        '<th class="header-text" scope="col">Published</th>'.
                        '<th class="header-text" scope="col">View</th>'.
                        '<th class="header-text" scope="col">Download</th>'.
                        '<th class="header-text" scope="col">Rate</th>'.
                        '<th class="header-text" scope="col">Mean Rate</th>'.
                        '<th class="header-text" >View</th>'.
                        '<th class="header-text" >Edit</th>'.
                        '<th class="header-text" >Delete</th>'.
                    '</tr>'.
                '</thead>'.
                    '<tbody>';

                    $tableBody = '';
                        if (!$books){
                            $tableBody = '<tr><td align="center" colspan="12">There is no record.</td> </tr>';
                        }else {
                            $order = 0;
                            foreach ($books as $book) {
                                $meanRate = intval($book->mean_rate);
                                $uploadDate = date_format(new DateTime($book->date), "Y-m-d");
                                $order++;

              $tableBody .= '<tr id="book'. $book->id .'" class="data-record">'.
                                    '<td>'. $uploadDate .'</td>'.
                                    '<td>'. $book->title .'</td>'.
                                    '<td>'. $book->author.'</td>'.
                                    '<td>'. $book->genres.'</td>'.
                                    '<td align="center">'. $book->published_date.'</td>'.
                                    '<td align="center">'. $book->view_number.'</td>'.
                                    '<td align="center">'. $book->download_number.'</td>'.
                                    '<td align="center">'. $book->rate_count.'</td>'.
                                    '<td align="center">'.
                                        '<div class="rating-table">'.
                                            '<div class="rating-widget">'.
                                                '<div class="rating-stars text-center">'.
                                                    '<ul id="stars-table">'.
                                                        '<li title="Poor" data-value="1"'. (($meanRate >= 1) ? 'class="star hover"' : 'class="star"') .'>'.
                                                            '<i class="fa fa-star fa-fw"></i>'.
                                                        '</li>'.
                                                        '<li title="Fair" data-value="2"'. (($meanRate >= 2) ? 'class="star hover"' : 'class="star"') .'>'.
                                                            '<i class="fa fa-star fa-fw"></i>'.
                                                        '</li>'.
                                                        '<li title="Good" data-value="3"'. (($meanRate >= 3) ? 'class="star hover"' : 'class="star"') .'>'.
                                                            '<i class="fa fa-star fa-fw"></i>'.
                                                        '</li>'.
                                                        '<li title="Very Good" data-value="4"'. (($meanRate >= 4) ? 'class="star hover"' : 'class="star"') .'>'.
                                                            '<i class="fa fa-star fa-fw"></i>'.
                                                        '</li>'.
                                                        '<li title="Excellent" data-value="5"'. (($meanRate >= 5) ? 'class="star hover"' : 'class="star"') .'>'.
                                                            '<i class="fa fa-star fa-fw"></i>'.
                                                        '</li>'.
                                                    '</ul>'.
                                                '</div>'.
                                            '</div>'.
                                        '</div>'.
                                    '</td>'.
                                    '<td align="center"><a href="book_detail.php?book='. $book->id .'"><i style="color: #1a9a7c;" class="fas fa-glasses"></i></a></td>'.
                                    '<td align="center"><a href="upload.php?book='. $book->id .'"><i style="color: #1a9a7c;" class="fas fa-pencil-alt"></i></a></td>'.
                                    '<td align="center"><a href="#" onclick="doDelete('. $book->id .')"><i class="fas fa-trash-alt"></i></a></td>'.
                                '</tr>';
                            }/*[End foreach]*/
                        }/*[End] else $books not equal false;*/

    $footer = '</tbody></table>';
    return $header . $tableBody . $footer;
}


function emptyTable(){
    $header = '<table border="1" id="upload-table" class="table table-hover">
        <col width="90px" />
        <col width="200px" />
        <col width="140px" />
        <col width="80px" />
        <col width="100px" />
        <col width="50px" />
        <col width="90px" />
        <col width="50px" />
        <col width="115px" />
        <col width="50px" />
        <col width="50px" />
        <col width="60px" />
        <thead>
        <tr>
        <th class="header-text" scope="col">Date</th>
        <th class="header-text" scope="col">Title</th>
        <th class="header-text" scope="col">Authors</th>
        <th class="header-text" scope="col">Genres</th>
        <th class="header-text" scope="col">Published</th>
        <th class="header-text" scope="col">View</th>
        <th class="header-text" scope="col">Download</th>
        <th class="header-text" scope="col">Rate</th>
        <th class="header-text" scope="col">Mean Rate</th>
        <th class="header-text" >View</th>
        <th class="header-text" >Edit</th>
        <th class="header-text" >Delete</th>
        </tr>
        </thead>
        <tbody>
        <tr><td align="center" colspan="12">There is no upload history !</td></tr>
        </tbody></table>';

    return $header;
}