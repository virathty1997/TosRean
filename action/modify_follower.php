<?php
require_once "../config/DbConfig.php";
require_once "../model/Follower.class.php";

if (isset($_POST['posterId']) && isset($_POST['followerId'])){

    $posterId = $_POST['posterId'];
    $followerId = $_POST['followerId'];
    if ($posterId > 0 && $followerId > 0){
        $follow = new Follower();
        $follow->posterId = $posterId;
        $follow->followerId = $followerId;
        $follow->notify = $_POST['notify'];
        $follow->status = $_POST['status'];

        if (Follower::isExist($posterId,$followerId) == 0){
            //not found => add new record
            echo Follower::add($follow);
        }else{
            echo Follower::update($follow);
        }
    }else echo 0;

}