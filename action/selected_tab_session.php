<?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_POST['selectedTab'])){
        $_SESSION['selectedTab'] = $_POST['selectedTab'];
        $_SESSION['setSortBy'] = $_POST['sortBy'];
        $_SESSION['setViewType'] = $_POST['viewType'];
        $_SESSION['searchText'] = $_POST['searchText'];
        echo $_SESSION['selectedTab'].' '. $_SESSION['setSortBy'].' '.$_SESSION['setViewType'].' '.$_SESSION['searchText'];
    }