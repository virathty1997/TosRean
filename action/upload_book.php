<?php
if(!isset($_POST['uploadId']) && !isset($_POST['userLoginId']) && !isset($_POST['title']) && !isset($_POST['author']) && !isset($_POST['genres']) && !isset($_POST['publish']) && !isset($_POST['description'])){
    $response = array('success'=>0, 'cover'=> 0, 'book'=> 0);
    echo json_encode($response);
    return;
}

require_once '../config/DbConfig.php';
require_once '../model/Book.php';
require_once '../model/BookDetail.php';
require_once '../model/Follower.class.php';
require_once '../model/UploadNotification.class.php';
require_once '../model/Email.php';

$maxCoverFileSize = 2097152; /*2M*/ /*allow equal or less than 2 MB*/
$maxBookFileSize = 10485760; /*10M*/ /*allow equal or less than 10 MB*/
$allowBookFileExt = array('jpg', 'jpeg', 'png', 'pdf'); /*Filter Book File extension*/
$allowCoverFileExt = array('jpg', 'jpeg', 'png','gif'); /*Filter Cover file extension*/

$uploadId = $_POST['uploadId'];
$userLoginId = $_POST['userLoginId'];
$title = $_POST['title'];
$author = $_POST['author'];
$genres = $_POST['genres'];
$publish = $_POST['publish'];
$description = $_POST['description'];

$removeCount = isset($_POST['removeCount'])?$_POST['removeCount']:0;

/*Check empty fields*/

if (empty($title)) $titleEmpty = true;
if (empty($author)) $authorEmpty = true;
if (empty($genres)) $genresEmpty = true;
if (empty($publish)) $publishEmpty = true;

if (!empty($title) && !empty($author) && !empty($genres) && !empty($publish)) {

    if ($uploadId > 0){
        $bookUpdate = Book::get_book_by_Id($uploadId);
    }
    $coverFileNotChanged = false;
    /*Validate Book Cover*/
    $newCoverFileName = '';
    $coverName = 'cover_file';
    $coverFileName = '';

    $coverError = false;
    $coverLarger = false;
    $coverInvalid = false;
    $isCoverError = false;
    $coverFeedBack = null;

    if (isset($_FILES[$coverName]['name']) && !empty($_FILES[$coverName]['name'])) {
        $coverFileName = $_FILES[$coverName]['name'];
        $coverFileSize = $_FILES[$coverName]['size'];
        $coverFileType = $_FILES[$coverName]['type'];
        $coverTpmName = $_FILES[$coverName]['tmp_name'];
        $coverFileError = $_FILES[$coverName]['error'];
        $coverFileExt = explode('.', $coverFileName);
        $coverActualFileExt = strtolower(end($coverFileExt));

        if (in_array($coverActualFileExt, $allowCoverFileExt)) {
            if ($coverFileSize <= $maxCoverFileSize) { /*allow equal or less than 10 MB*/
                if ($coverFileError === 0) {
                    $newCoverFileName = time() . "_" . rand(1000000, 9999999) . "." . $coverActualFileExt;
                    $coverPath = "../images/" . basename($newCoverFileName);
                    move_uploaded_file($coverTpmName, $coverPath);  /*Upload file is successful.*/
                } else $coverError = true;
            } else $coverLarger = true;
        } else $coverInvalid = true;

        if ($coverInvalid || $coverLarger || $coverError){
            $isCoverError = true;
            $coverFeedBack = array('name'=> $coverFileName,'not_accept'=> $coverInvalid,'too_large'=> $coverLarger,'error'=>$coverError);

            /*If User doesn't choose any new Book Cover from Local Machine*/
            if ($uploadId > 0) {
                /*On update Status => set old book cover to the updating*/
                $newCoverFileName = $bookUpdate->book_cover;
            } else {
                /*On new Book Upload*/
                $newCoverFileName = 'no_profile.png'; /*Default cover file name*/
            }
        }

    } else {
        /*If User doesn't choose any new Book Cover from Local Machine*/
        if ($uploadId > 0) {
            /*On update Status => set old book cover to the updating*/
            $newCoverFileName = $bookUpdate->book_cover;
            $coverFileNotChanged = true;
        } else {
            /*On new Book Upload*/
            $newCoverFileName = 'no_profile.png'; /*Default cover file name*/
        }
    }
    $bookFileCount = ($coverFileNotChanged)?count($_FILES):count($_FILES)-1;
    if ($uploadId > 0) {
        $affectedRows = 0;
        /* Update book */
        if ( $bookUpdate->title != $title ||  $bookUpdate->author != $author ||  $bookUpdate->published_date != $publish || $bookUpdate->description != $description || $bookUpdate->book_cover != $newCoverFileName){
            $bookUpdate->title = $title;
            $bookUpdate->author = $author;
            $bookUpdate->genres = $genres;
            $bookUpdate->published_date = $publish;
            $bookUpdate->description = $description;
            $bookUpdate->book_cover = $newCoverFileName;
            $affectedRows = Book::update_book($bookUpdate);
        }else $affectedRows = 1;

        if ($affectedRows > 0) {
            $isBookDetailSuccess = false;
            $successNumber = 0;
            $newBookFileName = '';
            $deleteOldBook = false;
            if ($bookFileCount <= 0 && $removeCount == 0){
                if ($isCoverError){
                    $response = array('success'=>0, 'cover'=> $coverFeedBack, 'book'=> 0);
                }else{
                    $response = array('success'=>1, 'cover'=> 0, 'book'=> 0);
                }
                echo json_encode($response);
            }else{

                $bookListFeedback = []; //record error book files

                /*Remove old which were removed by User*/
                $removed = 0;
                if ($removeCount > 0){
                    for ($i = 0 ; $i < $removeCount ; $i++){
                        $fileName = 'remove'.($i+1);
                        $affected = BookDetail::deleteBookFileByName($_POST[$fileName]);
                        if ($affected > 0) $removed++;
                    }
                }if ($removeCount == $removed) $deleteOldBook = true;
                $bookFileName = '';
                $bookError = false;
                $bookLarger = false;
                $bookInvalid = false;
                for ($i = 0 ; $i < $bookFileCount; $i++){
                    $fileName = 'book_file'.($i+1);

                    $bookFileName = $_FILES[$fileName]['name'];
                    $bookFileSize = $_FILES[$fileName]['size'];
                    $bookFileType = $_FILES[$fileName]['type'];
                    $bookTpmName = $_FILES[$fileName]['tmp_name'];
                    $bookFileError = $_FILES[$fileName]['error'];
                    $bookFileExt = explode('.', $bookFileName);
                    $bookActualFileExt = strtolower(end($bookFileExt));

                    if (in_array($bookActualFileExt, $allowBookFileExt)) {
                        if ($bookFileSize <= $maxBookFileSize) {
                            if ($bookFileError === 0) {
                                $newBookFileName = time() . "_" . rand(1000000, 9999999) . "." . $bookActualFileExt;
                                $bookFilePath = "../books/" . basename($newBookFileName);
                                move_uploaded_file($bookTpmName, $bookFilePath);  /*Upload file is successful.*/
                                $isBookDetailSuccess = true;
                            } else $bookError = true;
                        } else $bookLarger = true;
                    } else $bookInvalid = true;

                    /*If file upload successfully => add into table if not ignore*/
                    if ($isBookDetailSuccess) { /*If there is not error => upload those files into Upload_Detail table*/
                        $book_detail = new BookDetail();
                        $book_detail->upload_id = $uploadId;
                        $book_detail->book_file = $newBookFileName;
                        $result_detail = BookDetail::add_book_detail($book_detail);

                        if ($result_detail) $successNumber++;
                    }else{
                        //Record and send feedback to Front End
                        $bookFile = array('name'=> $bookFileName,'not_accept'=> $bookInvalid,'too_large'=> $bookLarger,'error'=>$bookError);
                        $bookListFeedback[] = $bookFile;
                    }
                }//End Loop
                if ($successNumber == $bookFileCount && $deleteOldBook && $isCoverError == false){
                    $response = array('success'=>1, 'cover'=> 0, 'book'=> 0);
                    echo json_encode($response);
                }
                else{
                    $response = array('success'=>0, 'cover'=> $coverFeedBack, 'book'=> $bookListFeedback);
                    echo json_encode($response);
                }
            }
        }/*[End] if Book was uploaded Successfully*/
    } else {
        /*Upload new book*/
        $book = new Book();
        $book->user_id = $userLoginId;
        $book->title = $title;
        $book->author = $author;
        $book->genres = $genres;
        $book->published_date = $publish;
        $book->description = $description;
        $book->book_cover = $newCoverFileName;
        $insertedId = Book::add_book($book);

        $bookListFeedback = []; //record error book files

        if ($insertedId > 0) {
            $isBookDetailSuccess = false;
            $successNumber = 0;
            $newBookFileName = '';
            $bookFileName = '';
            $bookError = false;
            $bookLarger = false;
            $bookInvalid = false;

            for ($i = 0 ; $i < $bookFileCount; $i++){
                $fileName = 'book_file'.($i+1);

                $bookFileName = $_FILES[$fileName]['name'];
                $bookFileSize = $_FILES[$fileName]['size'];
                $bookFileType = $_FILES[$fileName]['type'];
                $bookTpmName = $_FILES[$fileName]['tmp_name'];
                $bookFileError = $_FILES[$fileName]['error'];
                $bookFileExt = explode('.', $bookFileName);
                $bookActualFileExt = strtolower(end($bookFileExt));

                if (in_array($bookActualFileExt, $allowBookFileExt)) {
                    if ($bookFileSize <= $maxBookFileSize) {
                        if ($bookFileError === 0) {
                            $newBookFileName = time() . "_" . rand(1000000, 9999999) . "." . $bookActualFileExt;
                            $bookFilePath = "../books/" . basename($newBookFileName);
                            move_uploaded_file($bookTpmName, $bookFilePath);  /*Upload file is successful.*/
                            $isBookDetailSuccess = true;
                        } else $bookError = true;
                    } else $bookLarger = true;
                } else $bookInvalid = true;

                /*If file upload successfully => add into table if not ignore*/
                    if ($isBookDetailSuccess) { /*If there is not error => upload those files into Upload_Detail table*/
                        $book_detail = new BookDetail();
                        $book_detail->upload_id = $insertedId;
                        $book_detail->book_file = $newBookFileName;
                        $result_detail = BookDetail::add_book_detail($book_detail);

                        if ($result_detail) $successNumber++;
                    }else{
                        //Record and send feedback to Front End
                        $bookFile = array('name'=> $bookFileName,'not_accept'=> $bookInvalid,'too_large'=> $bookLarger,'error'=>$bookError);
                        $bookListFeedback[] = $bookFile;
                    }
            } /*[End]... For loop*/

            if ($successNumber == $bookFileCount){
                sendNotificationToAllFollowers($userLoginId,$insertedId);
                sendEmailNotifications($userLoginId,$insertedId);
                if ($isCoverError){
                    // If cover file is error we set it as the default picture
                    $response = array('success'=>0, 'cover'=> $coverFeedBack, 'book'=> 0);
                }else{
                    $response = array('success'=>1, 'cover'=> 0, 'book'=> 0);
                }
                echo json_encode($response);
            }
            else {
                /*If all selected BOOK FILEs uploaded not successfully we delete it from Table*/
                Book::deleteFromTable($insertedId);
                $response = array('success'=>0, 'cover'=> $coverFeedBack, 'book'=> $bookListFeedback);
                echo json_encode($response);
            }
        }/*[End] if Book was uploaded Successfully*/
    }
}/*[End] if all required fields are not empty*/


function sendNotificationToAllFollowers($posterId, $uploadId){
    $followerList = Follower::getAllGeneralFollower($posterId);
    if ($followerList){
        foreach ($followerList as $item){
            UploadNotification::add($item->posterId,$item->followerId,$uploadId);
        }
    }
    $followerList = null;
}

function sendEmailNotifications($posterId,$uploadId){
    $mailFollowerList = Follower::getAllEmailFollower($posterId);

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $posterName = $_SESSION['fullname'];

    if ($mailFollowerList){
        foreach ($mailFollowerList as $follower){
            sendEmailNotification($follower,$posterName,$uploadId);
        }
    }
}


