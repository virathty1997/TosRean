<?php

/*check whether session was started .......*/
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once '../config/DbConfig.php';
require_once '../model/User.class.php';
if (isset($_POST['signin'])){
    $email = $_POST['email'];
    $password = $_POST['password'];
    if (empty($email) || empty($password)){
        $_SESSION['email'] = $email;
        $_SESSION['password'] = $password;
        header('location:../pages/sign_in.php');
        exit();
    }else{
        if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
            $_SESSION['email'] = $email.'|invalid';
            $_SESSION['password'] = $password;
        }else{
            $result = User::isSuccess($email,$password);
            if ($result > 0){
                $user = User::getUserById($result);
                $_SESSION['userLoginId'] = $user->id;
                $_SESSION['profile'] = $user->profile;
                $_SESSION['fullname'] = $user->fullName;
                $_SESSION['email'] = $user->email;

                /*Clear all session info*/
                unset($_SESSION['password']);
                unset($_SESSION['sign_in']);

                $dt = new DateTime();
                $lastLoginDate= $dt->format('Y-m-d H:i:s');

                //SAVE to COOKIES for RE-SIGN IN
                $cookieName = "email";
                $cookieValue = $user->email;
                setcookie($cookieName, $cookieValue,  time() + (86400 * 30), "/");
                setcookie("last_login", $lastLoginDate,  time() + (86400 * 30), "/");

                /*Update Last Login in Users table*/
                User::updateLastLogin($user->id,$lastLoginDate);

                /*reload index page again*/
                header('location:../pages/index.php');
            }else{
                /*Sign in error*/
                $_SESSION['email'] = $email;
                $_SESSION['password'] = $password;
                $_SESSION['sign_in'] = 'error';
                header('location:../pages/sign_in.php');
                exit();
            }
        }
    }
}else{
    header('location:../pages/sign_in.php');
}
?>