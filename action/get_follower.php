<?php
require_once "../config/DbConfig.php";
require_once "../model/Follower.class.php";

if (isset($_POST['userLoginId']) && isset($_POST['posterId'])){

    if ($_POST['userLoginId'] != 0 && $_POST['posterId'] != 0){

        $follower = Follower::getFollower($_POST['posterId'],$_POST['userLoginId']);
        if ($follower){
            $array = array($follower->notify, $follower->status);
            echo json_encode($array,JSON_FORCE_OBJECT);
            //echo $follower->notify . ' ; ' . $follower->status;
        }else {
            $generate = array(0,0);
            //echo 'not found';
            echo json_encode($generate,JSON_FORCE_OBJECT);
        }
    }
}