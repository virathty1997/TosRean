<?php
    require_once "../config/DbConfig.php";
    require_once "../model/Reaction.class.php";

    /*echo "<br>User ID =>".$_POST['userId'];
    echo "<br>Comment ID =>".$_POST['commentId'];
    echo "<br>Reaction Type =>".$_POST['reactType'];*/

    $react = new Reaction();
    $react->userId = $_POST['userId'];
    $react->commentId = $_POST['commentId'];
    $react->reactType = $_POST['reactType'];

    $exitId = Reaction::isExist($react);
    if ($exitId > 0){
        Reaction::deleteById($exitId);
    }else{
        Reaction::delete($react);
        Reaction::add($react);
    }

    $like = Reaction::countReaction($react->commentId,0);
    $dislike = Reaction::countReaction($react->commentId,1);
    echo ($like ."|". $dislike);
?>