<?php
require_once "../config/DbConfig.php";
require_once "../model/Comment.class.php";

    if (!empty($_POST['commentText'])){
        $com = new Comment();
        $com->id = $_POST['commentId'];
        $com->userId = $_POST['userId'];
        $com->uploadId = $_POST['uploadId'];
        $com->commentText = $_POST['commentText'];

        echo Comment::update($com);
    }else{
        echo 0;
    }
?>