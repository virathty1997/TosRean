<?php
require_once "../config/DbConfig.php";
require_once "../model/Book.php";

if (isset($_POST['uploadId'])){
    $id= $_POST['uploadId'];
    echo ($id > 0)? Book::delete($id): 0;
}