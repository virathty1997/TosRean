<?php
include_once "../config/DbConfig.php";
include_once "../model/BookDetail.php";
include_once "../model/Book.php";

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    /*Get User Logged in ID*/
    $userLoginId = isset($_SESSION['userLoginId'])?$_SESSION['userLoginId']:0;

    if ($userLoginId <=0 ){
        /*User hasn't logged in yet OR we can say there is not user Id equal 0 in Database table*/
        header('location:../pages/upload.php');
        exit();
    }

    if (isset($_POST['upload'])) {

        $book_cover = $_FILES['cover_file']['name'];
        $cover_path = "../images/" . basename($book_cover);
        $title = $_POST['title'];
        $author = $_POST['author'];
        $genres = $_POST['genres'];
        $publish = $_POST['publish'];
        $description = $_POST['description'];

        $book = new Book();
        $book->user_id = $userLoginId;
        $book->title = $title;
        $book->author = $author;
        $book->genres = $genres;
        $book->published_date = $publish;
        $book->description = $description;
        $book->book_cover = $book_cover;
        move_uploaded_file($_FILES['cover_file']['tmp_name'], $cover_path);
        $result = Book::add_book($book);


        if ($result > 0) {

            $book_file = $_FILES['book_file']['name'];
            $file_path = "../images/" . basename($book_file);
            $book_detail = new BookDetail();
            $book_detail->upload_id = $result;
            $book_detail->book_file = $book_file;
            $result_detail = BookDetail::add_book_detail($book_detail);
            move_uploaded_file($_FILES['book_file']['tmp_name'], $file_path);

            if ($result_detail) {
                $message = "Inserted successfully 😊";
                echo "<script type='text/javascript'>alert('$message');</script>";
                header('location:../pages/upload.php');
            } else {
                echo 'not inserted';
            }
        } else {
            header('location:../pages/upload.php');
        }

    } else {
        echo 'not sumit';
    //    helo
    }


    function validateUPloadFile()
    {
        //upload image
        $fileName = $_FILES['photo']['name'];
        $fileSize = $_FILES['photo']['size'];
        $fileType = $_FILES['photo']['type'];
        $fileTpmName = $_FILES['photo']['tmp_name'];
        $fileError = $_FILES['photo']['error'];
        $fileExt = explode('.', $fileName);
        $fileActualExt = strtolower(end($fileExt));

        $newFileName = '';

        $allowExt = array('jpg', 'jpeg', 'png');
        if (in_array($fileActualExt, $allowExt)) {
            if ($fileSize <= 10000000) { /*allow equal or less than 10 MB*/
                if ($fileError === 0) {

                    /*$newFileName = time() . "_" . rand(1000000,9999999) . "." . $fileActualExt;
                    $path = "../images/".basename($new_file_name);*/

                    $newFileName = uniqid('', true) . "." . $fileActualExt;
                    $imagePath = "../images/" . basename($newFileName);
                    move_uploaded_file($fileTpmName, $imagePath);
                    /*Upload file is successful.*/
                } else {
                    echo "There was error while upload this file.";
                }
            } else {
                echo "This file is too big. You can upload file equal or less than 10 MB.";
            }
        } else {
            echo "You can't upload this file";
        }
        return $newFileName;
    }