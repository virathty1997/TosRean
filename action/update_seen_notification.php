<?php
require_once "../config/DbConfig.php";
require_once "../model/Reaction.class.php";
require_once "../model/Comment.class.php";
require_once "../model/UploadNotification.class.php";

    if (isset($_POST['type']) && isset($_POST['id'])){
        $type = $_POST['type'];
        $id = $_POST['id'];
        if ($type == 'reaction'){
             echo Reaction::updateSeenById($id);
        }else if ($type == 'comment'){
            echo Comment::updateSeenById($id);
        }else if ( $type == 'upload'){
            echo UploadNotification::updateUploadSendById($id);
        }
    }


