<?php
require_once "../config/DbConfig.php";
require_once "../model/Rate.class.php";
if (isset($_POST['uploadId'])){

    if ($_POST['uploadId'] != 0 ){
        $rates = Rate::getRates($_POST['uploadId']);
        echo json_encode($rates,JSON_FORCE_OBJECT);
    }
}
?>