<?php
require_once "../config/DbConfig.php";
require_once "../model/Notification.class.php";

if (isset($_POST['userLoginId'])){
    $userLoginId = $_POST['userLoginId'];
    if ($userLoginId > 0){

        $filterDate = isset($_POST['filterDate'])?$_POST['filterDate']:null; /*NULL means that user doesn't filter any date of notification*/
        $limitRows = isset($_POST['limitRows'])?$_POST['limitRows']:null;

        $notifications = Notification::getAllNotifications($userLoginId,$limitRows,$filterDate);
        if (count($notifications) == 0){
            echo '<div style="height: 100%; width: 100%; display: flex; justify-content: center; align-items: center;">
                    <div>
                       <center style="margin-bottom: 10px;"><i style="color: red; font-size: 30px;" class="p3 fa fa-bell fa-stack-1x fa-inverse"></i></center>
                       <br>
                       <center><p style="font-weight: 600; margin-top: 5px; color: #a5a5a5;">There is no notification !</p></center>                                          
                    </div>
                  </div>';
        }else{
            $orderNumItem = -1; /*Represent of item index on Notification dialog Note: all the Date info also count on INDEX*/

            for($index = 0 ; $index < count($notifications); $index++){
                if ($index == 0){
                    /*The current date is always behind one day => We have to add one date more like statement below*/
                    //$currentDate = date('Y-m-d', time() + 86400);
                    $currentDate = date('Y-m-d');

                    $date = datetimeToDate($notifications[$index]->datetime);

                    $number = countNotificationByDate($notifications,$date,$index);
                    if ($currentDate == $date){
                        echo groupByDate($date,'Today  ('.$number.')');
                    }
                    else{
                        echo groupByDate($date,$date .'  ('.$number.')');
                    }
                    $orderNumItem++;
                }else{
                    $date1 = datetimeToDate($notifications[$index-1]->datetime);
                    $date2 = datetimeToDate($notifications[$index]->datetime);

                    if ($date1 != $date2){
                        $orderNumItem++;
                        $number = countNotificationByDate($notifications,$date2,$index);
                        echo groupByDate($date2,$date2 .'  ('.$number.')');
                    }
                }
                $orderNumItem++;
                if ($notifications[$index] instanceof ReactionNotification){
                    echo renderReactNotification($notifications[$index],$orderNumItem);
                }else if ($notifications[$index] instanceof CommentNotification){
                    echo renderCommentNotification($notifications[$index],$orderNumItem);
                }else if ($notifications[$index] instanceof  UploadNotification){
                    echo renderUploadNotification($notifications[$index],$orderNumItem);
                }
            }
        }
    }
}

function countNotificationByDate($list, $date, $currentIndex){
    $num = 0;
    if (count($list) > 0){
        for($ind = $currentIndex ; $ind < count($list); $ind++){
            if (datetimeToDate($list[$ind]->datetime) == $date){
                $num++;
            }
        }
    }
    return $num;
}

function datetimeToDate($datetime){
    return date_format(new DateTime($datetime), "Y-m-d");
}

function groupByDate($date, $infoText){
    return '<a href="view_notifications.php?date='.$date.'" title="Click to filter" style="font-size: 13px; padding: 0px 5px; font-weight:700; " class="mark-label">' .$infoText.'</a>';
}

function renderReactNotification($react,$index){
    $element = '<a href="book_detail.php?book='.$react->uploadId.'&container='.$react->commentId.'#container'.$react->commentId.'" class="items border dropdown-item" role="presentation">'.
                    '<p hidden id="notification'.$index.'">reaction|'.$react->id.'</p>'.
                    '<div id="notification'.$index.'" class="notif-image">'.
                        '<img src="'.$react->profile.'" width="45px" height="45px" style="border-radius: 50px;" alt="">'.
                    '</div>'.
                    '<div class="notif-description">'.
                        '<div class="notifi-text">'.
                            '<p>'. $react->fullName .''. (($react->react == 0)?' likes':' dislikes') .' your comment on a book.</p>'.
                        '</div>'.
                        '<div class="notifi-type">'.
                            '<i '. (($react->react == 0)?'':' hidden') .' style="color: rgb(54,161,214); margin-bottom: 5px" class="fas fa-thumbs-up"></i>'.
                            '<i '. (($react->react == 1)?'':' hidden') . ' style="color: rgba(65,133,216,0.83); " class="fas fa-thumbs-down"></i>' .
                            '<p style="font-size: 11px;" class="issue-on">On '.$react->datetime.'</p>'.
                            '<i '. (($react->seen == 0)?' hidden':'') . '  style="color: rgba(32,139,33,0.83); align-self: flex-end; margin-bottom: 4px; margin-left: 10px;" class="fas fa-check"></i>' .
                        '</div>'.
                    '</div>'.
                '</a>';
    return $element;
}

function renderCommentNotification($comment,$index){
    $element = '<a href="book_detail.php?book='.$comment->uploadId.'&container='.$comment->commentId.'#container'.$comment->commentId.'" class="items border dropdown-item" role="presentation">'.
                    '<p hidden id="notification'.$index.'">comment|'.$comment->commentId.'</p>'.
                    '<div class="notif-image">'.
                        '<img src="'.$comment->profile.'" width="45px" height="45px" style="border-radius: 50px;" alt="">'.
                    '</div>'.
                    '<div class="notif-description">'.
                        '<div class="notifi-text">'.
                            '<p>'. $comment->fullName .' comments on your book</p>'.
                        '</div>'.
                        '<div class="notifi-type">'.
                            '<i style="color: rgb(49,188,48);" class="fas fa-comment"></i>' .
                            '<p style="font-size: 11px;" class="issue-on">On '.$comment->datetime.'</p>'.
                            '<i '. (($comment->seen == 0)?' hidden':'') .'  style="color: rgba(32,139,33,0.83); align-self: flex-end; margin-bottom: 4px; margin-left: 10px;" class="fas fa-check"></i>' .
                            '</div>'.
                    '</div>'.
                '</a>';
    return $element;
}


function renderUploadNotification($uploadNotification, $index){
    $element = '<a href="book_detail.php?book='.$uploadNotification->uploadId.'" class="items border dropdown-item" role="presentation">'.
        '<p hidden id="notification'.$index.'">upload|'.$uploadNotification->id.'</p>'.
        '<div class="notif-image">'.
        '<img src="'.$uploadNotification->profile.'" width="45px" height="45px" style="border-radius: 50px;" alt="">'.
        '</div>'.
        '<div class="notif-description">'.
        '<div class="notifi-text">'.
        '<p>'. $uploadNotification->fullName .' uploaded a new book</p>'.
        '</div>'.
        '<div class="notifi-type">'.
        '<i style="color: rgb(49,188,48);" class="fas fa-book"></i>' .
        '<p style="font-size: 11px;" class="issue-on">On '.$uploadNotification->datetime.'</p>'.
        '<i '. (($uploadNotification->seen == 0)?' hidden':'') .'  style="color: rgba(32,139,33,0.83); align-self: flex-end; margin-bottom: 4px; margin-left: 10px;" class="fas fa-check"></i>' .
        '</div>'.
        '</div>'.
        '</a>';
    return $element;
}