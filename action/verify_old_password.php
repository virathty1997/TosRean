<?php
    require_once '../config/DbConfig.php';
    require_once '../model/User.class.php';

    if (isset($_POST['userId']) && isset($_POST['oldPassword'])){
        $userId = $_POST['userId'];
        $oldPassword = $_POST['oldPassword'];

        if (!is_numeric($userId) || empty($oldPassword)){
            echo 0;
        }else{
            echo (User::verifyOldPassword($userId,$oldPassword))?1:0;
        }
    }else{
        echo 0;
    }