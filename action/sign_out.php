<?php

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

$cookieName = "email";
$cookieValue = $_SESSION['email'];
setcookie($cookieName, $cookieValue,  time() - (86400 * 30), "/");
setcookie("last_login", date('Y-m-d'),  time() - (86400 * 30), "/");

unset($_SESSION['userLoginId']);
unset($_SESSION['profile']);
unset($_SESSION['fullname']);

header('location:../pages/index.php');
?>

