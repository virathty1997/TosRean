<?php
    require_once "../config/DbConfig.php";
    require_once "../model/Comment.class.php";

    if (isset($_POST['commentId'])){
        $id= $_POST['commentId'];
        echo ($id > 0)? Comment::delete($id): 0;
    }
?>