<?php
    require_once "../config/DbConfig.php";
    require_once "../model/Rate.class.php";
    if (isset($_POST['userId']) && isset($_POST['uploadId']) && isset($_POST['rateType'])){

        if ($_POST['userId'] !=0 && $_POST['uploadId'] != 0 && $_POST['rateType'] !=0){
            $rate = new Rate();
            $rate->userId = $_POST['userId'];
            $rate->uploadId = $_POST['uploadId'];
            $rate->rateType = $_POST['rateType'];

            $existId = Rate::isExist($rate);
            $modifyId = 0;
            if ($existId > 0){
               $modifyId = Rate::update($rate);
            }else{
                $modifyId = Rate::add($rate);
            }
            echo $modifyId .'|'.Rate::getRateCount($rate->uploadId);
        }
    }
?>