<?php
    require_once "../config/DbConfig.php";
    require_once "../model/Comment.class.php";

    if (!empty($_POST['commentText'])){
        $com = new Comment();
        $com->id = 0;
        $com->userId = $_POST['userId'];
        $com->uploadId = $_POST['uploadId'];
        $com->commentText = $_POST['commentText'];

        $insertedId = Comment::add($com);
        if ($insertedId > 0){
            $comment = Comment::getById($insertedId);
            if ($comment){
                $result = array($comment->id,$comment->createdDate,$comment->userId,$comment->fullName,$comment->profile,$comment->uploadId,$comment->commentText,$comment->likeNumber,$comment->dislikeNumber,$comment->isEdited);
                echo json_encode($result);
            }
        }
    }
?>