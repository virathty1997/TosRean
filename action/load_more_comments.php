    <?php
    require_once "../config/DbConfig.php";
    require_once "../model/Comment.class.php";

    $uploadId = $_POST['uploadId'];
    $userLoginId = $_POST['userLoginId'];
    $newCommentCount = $_POST['newCommentPosition'];

    $comments = Comment::getLimitedRows($uploadId,$newCommentCount);
    foreach ($comments as $com){
        /*id = 0;  createdDate = 1; userId = 2; fullName = 3 ; profile = 4 ;
        uploadId = 5 ;commentText = 6 ; likeNumber = 7; dislikeNumber = 8; isEdited = 9*/

        $value1 =
            '<div id="container'.$com->id.'" class="message-container">' .
                    '<div class="profile">' .
                        '<div class="follow-dropdown">'.
                            '<img src="' . $com->profile . '" alt="" onmouseover="doProfileHover('. $com->id.','.$com->userId .')">'.
                            '<div '. (($com->userId == $userLoginId || $userLoginId == 0)?'hidden':'') .' class="follow-dropdown-content">'.
                                '<div class="profile-user">' .
                                    '<img style="width: 35px; height: 35px; border-radius: 50px;" src="' . $com->profile . '" alt="">' .
                                    '<p>' . $com->fullName . '</p>' .
                                '</div>' .
                                '<div class="all-send">' .
                                    '<div class="custom-control custom-checkbox">'.
                                        '<input  onchange="doChanged('.$com->id.')" type="checkbox" class="custom-control-input" id="checkbox_notify' . $com->id . '">' .
                                        '<label style="user-select: none;" class="custom-control-label" for="checkbox_notify' . $com->id . '">Notify to your email</label>' .
                                    '</div>' .
                                '</div>' .
                                '<div class="follow-button">' .
                                    '<button onclick="doFollow('.$com->id.')" id="button_follow'. $com->id .'" class="btn btn-success">Follow</button>' .
                                '</div>' .
                            '</div>' .
                        '</div>' .
                    '</div>' .
                    '<div class="comment-description" style="width: 100%;">' .
                        '<div class="comment-show">' .
                            '<div style="width: 100%;" class="name">' .
                                '<label>'.$com->fullName.'</label>' .
                                '<label id="created_date'.$com->id.'" style="float:right;">'.(($com->isEdited ==1)?'Edited  ':'') . $com->createdDate .'</label>' .
                            '</div>' .

                            '<div style="display: flex">' .
                                '<div' . (($com->rateType >= 1)?' class="com-star"':' class="white-star"') . '>' .
                                    '<i class="fa fa-star fa-fw"></i>' .
                                '</div>' .
                                '<div'. (($com->rateType >= 2)?' class="com-star"':' class="white-star"').'>' .
                                    '<i class="fa fa-star fa-fw"></i>'.
                                '</div>' .
                                '<div' .(($com->rateType >= 3)?' class="com-star"':' class="white-star"').'>' .
                                    '<i class="fa fa-star fa-fw"></i>' .
                                '</div>' .
                                '<div' .(($com->rateType >= 4)?' class="com-star"':' class="white-star"').'>' .
                                    '<i class="fa fa-star fa-fw"></i>' .
                                '</div>' .
                                '<div'. (($com->rateType >= 5)?' class="com-star"':' class="white-star"').'>' .
                                    '<i class="fa fa-star fa-fw"></i>' .
                                '</div>' .
                            '</div>' .

                            '<div class="comment-text">' .
                                '<label id="comment_text'. $com->id .'" class="mark-label">'.$com->commentText.'</label>' .
                            '</div>' .
                            '</div>' .
                            '<div class="operation">' .
                                    '<div class="like operation">' .
                                        '<div class="react">' .
                                            '<label class="mark-label" id="like'.$com->id.'">'. $com->likeNumber .''. (($com->likeNumber > 1)?" likes": " like") .'</label>' .
                                        '</div>' .
                                        '<div style="margin-left: 5px;" class="react">' .
                                            '<i style="color: rgba(45,45,45,0.83);" id="like_icon'. $com->id .'" class="fas fa-thumbs-up" onclick="doLike('.$com->id.')" onmouseover="doLikeHover('. $com->id .')" ></i>' .
                                        '</div>' .
                                    '</div>' .
                                    '<div class="dislike operation">' .
                                        '<div class="react">' .
                                            '<label class="mark-label" id="dislike'.$com->id.'">'. $com->dislikeNumber .''. (($com->dislikeNumber > 1)?" dislikes": " dislike") .'</label>' .
                                        '</div>' .
                                        '<div style="margin-left: 5px;" class="react">' .
                                            '<i style="color: rgba(45,45,45,0.83); margin-top: 4px;" id="dislike_icon'. $com->id .'" class="fas fa-thumbs-down" onclick="doDislike('.$com->id.')" onmouseover="doDislikeHover('. $com->id .')"></i>' .
                                        '</div>' .
                                    '</div>';

                $value2 = "";
                if ($com->userId == $userLoginId){
                    $value2 =
                        '<div class="edit-comment operation">'.
                            '<label class ="mark-label reaction" onclick="doEdit('. $com->id .')">edit</label>' .
                        '</div>' .
                        '<div class="delete-comment operation">' .
                            '<label class="mark-label reaction" onclick="doDelete('. $com->id .')">delete</label>' .
                        '</div>';
                }
            $value3 =
            '</div>' .
            '</div>' .
            '</div>';

            echo ($value1 . $value2 . $value3);
    }
?>