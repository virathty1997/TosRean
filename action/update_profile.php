<?php
    require_once '../config/DbConfig.php';
    require_once '../model/User.class.php';

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if (isset($_POST['update']) && isset($_SESSION['userLoginId'])){
        $userId = $_SESSION['userLoginId'];

        $fullName = $_POST['fullname'];
        $gender = $_POST['gender'];
        $dob = $_POST['dob'];
        $email = $_POST['email'];
        $oldPassword =  $_POST['old_password'];
        $newPassword = $_POST['new_password'];
        $retypePassword = $_POST['retype_password'];
        $errorUploadFile = '';
        if (empty($fullName) || empty($gender) || empty($dob) || empty($email)){
            redirectToProfilePage($fullName,$gender,$dob,$email,$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'error');
        }else{
            $user = User::getUserById($userId);

            if (strlen($fullName) > 50){
                redirectToProfilePage($fullName.'|invalid',$gender,$dob,$email,$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'error');
                exit();
            }else if ($gender != 'male' && $gender != 'female'){
                redirectToProfilePage($fullName,$gender.'|invalid',$dob,$email,$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'error');
                exit();
            }else if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                redirectToProfilePage($fullName,$gender,$dob,$email.'|invalid',$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'error');
                exit();
            }else if($email != $user->email && User::isExistEmail($email) > 0){
                redirectToProfilePage($fullName,$gender,$dob,$email.'|exist',$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'error');
                exit();
            }else if (password_verify($oldPassword,$user->password) && !empty($oldPassword)){

                if (!empty($newPassword)){
                    if (strlen($newPassword) < 8 || strlen($newPassword) > 50){
                        redirectToProfilePage($fullName,$gender,$dob,$email,$oldPassword,$newPassword.'|invalid',$retypePassword,$errorUploadFile,'error');
                        exit();
                    }else if ((!empty($newPassword) && empty($retypePassword)) /*|| (!empty($newPassword) && empty($retypePassword))*/){
                        redirectToProfilePage($fullName,$gender,$dob,$email,$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'error');
                        exit();
                    }else if ($newPassword !== $retypePassword){
                        redirectToProfilePage($fullName,$gender,$dob,$email,$oldPassword,$newPassword,$retypePassword.'|invalid',$errorUploadFile,'error');
                        exit();
                    }else{
                        goto update;
                    }
                }else goto update;
            }else{
                update:
                $errorUpload = false;
                $newFileName = '';
                $imagePath = '';
                if (!isset($_FILES['photo']['name'])){
                    $newFileName = $user->profile;
                }else{
                    $fileName = $_FILES['photo']['name'];
                    $fileSize = $_FILES['photo']['size'];
                    $fileType = $_FILES['photo']['type'];
                    $fileTpmName = $_FILES['photo']['tmp_name'];
                    $fileError = $_FILES['photo']['error'];
                    $fileExt = explode('.', $fileName);
                    $fileActualExt = strtolower(end($fileExt));

                    $allowExt = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array($fileActualExt, $allowExt)) {
                        if ($fileSize <= 10485760) { /*allow equal or less than 1 MB*/
                            if ($fileError === 0) {

                                $newFileName = time() . "_" . rand(1000000,9999999) . "." . $fileActualExt;
                                $imagePath = "../images/".basename($newFileName);

                                move_uploaded_file($fileTpmName, $imagePath);
                                /*Upload file is successful.*/
                            } else {
                                $errorUpload = true;
                                $errorUploadFile = 'error';
                            }
                        } else {
                            $errorUpload = true;
                            $errorUploadFile = 'larger';
                        }
                    } else {
                        $errorUpload = true;
                        $errorUploadFile = 'invalid';
                    }
                    if ($errorUpload){
                        /*If uploading file is error we get the old to save into table*/
                        $newFileName = $user->profile;
                    }
                }

                $hashPassword = '';
                /*Check if password is changed*/
                if (empty($newPassword)){
                    $hashPassword = $user->password;
                }else{
                    $hashPassword =  password_hash($newPassword,PASSWORD_DEFAULT);
                }

                $user->fullName = $fullName;
                $user->gender = $gender;
                $user->dob = $dob;
                $user->email = $email;
                $user->password = $hashPassword;
                $user->profile = $imagePath;

                if (User::update($user) > 0){

                    $_SESSION['userLoginId'] = $user->id;
                    $_SESSION['profile'] = $user->profile;
                    $_SESSION['fullname'] = $user->fullName;

                    redirectToProfilePage($fullName,$gender,$dob,$email,$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'success');
                }else{
                    redirectToProfilePage($fullName,$gender,$dob,$email,$oldPassword,$newPassword,$retypePassword,$errorUploadFile,'error');
                }
            }
        }
    }else{
        header("location:../pages/profile.php");
    }

    function redirectToProfilePage($fullName, $gender, $dob, $email, $oldPassword, $newPassword, $retypePassword, $uploadFile = '', $update = ''){

        $_SESSION['full_name'] = $fullName;
        $_SESSION['gender'] = $gender;
        $_SESSION['dob'] = $dob;
        $_SESSION['email'] = $email;
        $_SESSION['old_password'] = $oldPassword;
        $_SESSION['new_password'] = $newPassword;
        $_SESSION['retype_password'] = $retypePassword;
        $_SESSION['upload_file'] = $uploadFile;
        $_SESSION['update'] = $update;

        header("location:../pages/profile.php");
    }