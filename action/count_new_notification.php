<?php
require_once "../config/DbConfig.php";
require_once "../model/Notification.class.php";

if (isset($_POST['userLoginId'])){
    $userLoginId = $_POST['userLoginId'];
    echo Notification::countNotifications($userLoginId);
}