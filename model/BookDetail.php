<?php
include_once '../config/DbConfig.php';

class BookDetail extends DbConfig
{
    var $id;
    var $upload_id;
    var $book_file;
    var $status;

    function __construct()
    {
        $this->id = 0;
        $this->upload_id = 0;
        $this->book_file = "unknown";
        $this->status = 1;
    }

    static function get_book_all()
    {
        $books = [];
        $result = DbConfig::query("select * from upload_detail");
        if ($result) {
            while ($row = mysqli_fetch_assoc($result)) {
                $book = new BookDetail();
                $book->id = $row['id'];
                $book->upload_id = $row['upload_id'];
                $book->book_file = $row['book_file'];
                $book->status = $row['status'];
                $books[] = $book;
            }
            return $books;
        }
        return false;
    }


    static function add_book_detail($p)
    {
        $result = DbConfig::query("insert into upload_details(upload_id, book_file, status) values($p->upload_id,'$p->book_file',1);");
        return ($result)?1:0;
    }


    static function delete_book_details($uploadId)
    {
        DbConfig::query("UPDATE upload_details SET status = 0 WHERE upload_id = $uploadId",$not_used,$affectedRows);
        return $affectedRows;
    }

    static function deleteBookFileByName($fileName)
    {
        DbConfig::query("UPDATE upload_details SET status = 0 WHERE book_file = '$fileName' ",$not_used,$affectedRows);
        return $affectedRows;
    }

    static function getBookDetailById($uploadId){
        /*Add by Thoura Lai on 24-June-2018 => to get only book files for rendering on Book Detail page*/
        $bookDetailList = [];
        $result = DbConfig::query("select * from upload_details WHERE upload_id = $uploadId AND status = 1;");
        if ($result) {
            while ($row = mysqli_fetch_assoc($result)) {
                $bookDetail = new BookDetail();
                $bookDetail->id = $row['id'];
                $bookDetail->upload_id = $row['upload_id'];
                $bookDetail->book_file = $row['book_file'];
                $bookDetailList[] = $bookDetail;
            }
            return $bookDetailList;
        }
        return false;
    }


}

?>