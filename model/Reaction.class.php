<?php
    class Reaction{
        var $userId;
        var $commentId;
        var $reactType;   /*0 => like ; 1 => dislike*/

        function __construct($userId = 0, $commentId = 0, $reactType = 0)
        {
            $this->userId = $userId;
            $this->commentId = $commentId;
            $this->reactType = $reactType;
        }

        static function add($react){
            $result = DbConfig::query("INSERT INTO reaction(user_id, comment_id,type) values($react->userId, $react->commentId, $react->reactType);",$inserted_id);
            return ($result)? $inserted_id:0;
        }

        static  function update($react){
            $result = DbConfig::query("UPDATE reaction SET type = $react->reactType WHERE user_id = $react->userId AND comment_id = $react->commentId;",$not_used,$affected_rows);
            return ($result)?$affected_rows:0;
        }

        static  function updateSeenById($id){
            $result = DbConfig::query("UPDATE reaction SET seen = 1 WHERE id = $id",$not_used,$affected_rows);
            return ($result)?$affected_rows:0;
        }

        static  function delete($react){
            $result = DbConfig::query("DELETE FROM reaction WHERE user_id = $react->userId AND comment_id = $react->commentId;");
            return $result;
        }

        static  function deleteById($id){
            $result = DbConfig::query("DELETE FROM reaction WHERE id = $id;");
            return $result;
        }

        static function countReaction($commentId, $reactType){
            $reactCount = 0;
            $result = DbConfig::query("SELECT count(id) as number FROM reaction WHERE type = $reactType AND comment_id = $commentId");
            if ($result){
                $reactCount = mysqli_fetch_assoc($result)['number'];
            }
            return $reactCount;
        }

        static function isExist($react){
            $id = 0;
            $result = DbConfig::query("SELECT id FROM reaction WHERE type = $react->reactType AND comment_id = $react->commentId AND user_id = $react->userId;");
            if ($result){
                $id = mysqli_fetch_assoc($result)['id'];
            }
            return $id;
        }

       /* static function countRows(){
            $reactCount = 0;
            $result = DbConfig::query("SELECT count(id) as number FROM reaction WHERE type = $reactType AND comment_id = $commentId");
            if ($result){
                $reactCount = mysqli_fetch_assoc($result)['number'];
            }
            return $reactCount;
        }*/

        static  function changeAllReactionToSeenByUserId($userLoginId){
            $result = DbConfig::query("UPDATE comments AS C INNER JOIN reaction AS R ON C.id = R.comment_id SET R.seen = 1 WHERE C.user_id = $userLoginId AND R.user_id != $userLoginId AND R.seen = 0",$not_used,$affected_rows);
            return ($result)? $affected_rows:0;
        }

    }
?>