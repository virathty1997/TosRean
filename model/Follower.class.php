<?php

class Follower{
    var $id;
    var $followDate;
    var $posterId;
    var $followerId;
    var $notify;
    var $status;

    var $fullName;
    var $email;

    function __construct()
    {
        $dt = new DateTime();
        $currentStamp = $dt->format('Y-m-d H:i:s');

        $this->id = 0;
        $this->followDate = $currentStamp;
        $this->posterId = 0;
        $this->followerId = 0;
        $this->notify = 0;
        $this->status = 0;
    }

    static function add($follow){
        $result = DbConfig::query("INSERT INTO follower(date, poster_id, follower_id,notify,status) values(NOW(), $follow->posterId,$follow->followerId,$follow->notify,1);",$inserted_id);
        return ($result)? $inserted_id:0;
    }

    static  function update($follow){
        $result = DbConfig::query("UPDATE follower SET status = $follow->status ,notify = $follow->notify
                                                        WHERE poster_id = $follow->posterId AND follower_id = $follow->followerId;",$not_used,$affectedRows);
        return ($result)?$affectedRows:0;
    }

    static function getFollower($posterId,$followerId){
        $result = DbConfig::query("SELECT * FROM follower WHERE poster_id = $posterId AND follower_id = $followerId;");
        if ($result && mysqli_num_rows($result) > 0){
            $row = mysqli_fetch_assoc($result);
            $follow = new Follower();
            $follow->posterId =  $row['poster_id'];
            $follow->followerId =  $row['follower_id'];
            $follow->notify =  $row['notify'];
            $follow->status =  $row['status'];

            return $follow;
        }return false;
    }

    static function isExist($posterId,$followerId){
        $result = DbConfig::query("SELECT id FROM follower WHERE poster_id = $posterId AND follower_id = $followerId;");
        return ($result)? mysqli_num_rows($result):0;
    }

    static function getAllGeneralFollower($posterId){
        $followerList = [];
        $result = DbConfig::query("SELECT * FROM follower WHERE poster_id = $posterId AND status = 1;");
        if ($result && mysqli_num_rows($result) > 0){
            while ($row = mysqli_fetch_assoc($result)){
                $follow = new Follower();
                $follow->posterId =  $row['poster_id'];
                $follow->followerId =  $row['follower_id'];
                $follow->notify =  $row['notify'];
                $follow->status =  $row['status'];
                $followerList[] = $follow;
            }
            return $followerList;
        }return false;
    }

    static function getAllEmailFollower($posterId){
        $followerList = [];
        $result = DbConfig::query("SELECT F.follower_id, U.fullname, U.email FROM follower AS F INNER JOIN users AS U ON F.follower_id = U.id  WHERE poster_id = $posterId AND F.notify = 1 AND F.status = 1 AND U.status = 1;");
        if ($result && mysqli_num_rows($result) > 0){
            while ($row = mysqli_fetch_assoc($result)){
                $follow = new Follower();
                $follow->followerId =  $row['follower_id'];
                $follow->fullName = $row['fullname'];
                $follow->email = $row['email'];
                $followerList[] = $follow;
            }
            return $followerList;
        }return false;
    }

}