<?php

class User
{
    var $id;
    var $createdDate;
    var $userType;
    var $fullName;
    var $gender;
    var $dob;
    var $email;
    var $password;
    var $profile;
    var $codeMail;
    var $last_login;
    var $google_code;

    function __construct()
    {
        $this->id = 0;
        $this->createdDate = date('Y-m-d H:i:s');
        $this->userType = 'owner';
        $this->fullName = 'unknown';
        $this->gender = 'unknown';
        $this->dob = date('Y-m-d H:i:s');
        $this->email = 'unknown@gmail.com';
        $this->password = '';
        $this->profile = '';
        $this->google_code = '';
        $this->last_login = date('Y-m-d H:i:s');
        $this->codeMail = '';
    }

    static function add($user)
    {
        $result = DbConfig::query("INSERT INTO users(created_date, type, fullname, gender, birth_date, email, password, profile, status, google_code,last_login) values(NOW(),'$user->userType', '$user->fullName', '$user->gender', '$user->dob', '$user->email', '$user->password', '$user->profile', 1,'$user->google_code', '$user->last_login');", $inserted_id);
        return ($result) ? $inserted_id : 0;
    }

    static function update($user)
    {
        DbConfig::query("UPDATE users SET created_date = NOW(), fullname = '$user->fullName', gender = '$user->gender', birth_date = '$user->dob',email = '$user->email', password = '$user->password', profile = '$user->profile' WHERE id = $user->id;", $not_used, $affected_rows);
        return $affected_rows;
    }

    static function delete($id)
    {
        DbConfig::query("UPDATE users SET status = 0 WHERE id = $id;", $not_used, $affected_rows);
        return $affected_rows;
    }

    static function isExistEmail($email)
    {
        $result = DbConfig::query("SELECT id FROM users WHERE email = '$email' AND status = 1 AND type = 'owner';");
        return ($result) ? mysqli_num_rows($result) : 0;
    }

    static function verifyOldPassword($userId, $oldPassword)
    {
        $result = DbConfig::query("SELECT password FROM users WHERE id = '$userId' AND status = 1;");
        if ($result && mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            if (password_verify($oldPassword, $row['password']))
                return true;
        }
        return false;
    }

    static function isSuccess($email, $password)
    {
        $result = DbConfig::query("SELECT id,fullname, email, password, profile FROM users WHERE email = '$email' AND status = 1 AND type = 'owner';");
        if ($result) {
            if (mysqli_num_rows($result) == 0) return 0;
            $row = mysqli_fetch_assoc($result);
            if (password_verify($password, $row['password']) && $email == $row['email']) {
                return $row['id'];
            }
            return 0;
        }
        return 0;
    }

    static function getUserById($id)
    {
        $result = DbConfig::query("SELECT * FROM users WHERE id = $id AND status = 1;");
        if ($result) {
            if (mysqli_num_rows($result) <= 0) return false;
            $row = mysqli_fetch_assoc($result);
            return self::convertRowToUserObject($row);
        }
        return false;
    }


    static function getUserByCookie($loginEmail, $lastLoginDate)
    {
        $result = DbConfig::query("SELECT * FROM users WHERE email = '$loginEmail' AND last_login = '$lastLoginDate' AND status = 1;");
        if ($result) {
            if (mysqli_num_rows($result) <= 0) return false;
            $row = mysqli_fetch_assoc($result);
            return self::convertRowToUserObject($row);
        }
        return false;
    }

    static function getAllUsers()
    {
        $users = [];
        $result = DbConfig::query("SELECT * FROM users WHERE status = 1;");
        if ($result) {
            if (mysqli_num_rows($result) <= 0) return false;
            while ($row = mysqli_fetch_assoc($result)) {
                $users[] = self::convertRowToUserObject($row);
            }
            return $users;
        }
        return false;
    }

    static function convertRowToUserObject($row)
    {
        $user = new User();
        $user->id = $row['id'];
        $user->createdDate = $row['created_date'];
        $user->userType = $row['type'];
        $user->fullName = $row['fullname'];
        $user->gender = $row['gender'];
        $user->dob = $row['birth_date'];
        $user->email = $row['email'];
        $user->password = $row['password'];
        $user->profile = $row['profile'];
        $user->codeMail = $row['code_mail'];
        return $user;
    }

    static function updateVerificationCode($email){
        $generatedCode = rand(1000000,9999999) .''.time();
        DbConfig::query("UPDATE users SET code_mail = $generatedCode WHERE email = '$email' AND status = 1;", $not_used, $affected_rows);
        if ($affected_rows > 0){
            return self::getUserByEmail($email);
        }
        return false;
    }

    static function getUserByEmail($email)
    {
        $result = DbConfig::query("SELECT * FROM users WHERE email = '$email' AND status = 1;");
        if ($result) {
            if (mysqli_num_rows($result) <= 0) return false;
            $row = mysqli_fetch_assoc($result);
            return self::convertRowToUserObject($row);
        }
        return false;
    }

    static function resetPasswordByCode($hashedPassword,$generatedCode){
        DbConfig::query("UPDATE users SET password = '$hashedPassword' WHERE code_mail = '$generatedCode' AND status = 1;", $not_used, $affected_rows);
        return $affected_rows;
    }

    static function updateLastLogin($id,$lastLoginDate){
        DbConfig::query("UPDATE users SET last_login = '$lastLoginDate' WHERE id = $id AND status = 1;", $not_used, $affected_rows);
        return $affected_rows;
    }

    static function getUserByLoginAsEmail($email)
    {
        $result = DbConfig::query("SELECT * FROM users WHERE email = '$email' AND status = 1 AND type != 'owner';");
        if ($result) {
            if (mysqli_num_rows($result) <= 0) return false;
            $row = mysqli_fetch_assoc($result);
            return self::convertRowToUserObject($row);
        }
        return false;
    }
}

?>