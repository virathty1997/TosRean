<?php
include_once '../config/DbConfig.php';

class Book
{
    var $id;
    var $date;
    var $user_id;
    var $title;
    var $author;
    var $genres;
    var $published_date;
    var $description;
    var $book_cover;
    var $view_number;
    var $download_number;
    var $markId;
    var $mean_rate;
    var $rate_count;

    function __construct()
    {
        $this->id = 0;
        $this->date = 'unknown';
        $this->title = 'unknown';
        $this->user_id = '0';
        $this->author = 'unknown';
        $this->genres = 'unknown';
        $this->published_date = 'unknown';
        $this->description = '';
        $this->book_cover = '';
        $this->view_number = '0';
        $this->download_number = '0';
        $this->mean_rate = 0;
        $this->rate_count = 0;
    }

    static function get_book_all()
    {
        $books = [];
        $result = DbConfig::query("select * from uploads where status = 1;");
        if ($result && mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function get_book_by_Id($id)
    {
        $result = DbConfig::query("select * from uploads where status = 1 AND id = " . $id);
        if ($result && mysqli_num_rows($result) > 0) {
            $book = new Book();
            while ($row = mysqli_fetch_assoc($result)) {
                $book = self::convertRowObjectToBook($row);
            }
            return $book;
        }
        return false;
    }

    static function add_book($p)
    {
        $result = DbConfig::query("insert into uploads(date, user_id, title, author, genres, published_date, description, file_cover, view_number, download_number, status) values(NOW(), $p->user_id,'$p->title','$p->author','$p->genres','$p->published_date','$p->description','$p->book_cover',0,0,1)", $inserted_id);
        if ($result) return $inserted_id;
        return 0;
    }

    static function update_book_detail($p, $affected_rows)
    {
        $result = DbConfig::query("update uploads set date = '$p->date', user_id = '$p->user_id', author = '$p->author', genres = '$p->genres', published_date = '$p->publish_date', description = '$p->description', view-number = '$p->view_number', download_number = '$p->download-number' where id = $p->id", $affected_rows);
        if ($result) return $affected_rows;
        return 0;
    }

    static function update_view_number($id)
    {
        DbConfig::query("update uploads set view_number = (view_number + 1) where id = " . $id, $not_used, $affected_rows);
        return $affected_rows;
    }

    static function popular_book()
    {
        $books = [];
        $query = DbConfig::query("select * from uploads 
                                      where status = 1 AND view_number <= (select max(view_number) from uploads)
                                      order by view_number desc 
                                      limit 4");
        if ($query && mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function update_book($book)
    {
        DbConfig::query("UPDATE uploads SET title = '$book->title', author = '$book->author', genres = '$book->genres', published_date = '$book->published_date', description = '$book->description', file_cover = '$book->book_cover' WHERE id = $book->id;",$not_used, $affected_rows);
        return $affected_rows;
    }

    static function book_info($type,$userLoginId = 0)
    {
        $query = "SELECT U.*, 
		                        (
                              (SELECT SUM(R1.rate_type) FROM rates AS R1 WHERE R1.upload_id = U.id)/
                              (SELECT COUNT(R2.id) FROM rates AS R2 WHERE R2.upload_id = U.id)
                            ) AS mean_rate,
                            (SELECT id FROM marks WHERE user_id = $userLoginId AND upload_id = U.id AND status = 1) AS marked      
                            FROM uploads AS U WHERE U.status = 1 AND U.genres = '$type'
                            ORDER BY U.date DESC LIMIT 4";

        $books = [];
        $result = DbConfig::query($query);
        //$query = DbConfig::query("SELECT * FROM uploads WHERE status = 1 AND genres = '$type' LIMIT 4");
        if ($result && mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function get_marked_book($user_id)
    {
        $books = [];
        $result = DbConfig::query("SELECT U.*, 
		                                          (
		                                            (SELECT SUM(R1.rate_type) FROM rates AS R1 WHERE R1.upload_id = U.id)/
                                                (SELECT COUNT(R2.id) FROM rates AS R2 WHERE R2.upload_id = U.id)
                                              )AS mean_rate  
	                                            FROM uploads AS U INNER JOIN marks as M ON U.id = M.upload_id 
                                              WHERE U.status = 1 AND M.status = 1 AND M.user_id = $user_id
                                              ORDER BY M.id DESC;");
        if ($result && mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function get_book_by_category($userLoginId, $genres)
    {
        $selectQuery = "SELECT U.*, 
		                        (
                                (SELECT SUM(R1.rate_type) FROM rates AS R1 WHERE R1.upload_id = U.id)/
                                (SELECT COUNT(R2.id) FROM rates AS R2 WHERE R2.upload_id = U.id)
                            ) AS mean_rate,
                            (SELECT id FROM marks WHERE user_id = $userLoginId AND upload_id = U.id AND status = 1) AS marked      
                            FROM uploads AS U WHERE U.status = 1 AND U.genres = '$genres'";
        $books = [];
        //$query = DbConfig::query("SELECT * FROM uploads WHERE genres = '$genres' AND user_id = '$user_id'");
        $query = DbConfig::query($selectQuery);
        if ($query && mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function getCountBookByGenres($genres)
    {
        $query = DbConfig::query("SELECT COUNT(id) AS number FROM uploads WHERE genres = '$genres' AND status = 1;");
        if ($query && mysqli_num_rows($query) > 0) {
            $total = mysqli_fetch_assoc($query)['number'];
            return $total;
        }
        return false;
    }

    static function pagination($perPage, $offSet, $user_id, $book_genres)
    {
        $selectQuery = "SELECT U.*, 
		                        (
                                (SELECT SUM(R1.rate_type) FROM rates AS R1 WHERE R1.upload_id = U.id)/
                                (SELECT COUNT(R2.id) FROM rates AS R2 WHERE R2.upload_id = U.id)
                            ) AS mean_rate,
                            (SELECT id FROM marks WHERE user_id = $user_id AND upload_id = U.id AND status = 1) AS marked      
                            FROM uploads AS U WHERE U.status = 1 AND U.genres = '$book_genres'
                            ORDER BY id DESC LIMIT $perPage OFFSET $offSet";

        $books = [];
        //$query = DbConfig::query("SELECT * FROM uploads WHERE user_id = '$user_id' AND genres = '$book_genres' ORDER by id ASC  LIMIT $perPage OFFSET $offSet ");
        $query = DbConfig::query($selectQuery);

        if ($query && mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function updateDownloadNumber($id)
    {
        DbConfig::query("update uploads set download_number = (download_number + 1) where id = " . $id, $not_used, $affected_rows);
        return $affected_rows;
    }

    static function getUploadedBookByUser($userId,$sortBy,$searchText)
    {
        $books = [];
        $query = "SELECT U.*, 
		                        (
                              (SELECT SUM(R1.rate_type) FROM rates AS R1 WHERE R1.upload_id = U.id)/
                              (SELECT COUNT(R2.id) FROM rates AS R2 WHERE R2.upload_id = U.id)
                            ) AS mean_rate,
                            (SELECT id FROM marks WHERE user_id = $userId AND upload_id = U.id AND status = 1) AS marked,
                            (SELECT COUNT(id) FROM rates WHERE upload_id = U.id) AS rate_count
                            FROM uploads AS U WHERE U.user_id = $userId AND U.status = 1 ";

        /*Concatenate with search condition*/
        if (isset($searchText) && !empty($searchText)){
            $query .= " AND ( date_format(U.date, '%Y-%m-%d') LIKE '%$searchText%' OR date_format(U.published_date, '%Y-%m-%d') LIKE '%$searchText%' OR U.title LIKE '%$searchText%' OR U.author LIKE '%$searchText%' OR U.genres LIKE '%$searchText%') ";
        }

        /*Concatenate with Sort condition*/
        if ($sortBy == 'date') $query .= "ORDER BY U.date DESC;";
        else if ($sortBy == 'title') $query .= "ORDER BY U.title;";
        else if ($sortBy == 'author') $query .= "ORDER BY U.author;";
        else if ($sortBy == 'genres') $query .= "ORDER BY U.genres;";
        else if ($sortBy == 'published') $query .= "ORDER BY U.published_date DESC;";
        else if ($sortBy == 'view') $query .= "ORDER BY U.view_number DESC;";
        else if ($sortBy == 'download') $query .= "ORDER BY U.download_number DESC;";
        else if ($sortBy == 'rate') $query .= "ORDER BY rate_count DESC;";
        else if ($sortBy == 'mean-rate') $query .= "ORDER BY mean_rate DESC;";

        $result = DbConfig::query($query);
        if ($result && mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function delete($uploadId)
    {
        $result = DbConfig::query("UPDATE uploads SET status = 0 WHERE id = $uploadId;",$not_used, $affected_rows);
        return ($result)?$affected_rows:0;
    }

    static function deleteFromTable($uploadId)
    {
        DbConfig::query("DELETE FROM uploads WHERE id = $uploadId;",$not_used, $affected_rows);
        return $affected_rows;
    }


    static function getSearchBookResult($perPage,$offSet,$userId,$sortBy,$searchText)
    {
        if (empty($searchText)) return false;
        $books = [];
        $query = "SELECT U.*, 
		                        (
                              (SELECT SUM(R1.rate_type) FROM rates AS R1 WHERE R1.upload_id = U.id)/
                              (SELECT COUNT(R2.id) FROM rates AS R2 WHERE R2.upload_id = U.id)
                            ) AS mean_rate,
                            (SELECT id FROM marks WHERE user_id = $userId AND upload_id = U.id AND status = 1) AS marked
                            FROM uploads AS U WHERE U.status = 1 AND ( date_format(U.date, '%Y-%m-%d') LIKE '%$searchText%' OR date_format(U.published_date, '%Y-%m-%d') LIKE '%$searchText%' OR U.title LIKE '%$searchText%' OR U.author LIKE '%$searchText%' OR U.genres LIKE '%$searchText%') ";

        /*Concatenate with Sort condition*/
        if ($sortBy == 'date') $query .= "ORDER BY U.date DESC ";
        else if ($sortBy == 'title') $query .= "ORDER BY U.title";
        else if ($sortBy == 'author') $query .= "ORDER BY U.author";
        else if ($sortBy == 'genres') $query .= "ORDER BY U.genres";
        else if ($sortBy == 'published') $query .= "ORDER BY U.published_date DESC";
        else if ($sortBy == 'view') $query .= "ORDER BY U.view_number DESC";
        else if ($sortBy == 'download') $query .= "ORDER BY U.download_number DESC";
        else if ($sortBy == 'rate') $query .= "ORDER BY rate_count DESC";
        else if ($sortBy == 'mean-rate') $query .= "ORDER BY mean_rate DESC";

        /*Set limit display rows*/
        $query .= " LIMIT $perPage OFFSET $offSet;";

        $result = DbConfig::query($query);
        if ($result && mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

    static function getCountSearchResult($searchText)
    {
        if (empty($searchText)) return 0;

        $query = "SELECT COUNT(id) AS result_count FROM uploads AS U WHERE U.status = 1 AND ( date_format(U.date, '%Y-%m-%d') LIKE '%$searchText%' OR date_format(U.published_date, '%Y-%m-%d') LIKE '%$searchText%' OR U.title LIKE '%$searchText%' OR U.author LIKE '%$searchText%' OR U.genres LIKE '%$searchText%');";
        $result = DbConfig::query($query);
        return ($result)?mysqli_fetch_assoc($result)['result_count']:0;
    }

    static function convertRowObjectToBook($row){
        $book = new Book();
        $book->id = $row['id'];
        $book->date = $row['date'];
        $book->user_id = $row['user_id'];
        $book->title = $row['title'];
        $book->author = $row['author'];
        $book->genres = $row['genres'];
        $book->published_date = $row['published_date'];
        $book->description = $row['description'];
        $book->book_cover = $row['file_cover'];
        $book->view_number = $row['view_number'];
        $book->download_number = $row['download_number'];
        $book->mean_rate = isset($row['mean_rate'])?$row['mean_rate']:0;
        $book->markId = isset($row['marked'])?$row['marked']:0;
        $book->rate_count = isset($row['rate_count'])?$row['rate_count']:0;
        return $book;
    }

    static function getRecommendedBook($bookType,$uploadId)
    {
        $books = [];
        $query = DbConfig::query("SELECT * FROM uploads 
                                      WHERE genres = '$bookType' AND id != $uploadId AND status = 1 
                                      ORDER BY view_number DESC, download_number DESC
                                      LIMIT 10;");
        if ($query && mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query)) {
                $books[] = self::convertRowObjectToBook($row);
            }
            return $books;
        }
        return false;
    }

}

?>