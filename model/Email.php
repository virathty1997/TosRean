<?php
    require_once '../mailer/PHPMailerAutoload.php';
    require_once '../model/User.class.php';

    function sendResetPasswordLink($user){
        $mail = new PHPMailer;

        /*NOTE : 1- We can use only Gmail account that is not turned on 2 Steps Verifications of Security
                 2- The sender must TURN ON 'allow less secure App'*/

        $mail->SMTPDebug = 4;
        $fromEmail = 'reanpost2018@gmail.com';
        $password = 'reanpost@2018';

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $fromEmail;                 // SMTP username
        $mail->Password = $password;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom($fromEmail, 'Rean Post');
        $mail->addAddress($user->email);     // Add a recipient
        $mail->addReplyTo($fromEmail);
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Send verification link to reset password';
        $bodyText = '<div>Hi '.$user->fullName.', </div>';
        $bodyText .= '<div><br><br>You have sent request to reset your password. <a target="_blank" href="localhost/reanpost/pages/reset_password.php?code='.$user->codeMail.'">Click on this link to reset your password.</a></div>';
        $bodyText .= '<br><div>Best Regards, <br><br>Rean Post</div>';
        $bodyText .= '<div><br><b>Thank you for visiting our website!!!</b></div>';
        $mail->Body = $bodyText;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }

function sendEmailNotification($follower, $posterName, $uploadId){
    $mail = new PHPMailer;
    $mail->SMTPDebug = 4;
    $fromEmail = 'reanpost2018@gmail.com';
    $password = 'reanpost@2018';

    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = $fromEmail;
    $mail->Password = $password;
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->setFrom($fromEmail, 'Rean Post');
    $mail->addAddress($follower->email);
    $mail->addReplyTo($fromEmail);
    $mail->isHTML(true);

    $mail->Subject = 'New Book Upload From REAN POST Website';
    $bodyText = '<div>Hi '.$follower->fullName.', </div>';
    $bodyText .= '<div><br><br>Mr/Mrs '. $posterName .' has just uploaded a new interesting book. You are able to view and download it by this link.<a target="_blank" href="localhost/reanpost/pages/book_detail.php?book='.$uploadId.'">Click here to view book.</a></div>';
    $bodyText .= '<br><div>Best Regards, <br><br>Rean Post</div>';
    $bodyText .= '<div><br><b>Thank you for visiting our website!!!</b></div>';
    $mail->Body = $bodyText;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    if(!$mail->send()) {
        return false;
    } else {
        return true;
    }
}