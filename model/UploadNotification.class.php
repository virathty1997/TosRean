<?php
    class UploadNotification{
        var $id;
        var $datetime;
        var $fullName;
        var $profile;
        var $uploadId;
        var $seen;

        static function add($posterId,$followerId,$uploadId){
            $result = DbConfig::query("INSERT INTO upload_notification(date, poster_id, follower_id, upload_id,seen) values(NOW(),$posterId, $followerId, $uploadId, 0);",$inserted_id);
            return ($result)? $inserted_id:0;
        }

        static function updateUploadSendById($id){
            DbConfig::query("UPDATE upload_notification SET seen = 1 WHERE id = $id",$not_used,$affectedRows);
            return $affectedRows;
        }

        static function getUploadNotifications($userLoginId,$limitRows = null,$filterDate = null){
            $uploadList = [];
            if ($filterDate == null){
                if ($limitRows == null){
                    $result = DbConfig::query("SELECT UN.id, UN.date, U.fullname, U.profile, UN.upload_id, UN.seen 
                                                    FROM (users AS U INNER JOIN upload_notification AS UN ON U.id = UN.poster_id) INNER JOIN uploads AS UD ON UD.id = UN.upload_id
                                                    WHERE UN.follower_id = $userLoginId AND UD.status = 1;");
                }else{
                    $result = DbConfig::query("SELECT UN.id, UN.date, U.fullname, U.profile, UN.upload_id, UN.seen 
                                                    FROM (users AS U INNER JOIN upload_notification AS UN ON U.id = UN.poster_id) INNER JOIN uploads AS UD ON UD.id = UN.upload_id
                                                    WHERE UN.follower_id = $userLoginId AND UD.status = 1
                                                    ORDER BY UN.id DESC LIMIT $limitRows;");
                }
            }else{
                $result = DbConfig::query("SELECT UN.id, UN.date, U.fullname, U.profile, UN.upload_id, UN.seen 
                                               FROM (users AS U INNER JOIN upload_notification AS UN ON U.id = UN.poster_id) INNER JOIN uploads AS UD ON UD.id = UN.upload_id
                                               WHERE UN.follower_id = $userLoginId AND UD.status = 1 AND DATE(UN.date) = '$filterDate';");
            }

            if ($result){
                while ($row = mysqli_fetch_assoc($result)){
                    $upload = new UploadNotification();
                    $upload->id = $row['id'];
                    $upload->datetime = $row['date'];
                    $upload->fullName = $row['fullname'];
                    $upload->profile = $row['profile'];
                    $upload->uploadId = $row['upload_id'];
                    $upload->seen = $row['seen'];
                    $uploadList[] = $upload;
                }
                return $uploadList;
            }
            return false;
        }

        static function changeAllNewUploadToSeen($followerId){
            DbConfig::query("UPDATE upload_notification SET seen = 1 WHERE follower_id = $followerId AND seen = 0;",$not_used,$affectedRows);
            return $affectedRows;
        }

    }