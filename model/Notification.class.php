<?php
    require_once 'UploadNotification.class.php';

    class ReactionNotification{
        var $id;
        var $datetime;
        var $fullName;
        var $profile;
        var $react;
        var $commentId;
        var $uploadId;
        var $seen;
    }

    class CommentNotification{
        var $datetime;
        var $fullName;
        var $profile;
        var $uploadId;
        var $commentId;
        var $seen;
    }

    class Notification{
        static function countNotifications($userLoginId){
            $result = DbConfig::query("SELECT (SELECT COUNT(C.id) FROM uploads AS U INNER JOIN comments AS C ON U.id = C.upload_id WHERE U.user_id = $userLoginId AND C.seen = 0 AND C.status = 1 AND U.status = 1 AND C.user_id != $userLoginId) AS comment_number,
                                                  (SELECT COUNT(R.id) FROM (uploads AS U INNER JOIN comments AS C ON U.id = C.upload_id) INNER JOIN reaction AS R ON C.id = R.comment_id WHERE C.user_id = $userLoginId AND U.status = 1 AND R.seen = 0 AND C.status = 1 AND R.user_id != $userLoginId) AS reaction_number,
                                                  (SELECT COUNT(UN.id) FROM upload_notification AS UN INNER JOIN uploads AS UD ON UN.upload_id = UD.id WHERE UN.follower_id = $userLoginId AND UN.seen = 0 AND UD.status = 1) AS upload_notification");
            if ($result){
                $row = mysqli_fetch_assoc($result);
                return ($row['reaction_number'] + $row['comment_number'] + $row['upload_notification']);
            }
            return 0;
        }

        static function getAllNotifications($userLoginId,$limitRows = null, $filterDate = null){
            $notificationList = [];

            /*Move comment to Whole list*/
            $comments = self::getCommentNotifications($userLoginId,$limitRows,$filterDate);
            if ($comments){
                foreach ($comments as $comment){
                    $notificationList[] = $comment;
                }
            }

            //Move reaction to Whole list
            $reactions = self::getReactionNotifications($userLoginId,$limitRows,$filterDate);
            if ($reactions){
                foreach ($reactions as $reaction){
                    $notificationList[] = $reaction;
                }
            }

            $newUploads = UploadNotification::getUploadNotifications($userLoginId,$limitRows,$filterDate);
            if ($newUploads){
                foreach ($newUploads as $upload){
                    $notificationList[] = $upload;
                }
            }

            /*Sort All Notifications by DateTime*/
            usort($notificationList, "self::cmp");

            return $notificationList;
        }

        static function cmp($item1, $item2){
            return $item1->datetime < $item2->datetime;
        }

        private static function getCommentNotifications($userLoginId,$limitRows = null,$filterDate = null){
            $commentList = [];
            if ($filterDate == null){
                if ($limitRows == null){
                    $result = DbConfig::query("SELECT C.date,US.fullname,US.profile,C.upload_id,C.id,C.seen FROM (uploads AS U INNER JOIN comments AS C ON U.id = C.upload_id) INNER JOIN users AS US ON C.user_id = US.id WHERE U.user_id = $userLoginId  AND C.user_id != $userLoginId AND C.status = 1 AND U.status = 1;");
                }else{
                    $result = DbConfig::query("SELECT C.date,US.fullname,US.profile,C.upload_id,C.id,C.seen FROM (uploads AS U INNER JOIN comments AS C ON U.id = C.upload_id) INNER JOIN users AS US ON C.user_id = US.id WHERE U.user_id = $userLoginId  AND C.user_id != $userLoginId AND C.status = 1 AND U.status = 1 ORDER BY C.id DESC LIMIT $limitRows;");
                }
            }else{
                $result = DbConfig::query("SELECT C.date,US.fullname,US.profile,C.upload_id,C.id,C.seen FROM (uploads AS U INNER JOIN comments AS C ON U.id = C.upload_id) INNER JOIN users AS US ON C.user_id = US.id WHERE U.user_id = $userLoginId  AND C.user_id != $userLoginId AND C.status = 1 AND U.status = 1 AND DATE(C.date) = '$filterDate';");
            }

            if ($result){
                while ($row = mysqli_fetch_assoc($result)){
                    $comment = new CommentNotification();
                    $comment->datetime = $row['date'];
                    $comment->fullName = $row['fullname'];
                    $comment->profile = $row['profile'];
                    $comment->uploadId = $row['upload_id'];
                    $comment->commentId = $row['id'];
                    $comment->seen = $row['seen'];
                    $commentList[] = $comment;
                }
                return $commentList;
            }
            return false;
        }

        private static function getReactionNotifications($userLoginId,$limitRows = null,$filterDate = null){
            $reactionList = [];
            if ($filterDate == null){
                if ($limitRows == null){
                    $result = DbConfig::query("SELECT R.id, R.date,U.fullname,U.profile,R.type,R.comment_id,C.upload_id,R.seen FROM ((uploads INNER JOIN comments AS C ON uploads.id = C.upload_id) INNER JOIN reaction AS R ON C.id = R.comment_id) INNER JOIN users U ON U.id = R.user_id WHERE C.user_id = $userLoginId AND R.user_id != $userLoginId AND C.status = 1 AND uploads.status = 1;");
                }else{
                    $result = DbConfig::query("SELECT R.id, R.date,U.fullname,U.profile,R.type,R.comment_id,C.upload_id,R.seen FROM ((uploads INNER JOIN comments AS C ON uploads.id = C.upload_id) INNER JOIN reaction AS R ON C.id = R.comment_id) INNER JOIN users U ON U.id = R.user_id WHERE C.user_id = $userLoginId AND R.user_id != $userLoginId AND C.status = 1 AND uploads.status = 1 ORDER BY R.id DESC LIMIT $limitRows;");
                }
            }else{
                $result = DbConfig::query("SELECT R.id, R.date,U.fullname,U.profile,R.type,R.comment_id,C.upload_id,R.seen FROM ((uploads INNER JOIN comments AS C ON uploads.id = C.upload_id) INNER JOIN reaction AS R ON C.id = R.comment_id) INNER JOIN users U ON U.id = R.user_id WHERE C.user_id = $userLoginId AND R.user_id != $userLoginId AND C.status = 1 AND uploads.status = 1 AND DATE(R.date) = '$filterDate';");
            }

            if ($result){
                while ($row = mysqli_fetch_assoc($result)){
                    $react = new ReactionNotification();
                    $react->id = $row['id'];
                    $react->datetime = $row['date'];
                    $react->fullName = $row['fullname'];
                    $react->profile = $row['profile'];
                    $react->react = $row['type'];
                    $react->commentId = $row['comment_id'];
                    $react->uploadId = $row['upload_id'];
                    $react->seen = $row['seen'];
                    $reactionList[] = $react;
                }
                return $reactionList;
            }
            return false;
        }
    }