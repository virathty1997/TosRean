<?php
    class Rate{
        var $rateDate;
        var $userId;
        var $uploadId;
        var $rateType;

        function __construct($id = 0, $rateDate = 0, $userId = 0, $uploadId = 0, $rateType = 0)
        {
            $this->id = $id;
            $this->rateDate = $rateDate;
            $this->userId = $userId;
            $this->uploadId =  $uploadId;
            $this->rateType = $rateType;
        }

        static function isExist($rate){
            $id = 0;
            $result = DbConfig::query("SELECT id FROM rates WHERE upload_id = $rate->uploadId AND user_id = $rate->userId;");
            if ($result){
                $id = mysqli_fetch_assoc($result)['id'];
            }
            return $id;
        }

        static function modify($rate){
            $sql = "IF EXISTS(SELECT id FROM rates WHERE upload_id = $rate->uploadId AND user_id = $rate->userId) 
                        UPDATE rates SET date = NOW(), rate_type = $rate->rateType WHERE user_id = $rate->userId AND upload_id = $rate->uploadId
                   ELSE 
                        INSERT INTO rates(date, user_id, upload_id, rate_type, status) values(NOW(),$rate->userId, $rate->uploadId, $rate->rateType,1)";
            DbConfig::query($sql,$inserted_id,$affected_rows);
            if ($inserted_id ==0 && $affected_rows == 0) return 1; /*Record was edited but the new value is the same as old record and didn't insert new record*/
            else if($inserted_id != 0) return;
            return $affected_rows; /*Updated one record or more*/
        }

        static function add($rate){
            DbConfig::query("INSERT INTO rates(date, user_id, upload_id, rate_type, status) values(NOW(),$rate->userId, $rate->uploadId, $rate->rateType,1)",$inserted_id);
            return $inserted_id;
        }

        static  function update($rate){
            $result = DbConfig::query("UPDATE rates SET date = NOW(), rate_type = $rate->rateType WHERE user_id = $rate->userId AND upload_id = $rate->uploadId",$not_used,$affected_rows);
            return ($result)?$affected_rows: 0;
        }

        static  function delete($id){
            $result = DbConfig::query("UPDATE rates SET status = 0  WHERE id = $id",$not_used,$affected_rows);
            return ($result)?$affected_rows: 0;
        }

        static  function getRateCount($uploadId){
            $rateCount = 0;
            $result = DbConfig::query("SELECT COUNT(id) AS rate_count FROM rates WHERE upload_id = $uploadId");
            if ($result){
                $rateCount = mysqli_fetch_assoc($result)['rate_count'];
            }
            return $rateCount;
        }

        static  function getRateByUser($userId,$uploadId){
            $rateType = 0;
            $result = DbConfig::query("SELECT rate_type FROM rates WHERE upload_id = $uploadId AND user_id = $userId");
            if ($result){
                $rateType = mysqli_fetch_assoc($result)['rate_type'];
            }
            return $rateType;
        }

        static function getRates($uploadId){
            $rates = null;
            $result = DbConfig::query("SELECT (SELECT COUNT(id) FROM rates WHERE upload_id = $uploadId AND rate_type = 1) AS one,
                                            (SELECT COUNT(id) FROM rates WHERE upload_id = $uploadId AND rate_type = 2) AS two,
                                            (SELECT COUNT(id) FROM rates WHERE upload_id = $uploadId AND rate_type = 3) AS three,
                                            (SELECT COUNT(id) FROM rates WHERE upload_id = $uploadId AND rate_type = 4) AS four,
                                            (SELECT COUNT(id) FROM rates WHERE upload_id = $uploadId AND rate_type = 5) AS five;");
            if ($result){
                while($row = mysqli_fetch_assoc($result)){
                    $rates = array($row['one'], $row['two'], $row['three'], $row['four'], $row['five']);
                }
                return $rates;
            }
            return false;
        }
    }
?>