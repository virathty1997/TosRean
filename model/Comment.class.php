
<?php
    class Comment{
        var $id;
        var $createdDate;
        var $userId;
        var $fullName;
        var $profile;
        var $uploadId;
        var $commentText;
        var $likeNumber;
        var $dislikeNumber;
        var $isEdited;
        var $rateType;
        function __construct()
        {
            $this->id = 0;
            $this->createdDate = null;
            $this->userId = 0;
            $this->fullName = "unknown";
            $this->profile = "";
            $this->uploadId = 0;
            $this->commentText = "No comment text";
            $this->likeNumber = 0;
            $this->dislikeNumber = 0;
            $this->isEdited = 0;
            $this->rateType = 0;
        }

        static function convertRowToCommentObject($row){
            $com = new Comment();
            $com->id = $row['id'];
            $com->createdDate = $row['date'];
            $com->userId = $row['user_id'];
            $com->fullName = $row['fullname'];
            $com->profile = $row['profile'];
            $com->uploadId = $row['upload_id'];
            $com->commentText = $row['comment'];
            $com->likeNumber = $row['like_number'];
            $com->dislikeNumber = $row['dislike_number'];
            $com->isEdited = $row['is_edited'];
            $com->rateType = $row['rate_type'];
            return $com;
        }

        static  function countAllComments($uploadId){
            $comments_count = 0;
            $result = DbConfig::query("SELECT COUNT(id) AS comment_count FROM comments WHERE upload_id = $uploadId AND status = 1;");
            if ($result){
                while($row = mysqli_fetch_assoc($result)){
                    $comments_count = $row['comment_count'];
                }
                return $comments_count;
            }
            return false;
        }

        static  function getAll($uploadId){
            $comments = [];
            $result = DbConfig::query("SELECT C.id,C.date,C.user_id,U.fullname,U.profile,C.upload_id,C.comment, (SELECT count(id) as like_number FROM reaction WHERE type = 0 AND comment_id = C.id) AS like_number,(SELECT count(id) as dislike_number FROM reaction WHERE type = 1 AND comment_id = C.id) AS dislike_number, is_edited, (SELECT rate_type FROM rates WHERE upload_id = C.upload_id AND user_id = C.user_id) AS rate_type FROM comments AS C INNER JOIN users AS U ON C.user_id = U.id WHERE upload_id = $uploadId and C.status = 1 ORDER BY C.id DESC;");
            if ($result){
                while($row = mysqli_fetch_assoc($result)){
                    $comments[] = self::convertRowToCommentObject($row);
                }
                return $comments;
            }
            return false;
        }

        static function getLimitedRows($uploadId, $recordPosition){
            $comments = [];
            $result = DbConfig::query("SELECT C.id,C.date,C.user_id,U.fullname,U.profile,C.upload_id,C.comment, (SELECT count(id) as like_number FROM reaction WHERE type = 0 AND comment_id = C.id) AS like_number,(SELECT count(id) as dislike_number FROM reaction WHERE type = 1 AND comment_id = C.id) AS dislike_number, is_edited, (SELECT rate_type FROM rates WHERE upload_id = C.upload_id AND user_id = C.user_id) AS rate_type FROM comments AS C INNER JOIN users AS U ON C.user_id = U.id WHERE upload_id = $uploadId and C.status = 1 ORDER BY C.id DESC LIMIT $recordPosition;");
            if ($result){
                while($row = mysqli_fetch_assoc($result)){
                    $comments[] = self::convertRowToCommentObject($row);
                }
                return $comments;
            }
            return false;
        }

        static  function getById($id){
            $result = DbConfig::query("SELECT C.id, C.date, C.user_id, U.fullname, U.profile, C.upload_id, C.comment, (SELECT count(id) as like_number FROM reaction WHERE type = 0 AND comment_id = C.id) AS like_number,(SELECT count(id) as dislike_number FROM reaction WHERE type = 1 AND comment_id = C.id) AS dislike_number, is_edited, (SELECT rate_type FROM rates WHERE upload_id = C.upload_id AND user_id = C.user_id) AS rate_type FROM comments AS C INNER JOIN users AS U ON C.user_id = U.id WHERE C.status = 1 AND C.id = $id");
            if ($result){
                $com = null;
                while($row = mysqli_fetch_assoc($result)){
                    $com = self::convertRowToCommentObject($row);
                }
                return $com;
            }
            return false;
        }

        static function add($com){
            DbConfig::query("INSERT INTO comments(date,user_id,upload_id,comment, is_edited,status) VALUES( NOW(), $com->userId, $com->uploadId, '$com->commentText' ,0, 1)",$inserted_id);
            return $inserted_id;
        }

        static  function update($com){
            $result = DbConfig::query("UPDATE comments SET date = NOW(),user_id = $com->userId, upload_id = $com->uploadId, comment = '$com->commentText', is_edited = 1 WHERE id = $com->id",$not_used,$affected_rows);
            return ($result)? $affected_rows:0;
        }

        static  function delete($id){
            $result = DbConfig::query("UPDATE comments SET status = 0 WHERE id = $id",$not_used,$affected_rows);
            return ($result)?$affected_rows: 0;
        }

        static  function updateSeenById($id){
            $result = DbConfig::query("UPDATE comments SET seen = 1 WHERE id = $id;",$not_used,$affected_rows);
            return ($result)? $affected_rows:0;
        }

        static  function changeAllCommentToSeenByUserId($userLoginId){
            $result = DbConfig::query("UPDATE uploads AS U INNER JOIN comments AS C ON U.id = C.upload_id SET C.seen = 1 WHERE U.user_id = $userLoginId AND C.seen = 0 AND U.status = 1 AND C.status = 1 AND C.user_id != $userLoginId;",$not_used,$affected_rows);
            return ($result)? $affected_rows:0;
        }

    }
?>